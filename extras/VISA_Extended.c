
//VISA Extended library version 1.2.0
// [1.2.0]
// - added viClear() to VISAext_Open() and VISAext_ClearStatus()
//
// [1.1.0]
// - added VISAext_WriteWithSTBpollSync() and VISAext_QueryWithSTBpollSync()
// - added VISAext_WriteWithSRQsync() and VISAext_QueryWithSRQsync() 
// - added VISAext_WriteWithSRQevent()
// 
// [1.0.0]
// - First version created

#include "time.h"
#include <windows.h>
#include "VISA_Extended.h"
#include <ansi_c.h>
                    
#define CHUNK_BUFFER_SIZE 16384

#ifndef CHECKERR
#define CHECKERR(fCal) \
	status = (fCal);if (status < VI_SUCCESS) goto ERROR_HANDLING;
#endif

//------------------------------------------------------------------------------------
//internal functions
ViStatus VISAext_STBpolling(ViSession io, int timeout);

//------------------------------------------------------------------------------------
/// HIFN Initiates a new VISA connection defined by resourceString
ViStatus VISAext_Open(const char *resource, ViSession *io)
{
	ViStatus status = VI_SUCCESS;
	ViSession defaultRM;
	
	CHECKERR(viOpenDefaultRM(&defaultRM))
	CHECKERR(viOpen(defaultRM, (ViRsrc)resource, 0, 3000, io))
	CHECKERR(viSetAttribute(*io, VI_ATTR_USER_DATA_32, defaultRM))
    CHECKERR(viClear(*io))
    
ERROR_HANDLING:
	return status;
}

//------------------------------------------------------------------------------------
/// HIFN Closes the VISA session
ViStatus VISAext_Close(ViSession io)
{
	ViStatus status = VI_SUCCESS;
	ViSession defaultRM;

	status |= viGetAttribute(io, VI_ATTR_USER_DATA_32, &defaultRM);
	status |= viClose(io);
	status |= viClose(defaultRM);

	return status;
}

//------------------------------------------------------------------------------------
/// HIFN Sends command string to the instrument
ViStatus VISAext_Write(ViSession io, const char *command)
{
	ViStatus status = VI_SUCCESS;
	ViUInt32 count;
	ViChar commandLF[VISAEXT_BUFFER_SIZE];

	sprintf(commandLF, "%s\n", command);
	status = viWrite(io, (ViBuf)commandLF, strlen(commandLF), &count);
	
	return status;
}

//------------------------------------------------------------------------------------
/// HIFN Reads string response from the instrument
/// HIFN The potential LF character at the end is trimmed
/// HIFN The length of the response must not exceed the VISAEXT_BUFFER_SIZE
/// HIFN If you expect a long string of unknown length, use the VISAext_ReadLongString() function instead
ViStatus VISAext_Read(ViSession io, char *response)
{
	ViStatus status = VI_SUCCESS;
	ViUInt32 count = 0;
		
	status = viRead(io, (ViBuf)response, VISAEXT_BUFFER_SIZE, &count);
	response[count] = 0;
	// Trim the last LF of the response
	if (count > 0 && response[count - 1] == 10)
	{
		response[count - 1] = 0;
	}

	return status;
}

//------------------------------------------------------------------------------------
/// HIFN Reads string response from the instrument up to the maximum given length
/// HIFN The potential LF character at the end is trimmed
ViStatus VISAext_ReadLongString(ViSession io, char *response, int maxResponseSize)
{
	ViStatus status = VI_SUCCESS;
	ViUInt32 count = 0;

	status = viRead(io, (ViBuf)response, maxResponseSize, &count);
	response[count] = 0;
	// Trim the last LF of the response
	if (count > 0 && response[count - 1] == 10)
	{
		response[count - 1] = 0;
	}

	return status;
}

//------------------------------------------------------------------------------------
/// HIFN Combines the VISAext_Write() and VISAext_Read() into one function
/// HIFN The potential LF character at the end of the read string is trimmed
ViStatus VISAext_Query(ViSession io, const char *command, char *response)
{
	ViStatus status = VI_SUCCESS;
	
	CHECKERR(VISAext_Write(io, command))
	CHECKERR(VISAext_Read(io, response))
	
ERROR_HANDLING:
	return status;
}

//------------------------------------------------------------------------------------
/// HIFN Sends a query to the instrument, reads the reponse and converts it to integer number
ViStatus VISAext_QueryInteger(ViSession io, const char *command, int *response)
{
	ViStatus status = VI_SUCCESS;
	ViChar buffer[VISAEXT_BUFFER_SIZE];
	
	status = VISAext_Query(io, command, buffer);
	*response = atoi(buffer);

	return status;
}

//------------------------------------------------------------------------------------
/// HIFN Sends a query to the instrument, reads the reponse and converts it to double number
ViStatus VISAext_QueryDouble(ViSession io, const char *command, double *response)
{
	ViStatus status = VI_SUCCESS;
	ViChar buffer[VISAEXT_BUFFER_SIZE];

	status = VISAext_Query(io, command, buffer);
	*response = atof(buffer);

	return status;
}

//------------------------------------------------------------------------------------
/// HIFN Sets the timeout for all VISA Read operations
ViStatus VISAext_SetTimeout(ViSession io, int tout)
{
	ViStatus status = VI_SUCCESS;
	
	status = viSetAttribute(io, VI_ATTR_TMO_VALUE, tout);

	return status;
}

//------------------------------------------------------------------------------------
/// HIFN Returns the timeout for all VISA Read operations
ViStatus VISAext_GetTimeout(ViSession io, int *tout)
{
	ViStatus status = VI_SUCCESS;
	
	status = viGetAttribute(io, VI_ATTR_TMO_VALUE, &tout);

	return status;
}

//------------------------------------------------------------------------------------
/// HIFN Returns LF-separated string with all the errors read from the instrument error queue ('SYSTEM:ERROR?' query)
ViStatus VISAext_ReadErrorQueue(ViSession io, char *errors)
{
	ViStatus status = VI_SUCCESS;
	static char response[VISAEXT_BUFFER_SIZE];
	static char responseLow[VISAEXT_BUFFER_SIZE];
	int i;
    int stb;
    
    CHECKERR(VISAext_QueryInteger(io, "*STB?", &stb));
    if ((stb & 4) > 0)
    {	
        do
    	{
    		CHECKERR(VISAext_Query(io, "SYST:ERR?", response))
    		//response to lower case
    		strcpy(responseLow, response);
    		for (i = 0 ; i < (int)strlen(response) ; i++)
    			responseLow[i] = tolower(response[i]);
		
    		if (strstr(responseLow, "\"no error\""))
    			break;

    		if (status != VI_SUCCESS)
    			break;

    		if (errors != NULL)
    			sprintf(errors, "%s\n%s", errors, response);

    	} while (1);
    }
    
ERROR_HANDLING:
	return status;
}

//------------------------------------------------------------------------------------
/// HIFN Sends *CLS command and clears the Error Queue if necessary
ViStatus VISAext_ClearStatus(ViSession io)
{
	ViStatus status = VI_SUCCESS;
	static char response[VISAEXT_BUFFER_SIZE];
	
	CHECKERR(viClear(io))
    CHECKERR(VISAext_Query(io, "*CLS;*OPC?", response))
	CHECKERR(VISAext_ReadErrorQueue(io, NULL))

ERROR_HANDLING:
	return status;
}

//------------------------------------------------------------------------------------
/// HIFN Returns VISAEXT_ERR_INSTRUMENT_STATUS error if any error in the Error Queue is detected
ViStatus VISAext_ErrorChecking(ViSession io, char *errors)
{
	ViStatus status = VI_SUCCESS;
	sprintf(errors, "");
	CHECKERR(VISAext_ReadErrorQueue(io, errors))
	if (strlen(errors) > 0)
	{
		status = VISAEXT_ERR_INSTRUMENT_STATUS;
	}
ERROR_HANDLING:
	return status;
}

//------------------------------------------------------------------------------------
/// HIFN Converts errorCode into human-readable string
ViStatus VISAext_ErrorCodeDescription(ViSession io, ViStatus errorCode, char *description)
{
	ViStatus status = VI_SUCCESS;

	status = viStatusDesc(io, errorCode, description);

	return status;
}

//------------------------------------------------------------------------------------
/// HIFN Reads data of any size - use it for longer responses
/// HIFN This function allocates a buffer that needs to be freed afterwards
ViStatus VISAext_ReadDataUnknownLength(ViSession io, ViChar **outputBuffer, ViUInt32 *byteCount)
{
	ViStatus    status = VI_SUCCESS;
	ViUInt32    count = 0;
	ViChar      *buffer, *p2buffer;
	size_t      actualSize = 0;

	*outputBuffer = VI_NULL;

	if (byteCount)
		*byteCount = 0;

	/* Allocate Buffer */
	actualSize = CHUNK_BUFFER_SIZE;
	buffer = (ViChar *)calloc(actualSize, sizeof(ViChar));

	p2buffer = buffer;

	do
	{
		status = viRead(io, (ViPBuf)p2buffer, CHUNK_BUFFER_SIZE, &count);
		if (status < 0)
		{
			free(buffer);
			*outputBuffer = VI_NULL;

			return status;
		}

		if (count == CHUNK_BUFFER_SIZE) /* Buffer is small, reallocate it */
		{
			actualSize += CHUNK_BUFFER_SIZE;
			buffer = (ViChar *)realloc(buffer, actualSize);
			p2buffer = buffer + (actualSize - CHUNK_BUFFER_SIZE); /* Set pointer to end of data in reallocated buffer */
		}
		else
		{
			*(p2buffer + count) = '\0'; /* To be safe, set end of string at the end of data */
		}

	} while (status == VI_SUCCESS_MAX_CNT); /* if buffer was small, continue with next read */

	*outputBuffer = buffer;

	if (byteCount != NULL)
		*byteCount = (ViUInt32)((actualSize - CHUNK_BUFFER_SIZE) + count);

	return status;
}

//------------------------------------------------------------------------------------
/// HIFN Reads the list of doubles that are separated by comma
ViStatus VISAext_ReadListOfDoubles(ViSession io, double *values, int maxCount, int *actualCount)
{
	ViStatus status = VI_SUCCESS;
	ViChar* dataBuffer = NULL;
	ViUInt32 byteCount;
	int i = 0;
	char *pointer;
	
	CHECKERR(VISAext_ReadDataUnknownLength(io, &dataBuffer, &byteCount))
	pointer = strtok(dataBuffer, ",");
	while (pointer != NULL)
	{
		if (i < maxCount)
			*(values + i) = atof(pointer);
		i++;
		pointer = strtok(NULL, ",");
	}

	*actualCount = i;

ERROR_HANDLING:
	return status;
}

//------------------------------------------------------------------------------------
/// HIFN Parses binary data response block header to find out the length of the binary data
ViStatus VISAext_ParseBinaryBlockHeader(ViSession io, ViUInt32 *dataSize)
{
	ViStatus    status = VI_SUCCESS;
	ViChar      buffer[VISAEXT_BUFFER_SIZE];
	ViUInt32	count;
	int			lenOfLen;

	// Read Hash
	CHECKERR(viRead(io, (ViBuf)buffer, 1, &count));
	if (buffer[0] != '#') CHECKERR(VISAEXT_ERR_UNEXPECTED_RESPONSE);
	
	// Read length of length
	CHECKERR(viRead(io, (ViBuf)buffer, 1, &count));
	buffer[1] = '\0';
	lenOfLen = atoi(buffer);

	// Read length
	CHECKERR(viRead(io, (ViBuf)buffer, lenOfLen, &count));
	buffer[count] = '\0';
	*dataSize = (ViUInt32)atol(buffer);

ERROR_HANDLING:
	return status;
}

//------------------------------------------------------------------------------------
/// HIFN Reads binary data block
ViStatus VISAext_ReadBinaryDataBlock(ViSession io, char **outputBuffer, unsigned int *byteCount)
{
	ViStatus    status = VI_SUCCESS;
	ViUInt32    count;
	ViUInt32	dataSize;
	static ViChar  buffer[VISAEXT_BUFFER_SIZE];

	*outputBuffer = VI_NULL;

	CHECKERR(VISAext_ParseBinaryBlockHeader(io, &dataSize));
	*outputBuffer = (ViChar*)malloc(dataSize);
	// Disable termination character
	CHECKERR(viSetAttribute(io, VI_ATTR_TERMCHAR_EN, VI_FALSE));
	
	// Read useful data
	CHECKERR(viRead(io, (ViBuf)*outputBuffer, dataSize, &count));
	*byteCount = count;

	if (count != (ViInt32)dataSize) CHECKERR(VISAEXT_ERR_UNEXPECTED_RESPONSE);

	if (status == VI_SUCCESS_MAX_CNT) //More data available, read it as junk
	{
		CHECKERR(viRead(io, (ViBuf)buffer, VISAEXT_BUFFER_SIZE, &count));
	}

ERROR_HANDLING:
	viSetAttribute(io, VI_ATTR_TERMCHAR_EN, VI_TRUE);
	return status;
}

//------------------------------------------------------------------------------------
/// HIFN Reads binary data block and interprets it as 4-byte/number array
ViStatus VISAext_ReadBinaryFloatData(ViSession io, int swapEndianess, double *values, int maxCount, int *actualCount)
{
	ViStatus status = VI_SUCCESS;
	static unsigned char bytes[4];
	char *buffer;
	unsigned int byteCount = 0;
	
	int conversionCount = 0;
	int i;
	int number;

	CHECKERR(VISAext_ReadBinaryDataBlock(io, &buffer, &byteCount));
	*actualCount = (int)(byteCount / 4);
	
	conversionCount = (maxCount > *actualCount) ? *actualCount : maxCount;
	
	if (swapEndianess == 0)
	{
		float *numberPointer = (float*)buffer;
		//convert all the values, no endianess swap
		for (i = 0; i < conversionCount; i++)
			*values++ = (ViReal64)*numberPointer++;
	}
	else
	{
		int *numberPointer = (int*)buffer;
		//convert all the values, endianess swap
		for (i = 0; i < conversionCount; i++)
		{
			number = (int)*numberPointer++;
			bytes[0] = (number >> 24) & 0xff; // move byte 3 to byte 0
			bytes[1] = (number >> 16) & 0xff;
			bytes[2] = (number >> 8) & 0xff;
			bytes[3] = (number) & 0xff;
			*values++ = (ViReal64)*bytes;
		}
	}

ERROR_HANDLING:
	if (buffer) free(buffer);
	return status;
}

//------------------------------------------------------------------------------------
/// HIFN Reads the instrument response to a file
ViStatus VISAext_ReadBinaryDataToFile(ViSession io, const char *PCfilePath)
{
	ViStatus status = VI_SUCCESS;
	char *buffer = NULL;
	FILE *file = NULL;
	unsigned int count;
	
	if ((file = fopen(PCfilePath, "wb")) == NULL)
		return VI_ERROR_FILE_ACCESS;

	CHECKERR(VISAext_ReadBinaryDataBlock(io, &buffer, &count))
	fwrite(buffer, sizeof(char), (size_t)count, file);
	
ERROR_HANDLING:
	if (file) fclose(file);
	if (buffer) free(buffer);
	return status;
}

//------------------------------------------------------------------------------------
/// HIFN Sends a command synchronised with the STB polling mechanism
ViStatus VISAext_WriteWithSTBpollSync(ViSession io, const char *command, int timeout)
{
    ViStatus status = VI_SUCCESS;
    int esr;
    char completeCmd[VISAEXT_BUFFER_SIZE];

    CHECKERR(VISAext_QueryInteger(io, "*ESR?", &esr)); //Clear the Event Status Register
    sprintf(completeCmd, "%s;*OPC", command);
    CHECKERR(VISAext_Write(io, completeCmd));
    CHECKERR(VISAext_STBpolling(io, timeout));
    CHECKERR(VISAext_QueryInteger(io, "*ESR?", &esr)); //Clear the Event Status Register

ERROR_HANDLING:
    return status;
}

//------------------------------------------------------------------------------------
/// HIFN Sends a query synchronised with the STB polling mechanism
ViStatus VISAext_QueryWithSTBpollSync(ViSession io, const char *command, int timeout, char *response)
{
    ViStatus status = VI_SUCCESS;
    int esr;
    char completeCmd[VISAEXT_BUFFER_SIZE];

    CHECKERR(VISAext_QueryInteger(io, "*ESR?", &esr)); //Clear the Event Status Register
    sprintf(completeCmd, "%s;*OPC", command);
    CHECKERR(VISAext_Write(io, completeCmd));
    CHECKERR(VISAext_STBpolling(io, timeout));
    CHECKERR(VISAext_Read(io, response));
    CHECKERR(VISAext_QueryInteger(io, "*ESR?", &esr)); //Clear the Event Status Register

ERROR_HANDLING:
    return status;
}

//------------------------------------------------------------------------------------
/// HIFN Sends a command synchronised with SRQ synchronization mechanism
ViStatus VISAext_WriteWithSRQsync(ViSession io, const char *command, int timeout)
{
    ViStatus status = VI_SUCCESS;
    int esr;
    char completeCmd[VISAEXT_BUFFER_SIZE];

    CHECKERR(VISAext_QueryInteger(io, "*ESR?", &esr)); //Clear the Event Status Register
    sprintf(completeCmd, "%s;*OPC", command);
    CHECKERR(viDiscardEvents(io, VI_EVENT_SERVICE_REQ, VI_QUEUE | VI_SUSPEND_HNDLR));
    CHECKERR(viEnableEvent(io, VI_EVENT_SERVICE_REQ, VI_QUEUE, VI_NULL));
    CHECKERR(VISAext_Write(io, completeCmd));
    CHECKERR(viWaitOnEvent (io, VI_EVENT_SERVICE_REQ, timeout, VI_NULL, VI_NULL));
    CHECKERR(viDisableEvent(io, VI_EVENT_SERVICE_REQ, VI_QUEUE));
    CHECKERR(VISAext_QueryInteger(io, "*ESR?", &esr)); //Clear the Event Status Register

ERROR_HANDLING:
    return status;
}

//------------------------------------------------------------------------------------
/// HIFN Sends a query synchronised with SRQ synchronization mechanism
ViStatus VISAext_QueryWithSRQsync(ViSession io, const char *command, int timeout, char *response)
{
    ViStatus status = VI_SUCCESS;
    int esr;
    char completeCmd[VISAEXT_BUFFER_SIZE];

    CHECKERR(VISAext_QueryInteger(io, "*ESR?", &esr)); //Clear the Event Status Register
    sprintf(completeCmd, "%s;*OPC", command);
    CHECKERR(viDiscardEvents(io, VI_EVENT_SERVICE_REQ, VI_QUEUE | VI_SUSPEND_HNDLR));
    CHECKERR(viEnableEvent(io, VI_EVENT_SERVICE_REQ, VI_QUEUE, VI_NULL));
    CHECKERR(VISAext_Write(io, completeCmd));
    CHECKERR(viWaitOnEvent (io, VI_EVENT_SERVICE_REQ, timeout, VI_NULL, VI_NULL));
    CHECKERR(viDisableEvent(io, VI_EVENT_SERVICE_REQ, VI_QUEUE));
    CHECKERR(VISAext_Read(io, response));
    CHECKERR(VISAext_QueryInteger(io, "*ESR?", &esr)); //Clear the Event Status Register

ERROR_HANDLING:
    return status;
}

//------------------------------------------------------------------------------------
/// HIFN Internal function that makes the Status byte polling
ViStatus VISAext_STBpolling(ViSession io, int timeout)
{
    ViStatus status = VI_SUCCESS;
    ViUInt16 stb;
    double elapsedTime;
    static double msDivider = (double)CLOCKS_PER_SEC / 1000.0;
    clock_t startTime = clock();

    // STB polling loop
    do
    {
        CHECKERR(viReadSTB (io, &stb));
    	if ((stb & 32) != 0)
    		break;
        
		elapsedTime = (double)(clock() - startTime)/msDivider;
		if (elapsedTime > timeout)
            return VISAEXT_ERR_STBPOLL_TIMEOUT;
		
		if (elapsedTime < 10.0)
		{/* Do it as fast as possible, therefore no Sleep */
		}
		else if (elapsedTime < 100.0)
            Sleep (1);
		else if (elapsedTime < 1000.0)
            Sleep (10);
		else if (elapsedTime < 5000.0)
            Sleep (50);
		else if (elapsedTime < 10000.0)
            Sleep (100);
		else if (elapsedTime < 30000.0)
            Sleep (500);
		else
			Sleep (1000);
        
    } while (1);

ERROR_HANDLING:
    return status;
}

//------------------------------------------------------------------------------------
/// HIFN Sends a command with the registration of the service request handler
ViStatus VISAext_WriteWithSRQevent(ViSession io, const char *command, ViHndlr eventHandler)
{
    ViStatus status = VI_SUCCESS;
    int esr;
    char completeCmd[VISAEXT_BUFFER_SIZE];

    CHECKERR(VISAext_QueryInteger(io, "*ESR?", &esr)); //Clear the Event Status Register
    viUninstallHandler(io, VI_EVENT_SERVICE_REQ, eventHandler, 0); // uninstall even if it does not exist
    CHECKERR(viInstallHandler(io, VI_EVENT_SERVICE_REQ, eventHandler, 0)); //uninstall again, this prevents double registrations
    CHECKERR(viEnableEvent (io, VI_EVENT_SERVICE_REQ, VI_HNDLR, VI_NULL));
    
    sprintf(completeCmd, "%s;*OPC", command);
    CHECKERR(VISAext_Write(io, completeCmd));
    
ERROR_HANDLING:
    return status;
}
