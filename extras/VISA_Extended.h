//Exported functions for VISA_Extended library

#pragma once

#include "visa.h"
#include "visatype.h"

#define VISAEXT_BUFFER_SIZE 4096
#define VISAEXT_ERR_INSTRUMENT_STATUS 0xBFFC09F0L
#define VISAEXT_ERR_UNEXPECTED_RESPONSE 0xBFFC0059L
#define VISAEXT_ERR_STBPOLL_TIMEOUT 0xBFFC0058L



ViStatus VISAext_Open(const char *resource, ViSession *io);
ViStatus VISAext_Write(ViSession io, const char *command);
ViStatus VISAext_Read(ViSession io, char *response);
ViStatus VISAext_ReadLongString(ViSession io, char *response, int maxResponseSize);
ViStatus VISAext_Query(ViSession io, const char *command, char *response);
ViStatus VISAext_QueryInteger(ViSession io, const char *command, int *response);
ViStatus VISAext_QueryDouble(ViSession io, const char *command, double *response);
ViStatus VISAext_Close(ViSession io);
ViStatus VISAext_SetTimeout(ViSession io, int tout);
ViStatus VISAext_GetTimeout(ViSession io, int *tout);
ViStatus VISAext_ErrorChecking(ViSession io, char *errors);
ViStatus VISAext_ReadErrorQueue(ViSession io, char *errors);
ViStatus VISAext_ClearStatus(ViSession io);
ViStatus VISAext_ErrorCodeDescription(ViSession io, ViStatus errorCode, char *description);
ViStatus VISAext_ReadListOfDoubles(ViSession io, double *values, int maxCount, int *actualCount);
ViStatus VISAext_ReadBinaryDataBlock(ViSession io, char **outputBuffer, unsigned int *byteCount);
ViStatus VISAext_ReadBinaryFloatData(ViSession io, int swapEndianess, double *values, int maxCount, int *actualCount);
ViStatus VISAext_ReadBinaryDataToFile(ViSession io, const char *PCfilePath);
ViStatus VISAext_WriteWithSTBpollSync(ViSession io, const char *command, int timeout);
ViStatus VISAext_QueryWithSTBpollSync(ViSession io, const char *command, int timeout, char *response);
ViStatus VISAext_WriteWithSRQsync(ViSession io, const char *command, int timeout);
ViStatus VISAext_QueryWithSRQsync(ViSession io, const char *command, int timeout, char *response);
ViStatus VISAext_WriteWithSRQevent(ViSession io, const char *command, ViHndlr eventHandler);
