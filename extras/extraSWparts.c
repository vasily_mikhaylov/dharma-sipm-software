/****************************************************/
/****************************************************/
/*******           extra SW commands          *******/
/*******   no longer needed in main code      *******/ 
/*******   or to be implemented in future     *******/
/****************************************************/
/****************************************************/

/***************************************/
/********  Analysis FUNCTIONS  *********/
/********        BEGIN         *********/
/***************************************/

/************* At start of code **********************/
int NTAB_ANA = 5;
int strtype = 0,avstart,avstop,erel, area; 

/************* At start of measuremrnt **********************/
if (mode == NTAB_ANA || mode == NTAB_DE)
	
/************* At ClearGraph **********************/ 	
	if (mode == NTAB_ANA) tabid = TAB_AN_GRAPH;

/************* In SaveConfig  **********************/
	GetPanelHandleFromTabPage(pnlvar, PNL_TAB, NTAB_ANA, &tabhandle);
	GetCtrlVal (tabhandle, TAB_AN_STRTYPE, &strtype);
	GetCtrlVal (tabhandle, TAB_AN_STARTV, &avstart);
	GetCtrlVal (tabhandle, TAB_AN_STOPV, &avstop);
	GetCtrlVal (tabhandle, TAB_AN_RELPERM, &erel);
	GetCtrlVal (tabhandle, TAB_AN_AREA, &area);
	fprintf(fpp,"%d %lf %lf %lf %lf\n",strtype,avstart,avstop,erel,area);
	
/************* In LoadConfig  **********************/
	fscanf(fpp,"%d %lf %lf %lf %lf\n",&strtype,&avstart,&avstop,&erel,&area);
	GetPanelHandleFromTabPage(pnlvar, PNL_TAB, NTAB_ANA, &tabhandle);
		SetCtrlVal (tabhandle, TAB_AN_STRTYPE, strtype);
		StrTypeCng();
		SetCtrlVal (tabhandle, TAB_AN_STARTV, avstart);
		SetCtrlVal (tabhandle, TAB_AN_STOPV, avstop);
		SetCtrlVal (tabhandle, TAB_AN_RELPERM, erel);
		SetCtrlVal (tabhandle, TAB_AN_AREA, area);
		
		/************* In Dataload  **********************/
		else if (mode==NTAB_ANA) 
			{
				i = 0;
				while (!feof(datfile)) 
				{
					
					fscanf (datfile,"%le,%le,%le,%le,%le,%le,%le,",&x1,&temp,&volt,&x2,&cap,&x3,&x4);
					if (volt >= 0 && strtype == 1) PlotPoint (tabhandle, TAB_AN_GRAPH, volt, cap*(1.0E+12), VAL_SOLID_CIRCLE,VAL_GREEN);
					if (strtype != 1) PlotPoint (tabhandle, TAB_AN_GRAPH, volt, cap*(1.0E+12), VAL_SOLID_CIRCLE,VAL_GREEN);
					
					cvdat[0][i] = cap;
					cvdat[1][i] = volt;
					i++;
				}
				elnum = i++;
				if(elnum==1000)
				{
					SetCtrlVal (pnlvar, PNL_STATUS, "File is large! More 1000 points Stop..."); 
					break;
				}
			}
			
			if(datfile!=NULL) fclose(datfile);
		}


/***************   Vfn calculation     ******************/
/*************** using 2nd derivative  ******************/
double Vfn_calc(double h, int istart, int iend)
{
	double Vfn, h2 = 4*h*h;
	double d2c[500];
	int i = istart;

	/******* derivatives calculation  *******/
	while (i <= iend)
	{
		d2c[i] = (cvdat[0][i+1] - 2*cvdat[0][i] + cvdat[0][i-1])/h2;
		i++;
	}

	/******* searching for d2==0  *******/
	i = istart+1;
	while (i <= iend)
	{
		if (d2c[i] > 0 && d2c[i-1] <= 0) break;
		i++;
	}

	Vfn = (cvdat[1][i] + cvdat[1][i-1])/2;				  

	return Vfn;
};

/*********** 1st derivative calculation  ***********/
double Derivative_1st(double h, double xinit, double xfinal)
{
	double der = (xinit - xfinal)/(2*h);
	return der;
};

/*********** Choose the silicon structure  ***********/
int StrTypeCng (void)
{
	GetPanelHandleFromTabPage(pnlvar, PNL_TAB, NTAB_ANA, &tabhandle);
	GetCtrlVal (tabhandle, TAB_AN_STRTYPE, &strtype);
	if (strtype == 0) 
	{
		SetCtrlVal (tabhandle, TAB_AN_RELPERM, 3.9);
		SetCtrlVal (tabhandle, TAB_AN_AREA, 3.14);			  
		SetCtrlAttribute (tabhandle, TAB_AN_DOX, ATTR_VISIBLE, 1);
		
		SetCtrlAttribute (tabhandle, TAB_AN_GRAPH, ATTR_ACTIVE_XAXIS, VAL_TOP_XAXIS);
		SetCtrlAttribute (tabhandle, TAB_AN_GRAPH, ATTR_XLABEL_VISIBLE, 0);
		SetCtrlAttribute (tabhandle, TAB_AN_GRAPH, ATTR_ACTIVE_YAXIS, VAL_RIGHT_YAXIS);
		SetCtrlAttribute (tabhandle, TAB_AN_GRAPH, ATTR_YLABEL_VISIBLE, 0);

		SetCtrlAttribute (tabhandle, TAB_AN_GRAPH, ATTR_ACTIVE_XAXIS, VAL_TOP_XAXIS);
		SetCtrlAttribute (tabhandle, TAB_AN_GRAPH, ATTR_XNAME, "");
		SetCtrlAttribute (tabhandle, TAB_AN_GRAPH, ATTR_ACTIVE_YAXIS, VAL_RIGHT_YAXIS);
		SetCtrlAttribute (tabhandle, TAB_AN_GRAPH, ATTR_YNAME, "");

		SetCtrlAttribute (tabhandle, TAB_AN_GRAPH, ATTR_ACTIVE_XAXIS, VAL_BOTTOM_XAXIS);
		SetCtrlAttribute (tabhandle, TAB_AN_GRAPH, ATTR_ACTIVE_YAXIS, VAL_LEFT_YAXIS);

	}																				 

	if (strtype == 1) 											 
	{
		SetCtrlVal (tabhandle, TAB_AN_RELPERM, 11.9);
		SetCtrlVal (tabhandle, TAB_AN_AREA, 7.5);
		SetCtrlAttribute (tabhandle, TAB_AN_DOX, ATTR_VISIBLE, 0);
		
		SetCtrlAttribute (tabhandle, TAB_AN_GRAPH, ATTR_ACTIVE_XAXIS, VAL_TOP_XAXIS);
		SetCtrlAttribute (tabhandle, TAB_AN_GRAPH, ATTR_XLABEL_VISIBLE, 1);
		SetCtrlAttribute (tabhandle, TAB_AN_GRAPH, ATTR_ACTIVE_YAXIS, VAL_RIGHT_YAXIS);
		SetCtrlAttribute (tabhandle, TAB_AN_GRAPH, ATTR_YLABEL_VISIBLE, 1);
		
		SetCtrlAttribute (tabhandle, TAB_AN_GRAPH, ATTR_ACTIVE_XAXIS, VAL_TOP_XAXIS);
		SetCtrlAttribute (tabhandle, TAB_AN_GRAPH, ATTR_XNAME, "Depletion region, um");
		SetCtrlAttribute (tabhandle, TAB_AN_GRAPH, ATTR_ACTIVE_YAXIS, VAL_RIGHT_YAXIS);
		SetCtrlAttribute (tabhandle, TAB_AN_GRAPH, ATTR_YNAME, "Donor concentration,E+18 m^-3");
		
		SetCtrlAttribute (tabhandle, TAB_AN_GRAPH, ATTR_ACTIVE_XAXIS, VAL_BOTTOM_XAXIS);
		SetCtrlAttribute (tabhandle, TAB_AN_GRAPH, ATTR_ACTIVE_YAXIS, VAL_LEFT_YAXIS);
	}
	if (strtype == 2) 
	{
		
	}
	
	return 0;
}		

/*********** Calculate flatband voltage  ***********/
int CVICALLBACK VfnCalc (int panel, int control, int event,
void *callbackData, int eventData1, int eventData2)
{
	int istart, istop, i;
	double vstart, vstop, vfn, h, cfn;
	
	switch (event)
	{
	case EVENT_COMMIT:
		GetPanelHandleFromTabPage(pnlvar, PNL_TAB, NTAB_ANA, &tabhandle);
		GetCtrlVal (tabhandle, TAB_AN_STARTV, &vstart);
		GetCtrlVal (tabhandle, TAB_AN_STOPV, &vstop);

		i = 0;
		istart = 0;
		if (vstart >= 0) while (cvdat[1][i] < vstart)
		{
			i++;
			istart = i;
		}
		if (vstart < 0) while (cvdat[1][i] > vstart)
		{
			i++;
			istart = i;
		}
		
		i = 0;
		istop = 0;
		if (vstop >= 0) while (cvdat[1][i] < vstop)
		{
			i++;						  
			istop = i;
		}
		if (vstop < 0) while (cvdat[1][i] > vstop)
		{
			i++;						  
			istop = i;
		}
		
		if (istop < istart) 
		{
			i = istart;
			istart = istop;
			istop = i;
		}
		
		DeleteGraphPlot (tabhandle, TAB_AN_GRAPH, -1, VAL_IMMEDIATE_DRAW);
		for (i=istart;i<=istop;i++)
		PlotPoint (tabhandle, TAB_AN_GRAPH, cvdat[1][i], cvdat[0][i]*(1.0E+12), VAL_SOLID_CIRCLE,VAL_GREEN);
		PlotLine (tabhandle, TAB_AN_GRAPH, cvdat[1][istart], cvdat[0][istart]*(1.0E+12), cvdat[1][istop], cvdat[0][istop]*(1.0E+12), VAL_GREEN);

		//h = cvdat[1][1] - cvdat[1][0];
		//vfn = Vfn_calc(h, istart, istop);
		
		vfn = (cvdat[1][istop] + cvdat[1][istart])/2;
		cfn = (cvdat[0][istop] + cvdat[0][istart])/2;
		PlotPoint (tabhandle, TAB_AN_GRAPH, vfn, cfn*(1.0E+12), VAL_SOLID_CIRCLE,VAL_RED);
		SetCtrlVal (tabhandle, TAB_AN_VFN, vfn);
		
		break;
	}
	return 0;
}

/*********** Calculate donor concentration, resistivity and SiO2 thickness  ***********/
int CVICALLBACK DoxCalc (int panel, int control, int event,
void *callbackData, int eventData1, int eventData2)
{																 
	int i, imaxcap, imincap, loc_elnum,mdd=0;
	double erel, area, dox, maxcap, mincap, h;
	double ndon, ndon_s[1000];  // Donor concentrations
	double px, rox_s[1000];     // Resistivities
	double w_reg0 = 18, w_reg[1000];		   // Deplet region constant and deplet regions
	double cap[1000], volt[1000];
	double evac = 8.854187817620E-12;
	double q = 1.602176565E-19;
	double uu = 0.145;					// Electron mobility u = 1450 cm^2/(V*s) [300 K]
	
	switch (event)
	{
	case EVENT_COMMIT:
		GetActiveTabPage (pnlvar, PNL_TAB, &mode);
		GetPanelHandleFromTabPage(pnlvar, PNL_TAB, mode, &tabhandle);
		GetCtrlVal (tabhandle, TAB_AN_RELPERM, &erel);
		GetCtrlVal (tabhandle, TAB_AN_AREA, &area);
		GetCtrlVal (tabhandle, TAB_AN_RBGRAPH, &mdd);
		
		area = area/(1.0E+6); 
		
		for (i=0;i<elnum;i++) 
		{
			cap[i] = cvdat[0][i];
			volt[i] = cvdat[1][i];
		}
		
		MaxMin1D (cap, elnum, &maxcap, &imaxcap, &mincap, &imincap);
		if (strtype == 0) 			   // for the MOS-structure only
		{
			dox = evac*erel*area/(maxcap*2);     
			SetCtrlVal (tabhandle, TAB_AN_DOX, dox*(1.0E+6));
			PlotPoint (tabhandle, TAB_AN_GRAPH, cvdat[1][imaxcap], maxcap*(1.0E+12), VAL_SOLID_CIRCLE, VAL_RED);
		}
		
		h = fabs( cvdat[1][imincap] - cvdat[1][imincap-1] );
		if (strtype == 0) ndon = pow(mincap*2, 3) / (q * evac * area * area * fabs( Derivative_1st(h, cap[imincap-1]*2, cap[imincap+1]*2) ) );    // for the MOS-structure
		if (strtype == 1) ndon = pow(mincap, 3) / (q * evac * area * area * fabs( Derivative_1st(h, cap[imincap-1], cap[imincap+1]) ) );			// for the PIN-structure 
		if (strtype == 2) ;									 // for the PN-structure 
		SetCtrlVal (tabhandle, TAB_AN_NDON, ndon*(1.0E-18));
		
		PlotPoint (tabhandle, TAB_AN_GRAPH, volt[imincap], mincap*(1.0E+12), VAL_SOLID_CIRCLE, VAL_RED); 
		
		px = 1 / (q * ndon * uu);
		SetCtrlVal (tabhandle, TAB_AN_PX, px*100);
		
		SetCtrlAttribute (tabhandle, TAB_AN_GRAPH, ATTR_ACTIVE_XAXIS, VAL_TOP_XAXIS);
		SetCtrlAttribute (tabhandle, TAB_AN_GRAPH, ATTR_ACTIVE_YAXIS, VAL_RIGHT_YAXIS);
		
		if (strtype == 1) 			   // for the PIN-structure only
		{
			for (i=1;i<elnum-1;i++) 	// calculation is not awaible for i=0 and i=elnum (due to derivative calculation)
			{
				ndon_s[i] = pow(cap[i], 3) / (q * evac * area * area * fabs( Derivative_1st(h, cap[i-1], cap[i+1]) ) );
				rox_s[i] = 1 / (q * ndon_s[i] * uu);
				if (volt[i] >= 0) 
				{
					w_reg[i] = w_reg0 * sqrt( (rox_s[i] / 10) * volt[i]);  // divided by 10 because of Ohm*m -> kOhm*cm
					PlotPoint (tabhandle, TAB_AN_GRAPH, w_reg[i], ndon_s[i]*(1E-18), VAL_BOLD_X, VAL_RED);
				}
			}
			
			
		}
		
		SetCtrlAttribute (tabhandle, TAB_AN_GRAPH, ATTR_ACTIVE_XAXIS, VAL_BOTTOM_XAXIS);
		SetCtrlAttribute (tabhandle, TAB_AN_GRAPH, ATTR_ACTIVE_YAXIS, VAL_LEFT_YAXIS);
		
		break;
	}
	return 0;
}

/*********** Choose the silicon structure: callback  ***********/
int CVICALLBACK StrTypeChange (int panel, int control, int event,
void *callbackData, int eventData1, int eventData2)
{
	switch (event)										
	{
	case EVENT_COMMIT:
		StrTypeCng();
		break;
	}
	return 0;
}

/*********** Calculate hysteresis charge  ***********/
int CVICALLBACK HystChargeCalc (int panel, int control, int event,
void *callbackData, int eventData1, int eventData2)
{
	int i;
	double h, vleft, vright;
	double hystcharge, sum1 = 0, sum2 = 0; // Charge of hysteresis
	double cap[500], volt[500];

	switch (event)
	{
	case EVENT_COMMIT:
		GetPanelHandleFromTabPage(pnlvar, PNL_TAB, NTAB_ANA, &tabhandle);
		GetCtrlVal (tabhandle, TAB_AN_VLEFT, &vleft);
		GetCtrlVal (tabhandle, TAB_AN_VRIGHT, &vright);
		GetCtrlVal (pnlvar, PNL_HYSTERESIS, &hyst);

		if (hyst == 1)
		{
			for (i=0;i<elnum;i++) 
			{
				cap[i] = cvdat[0][i];
				volt[i] = cvdat[1][i];
			}
			
			for (i = 1; i < elnum-1; i++)
			{ 
				h = fabs(volt[i] - volt[i+1]);
				sum1 = sum1 + (cap[i-1] + 4*cap[i] + cap[i+1])*(h/6);
				if (volt[i+1] > vright) break;
			}
			
			for (i = i+1; i < elnum-1; i++)
			{ 
				if (volt[i+1] < vright) break;
			}
			
			for (i = i+1; i < elnum-1; i++)
			{ 
				h = fabs(volt[i] - volt[i+1]);
				sum2 = sum2 + (cap[i-1] + 4*cap[i] + cap[i+1])*(h/6);
				if (volt[i+1] < vleft) break;
			}
			
			for (i = i+1; i < elnum-1; i++)
			{ 
				if (volt[i+1] > vleft) break;
			}
			
			for (i = i+1; i < elnum-1; i++)
			{ 
				h = fabs(volt[i] - volt[i+1]);
				sum1 = sum1 + (cap[i-1] + 4*cap[i] + cap[i+1])*(h/6);
			}
			
			hystcharge = sum1 - sum2;
			SetCtrlVal (tabhandle, TAB_AN_HCHARGE, hystcharge*(1.0E+12));
		}
		break;
	}
	return 0;
}


/***************************************/
/********  Analysis FUNCTIONS  *********/
/********         END          *********/
/***************************************/



/*****************************************************/
/************* V-step adjustement for IV **********************/

/************* At start of code **********************/
int adjust=0, addnumb=0;
double preval=0;

/************* Inside IV **********************/
					  /******************* Adjust step of bias voltage **********************/
					  GetCtrlVal (pnlvar, PNL_ADJUST, &adjust);
					  if(adjust==1)
					  {
					  if(fvoltage<0) fvoltage=-fvoltage;
					  if(fvoltage>0.2) K6517_CurrentAutoRange(comport, K6517A, 1);
						  
					 
					  if(i>5 && (fvoltage-preval)>0 && enditter==0)
					  {
					   	 incr=(fvoltage-preval)/preval; 
						 
						 if(incr>0.5)
						 {
						   K6517A_SetVout(comport,K6517A, vc-i*vstep); 
						   vstep=vstep/2;
						   if(vstep<0.01) 
						   {
						    vstep=0.01;
							enditter=1;
						    }
						   addnumb=addnumb+1;
						   //preval=fvoltage;
						   goto nxt_try;
						  }
						 
					  }
					  if(addnumb>0)
					  {
						ns=ns+(int)(addnumb^2)*(ns-i); 
						if(ns>499)
						{
						 SetCtrlVal (pnlvar, PNL_STATUS, "Limit overload!!!");
						 ns=499;
						 }
						ClearBuff(buf,100);
						sprintf(&buf[0],"Add %i for %i points",addnumb,ns-i);
						SetCtrlVal (tabhandle, TAB_IV_STEPVOLTAGE, vstep);
						SetCtrlVal (pnlvar, PNL_STATUS, buf); 
						addnumb=0;
						
					  }
					  preval=fvoltage;
					  } 
/****************************************************/


/******* Test for user event  *****************/  
int TestEvent(int panel, int ctrl)
{
 GetUserEvent (0, &eventPanel, &eventCtrl);
 //sprintf(message,"Event: %i %i",usrEvent,usrCtrl); 
 //SetCtrlVal (RADMETER,RADMETER_STR_TIME,message);
 if(panel==eventPanel && ctrl==eventCtrl) return 1;
 return 0;
};					  
					  

int CVICALLBACK FileNameRecord (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	
	int l=-1;
	ClearBuff(buf,100);  
	switch (event)
	{
		case EVENT_COMMIT:
			    GetCtrlVal (pnlvar, PNL_DATFILENAME, datname);
				l=strncmp(&datname[strlen(datname)-4],".dat",4);
				if(l!=0)
				{
				 sprintf(logname,"%s.log",datname);
				 strncat(datname,".dat",4);
				}
				SetCtrlVal (pnlvar, PNL_DATFILENAME, datname);
				SetCtrlVal (pnlvar, PNL_LOGFILENAME, logname);
				sprintf(buf,"New file name:%s",datname);
				SetCtrlVal (pnlvar, PNL_STATUS, buf);  
			break;
	}
	return 0;
}


int CVICALLBACK SetReadTimeout (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:
			
			if (popen!=0)popen= InitGPIB();
			if (popen==0) 
			{
				GetCtrlVal (pnlvar, PNL_READTIMEOUT, &readtimeout);
				if (GPIBMODE == 0 && readtimeout >= 0) 
				{
					SetComTime (comport, readtimeout);
	  				sprintf (buf, "COM%i-port timeout is set to %.3f sec", comport, readtimeout);   //upgrade for 2d..
				}
				else if (GPIBMODE == 1 && readtimeout >= 0)
				{
					
				}
				SetCtrlVal (pnlvar, PNL_STATUS, buf);
			}
			 

			break;
	}
	return 0;
}

