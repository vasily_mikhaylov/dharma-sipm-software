/****************************************************/
/****************************************************/
/*******            old HW commands           *******/
/*******   no longer needed in main code      *******/ 
/****************************************************/
/****************************************************/

/************* At start of code **********************/
int panelHandle, ctrl, eventPanel, eventCtrl, addnumb=0, mainerr, 
char cmd[1024], buf[1024], dat[1024], buferr[6], buferr2[2];
double vc=0;


/*  Setup a device for data output  by USB-GPIB with the termination byte */
int USBGPIBenterTerm (int comport, int device, char *buf, int nm)
{
  int bn=0,err=0;
  char comd[40];
  
  strcpy(buf,"");
  
  /* New CPIB addr */  
  sprintf(comd,"IBC%c\n",(char)(device+64));
  bn = ComWrt (comport, comd, strlen(comd));
  err = ComRdByte (comport);
  //Delay(0.10);
  //Delay(0.5); 
  //printf("GPIB Readdressing [0x%X]\n",err); 
  //FlushOutQ (comport); 
  
  /* Read CPIB */  
  bn = ComWrt (comport, "IB?\n", 4);
  err = ComRdByte (comport);
  //Delay(0.5); 
  //printf("GPIB READ command [0x%X]\n",err); 
  bn = ComRdTerm (comport, buf, nm, 16);
  //if (bn != nm)  printf("GPIB READ timeout %i\n",err);  

  //FlushOutQ (comport); 
  
  //ComWrt (comport, "\n", 1); 
  return err;	
};

/*  Send a command to device test */
int USBGPIBsendTest (int comport, int device, char *cmd)
{
  int bn=0,err=0;
  char comd[40];
  
  /* Unlistner CMD */
  bn = ComWrt (comport, "IBc?\n", 5);
  err = ComRdByte (comport);
  //Delay(0.5);
  //printf("GPIB Unlistner [0x%X]\n",err); 
  //FlushOutQ (comport); 
  
  /* New CPIB addr */  
  sprintf(comd,"IBC%c\n",(char)(device+32));
  bn = ComWrt (comport, comd, strlen(comd));
  err = ComRdByte (comport); 
  //Delay(0.5); 
  //printf("GPIB Readdressing [0x%X]\n",err); 
  //FlushOutQ (comport); 
  
  /* New CPIB cmd */  
  sprintf(comd,"IB%c%c%s%c%c\n",0x10,0x02,cmd,0x10,0x03);
  bn = ComWrt (comport, comd, strlen(comd));
  err = ComRdByte (comport);
  //Delay(0.5); 
  //printf("GPIB [ %s ] command [0x%X]\n",comd,err); 
  //FlushOutQ (comport); 
  
  //ComWrt (comport, "\n", 1); 
  //getchar(); 
  return err;	
};

/*  Setup a device for data output test */
int USBGPIBenterTest (int comport, int device, char *buf, int nm)
{
  int bn=0,err=0;
  char comd[40];
  
  strcpy(buf,"");
  
  /* New CPIB addr */  
  sprintf(comd,"IBC%c\n",(char)(device+64));
  bn = ComWrt (comport, comd, strlen(comd));
  err = ComRdByte (comport);
  //Delay(0.10);
  //Delay(0.5); 
  //printf("GPIB Readdressing [0x%X]\n",err); 
  //FlushOutQ (comport); 
  
  /* Read CPIB */  
  bn = ComWrt (comport, "IB?\n", 4);
  err = ComRdByte (comport);
  //SyncWaitLoop(5);
  //Delay(0.5); 
  //printf("GPIB READ command [0x%X]\n",err); 
  bn = ComRd (comport, buf, 4);
  //if (bn != nm)  printf("GPIB READ timeout %i\n",err);  

  //FlushOutQ (comport); 
  
  //ComWrt (comport, "\n", 1); 
  //getchar(); 
  return err;	
};

/* Go to LOCAL */
int USBGPIBGoToLocal(int comport, int device)
{
  int err;
  char comd[10]; 
  /* New CPIB addr */  
  sprintf(comd,"IBC%c\n",0x01);
  err = ComWrt (comport, comd, 4);
  //err = ComRdByte (comport);
  printf("GPIB Go To Local [0x%X]\n",err);
  return err;
};



/***************************************/
/*************** K6517A ****************/ 
/***************************************/
/**********      BEGIN      ************/

/***************** 1 ********************/
void K6517A_TempMeas(int comport, int devnum)
{
 USBGPIBsend(comport, devnum, ":syst:tsc on");  		// Temp meas ON
 USBGPIBsend(comport, devnum, ":form:elem read,etem");  // Temp incl. in DATA
};


/***************** 2 ********************/
int K6517A_GetI(int comport, int devnum, char *I)
{
 char buf[26];
 USBGPIBsend (comport, devnum,":syst:zch off");  	   /* Zero Check OFF*/ 
 USBGPIBsend (comport, devnum,"func 'curr:dc';:read?"); 
 USBGPIBenter (comport, devnum, buf, 26);
 strncpy(I,&buf[1],21);
 return 0;
};


/***************** 3 ********************/
/* must be V[22] */
int K6517A_GetV(int comport, int devnum, char *cv)
{
 char buf[30],V[14];
 ClearBuff(V,14);
 USBGPIBsend (comport, devnum,":syst:zch off");  	   /* Zero Check OFF*/ 
 USBGPIBsend (comport, devnum,"func 'volt:dc';:read?"); 
 USBGPIBenter (comport, devnum, buf, 26);
 strncpy(V,&buf[1],14);
 cv=&V[0];
 V[13]=0;
 printf("K6517A VDC=%s\n",V);
 return 0;
};


/***************** 0 V out Ctrl ********************/
int K6517A_SetVout(int comport, int devnum, float V)
{
 char cmd[40];
 ClearBuff(cmd,40);
 sprintf(cmd,":sour:volt %f",V);
 USBGPIBsend (comport, devnum, cmd);
 return 0;
};


/***************** 1 V out Ctrl ********************/
int K6517A_SetVoutMAX(int comport, int devnum, float V)
{
 char cmd[40];
 ClearBuff(cmd,40);
 sprintf(cmd,":sour:volt:range %f",V);
 USBGPIBsend (comport, devnum, cmd);
 return 0;
};


/***************** 2 V out Ctrl ********************/
int K6517A_SetVoutON(int comport, int devnum)
{
 USBGPIBsend (comport, devnum, ":outp:stat on");
 return 0;
};


/***************** 3 V out Ctrl ********************/
int K6517A_SetVoutOFF(int comport, int devnum)
{
 USBGPIBsend (comport, devnum, ":outp:stat off");
 return 0;
};


/***************** 4 ********************/  
int K6517_MeasureIT(int comport, int devnum, double *xx, double *tt)
{
 unsigned char buf[100],*bufptr;
 unsigned char bt[50],btm[50];
 double x,t;
 int i,leng=0,a=0;
 
 //USBGPIBsend (comport, devnum,"*rst");
 //K6517A_TempMeas(comport, devnum);
 USBGPIBsend (comport, devnum,":syst:zch off");  	   /* Zero Check OFF*/ 
 Delay(1.5);	 // 0.5 changed to 1.5
 //USBGPIBenter (comport, devnum, buf, 100); 
 ClearBuff(buf,100);
 USBGPIBsend (comport, devnum,"FUNC 'CURR:DC';:READ?"); 
 Delay(1.5);
 //a=ibrd (m, buf, 100);  /* Read Data to PC */
 USBGPIBenter (comport, devnum, buf, 100); 
 //while((buf[a]!=43) & (buf[a]!=45)) a=a+1;
 bufptr=&buf[1];
 
  Scan (bufptr,"%s>%s[t44]",btm);
  leng=(int)strlen(btm);
  bufptr=bufptr+leng+1;
  
  Scan (bufptr,"%s>%s[t10]",bt);
  leng=(int)strlen(bt);
  bufptr=bufptr+leng+1;
  
  Scan (bt,"%s>%f",&t);     /* READ TEMPERATURE */
  *tt=t;
  Fmt(btm,"%s[a]<\tT=%s",bt);
  Scan (btm,"%s>%f",&x);    /* READ X? */ 
  *xx=x;
  USBGPIBsend (comport, devnum,":syst:zch on");  	   /* Zero Check ON*/
 return 0;
};


/***************** 5 ********************/  
int K6517_MeasureVT(int comport, int devnum, double *xx, double *tt)
{
 unsigned char buf[100],*bufptr;
 unsigned char bt[50],btm[50];
 double x,t;
 int i,leng=0,a=0;
 
 //USBGPIBsend (comport, devnum,"*rst");
 //K6517A_TempMeas(comport, devnum);
 USBGPIBsend (comport, devnum,":syst:zch off");  	   /* Zero Check OFF*/ 
 Delay(1.5);	// 0.5 changed to 1.5
 //USBGPIBenter (comport, devnum, buf, 100); 
 ClearBuff(buf,100);
 USBGPIBsend (comport, devnum,"FUNC 'VOLT:DC';:READ?"); 
 Delay(1.5);
 //a=ibrd (m, buf, 100);  /* Read Data to PC */
 USBGPIBenter (comport, devnum, buf, 100); 
 //while((buf[a]!=43) & (buf[a]!=45)) a=a+1;
 
  i=0;
  while(i<100)
  {
   if((buf[i]==0x2B) || (buf[i]==0x2D)) break;
   i=i+1;
  }

   bufptr=&buf[i];
   Scan (bufptr,"%s>%s[t44]",btm);
   leng=(int)strlen(btm);
   bufptr=bufptr+leng+1;
  
  Scan (bufptr,"%s>%s[t10]",bt);
  leng=(int)strlen(bt);
  bufptr=bufptr+leng+1;
  
  Scan (bt,"%s>%f",&t);     /* READ TEMPERATURE */
  *tt=t;
  Fmt(btm,"%s[a]<\tT=%s",bt);
  Scan (btm,"%s>%f",&x);    /* READ X */ 
  *xx=x;
  USBGPIBsend (comport, devnum,":syst:zch on");  	   /* Zero Check ON*/
 return 0;
};




/************************/
/* Current Measurement  */
/************************/
/*-------- F6 -----------*/  
int K6517_CurrentAutoRange(int comport, int devnum, int sw) 
{
 char comd[40];
 Fmt(comd,"%s<%s",":curr:rang:auto");
 Fmt(comd,"%s[a]<%c",32); 
 Fmt(comd,"%s[a]<%i",sw);
 return USBGPIBsend (comport, devnum,comd);  	/* Set ON/OFF = 1/0   */
};


/*-------- F11-----------*/  
int K6517_SetCurrentRange(int comport, int devnum, int *rg, float val)
{
 int rang=0;
 float currmax=0.00000000002;
 char comd[40];
 
 if(val > 100) 
 {
	 if(*rg<9) *rg=*rg+1;
	 rang=*rg;
	 goto nnn;
 }
 if(val > 0.00000000002) rang=1;
 if(val > 0.0000000002) rang=2; 
 if(val > 0.000000002) rang=3; 
 if(val > 0.00000002) rang=4; 
 if(val > 0.0000002) rang=5; 
 if(val > 0.000002) rang=6; 
 if(val > 0.00002) rang=7; 
 if(val > 0.0002) rang=8; 
 if(val > 0.002) rang=9; 
 

nnn:
 switch (rang) {
	 case 0:
		 currmax=0.00000000002;
		 break;
	 case 1:
		 currmax=0.0000000002;  
		 break;
	 case 2:
		 currmax=0.000000002;  
		 break;
	 case 3:
		 currmax=0.00000002; 
		 break;
	 case 4:
		 currmax=0.0000002; 
		 break;
	 case 5:
		 currmax=0.000002; 
		 break;
	 case 6:
		 currmax=0.00002; 
		 break;
	 case 7:
		 currmax=0.0002; 
		 break;
	 case 8:
		 currmax=0.002;  
		 break;
	 case 9:
		 currmax=0.02;  
		 break;
	 default:
		
		 break;
 }
 
 *rg=rang;
 
 Fmt(comd,"%s<%s",":curr:dc:rang");
 Fmt(comd,"%s[a]<%c",32); 
 Fmt(comd,"%s[a]<%f",currmax); 
 /* RANGE SETUP */ 
 USBGPIBsend (comport, devnum,comd); 
 return currmax;
};


/****************************************/ 
/*************** K6517A *****************/ 
/****************************************/
/*************     END     **************/



/***************** 1 HIOKI 3532-50  ********************/
int HIOKI3523_GetFreq(int comport, char *freq)
{
 char buf[15];
 ClearBuff(buf,15);
 USBGPIBsend (comport, HIOKICV, "FREQ?");
 USBGPIBenter (comport, HIOKICV, buf, 15);
 strncpy(freq,&buf[1],9);
 return 0;
};

/***************** 2 HIOKI 3532-50  ********************/
int HIOKI3523_Measure(int comport, char *data, double *capp)
{
 char buf[30], *bufptr, bt[15];
 double cap;
 int i;
 ClearBuff(buf,30);
 ClearBuff(bt,15);
 USBGPIBsend (comport, HIOKICV, "MEAS?");
 USBGPIBenter (comport, HIOKICV, buf, 30);
 strncpy(data,&buf[1],23);
 
 i=0;
 while(i<30)
 {
  if(buf[i]==0x2C) 
  {
   i=i+1;
   break;
  }
  i=i+1;
 }

 bufptr=&buf[i];
 Scan (bufptr,"%s>%s[t44]",bt);
 Scan (bt,"%s>%f",&cap);
 *capp = cap;
 return 0;
};

/***************** 3 HIOKI 3532-50  ********************/
int HIOKI3523_SetFreq(int comport, double freq)
{
 char buf[15], buff[15]="FREQ ";
 ClearBuff(buf,15);
 Scan (&freq,"%f>%s",buf);
 strncpy(&buff[5],&buf[0],9);
 USBGPIBsend (comport, HIOKICV, buff);
 return 0;
};

/***************** 4 HIOKI 3532-50  ********************/
int HIOKI3523_SetVoltage(int comport, double volt)
{
 char buf[30], buff[30]=":LEV:VOLT ";
 ClearBuff(buf,30);
 Scan (&volt,"%f>%s",buf);
 strncpy(&buff[10],&buf[0],9);
 USBGPIBsend (comport, HIOKICV, buff);
 return 0;
};
			
/********************************************************/
/*************** HM8212 functions ***********************/
/********************************************************/
/*****   F1  ******/
int HM8112_SetVDC(int comport, int devnum)
{
 return USBGPIBsend (comport, devnum, "vd");
 };
 
 /*****   F2  ******/
int HM8112_SetVAC(int comport, int devnum)
{
 return USBGPIBsend (comport, devnum, "va");
 };
 
/*****   F3  ******/
int HM8112_SetIDC(int comport, int devnum)
{
 return USBGPIBsend (comport, devnum, "id");
 };
 
/*****   F4  ******/
int HM8112_SetIAC(int comport, int devnum)
{
 return USBGPIBsend (comport, devnum, "ia");
 };
 
 /*****   F5  ******/
 /* 2 wires resistanse */
int HM8112_SetR2(int comport, int devnum)
{
 return USBGPIBsend (comport, devnum, "o2");
 };
 
 /*****   F6  ******/
 /* 4 wires resistanse */
int HM8112_SetR4(int comport, int devnum)
{
 return USBGPIBsend (comport, devnum, "o4");
 };
 
 /*-------  F7  -------*/
/* Temperature  in C */
int HM8112_SetTC(int comport, int devnum)
{
  return USBGPIBsend (comport, devnum, "tc");
 };
 
 /*-------  F8  -------*/
/* Temperature  in K */
int HM8112_SetTK(int comport, int devnum)
{
  return USBGPIBsend (comport, devnum, "tk");
 };

/*-------  F9  -------*/
/* Temperature  in F */
int HM8112_SetTF(int comport, int devnum)
{
  return USBGPIBsend (comport, devnum, "tf");
 };
 
 /*-------  F10  -------*/
/* Display  ON/OFF     */
int HM8112_Display(int comport, int devnum, int sw)
{
  int a=100;
  if(sw) a=USBGPIBsend (comport, devnum, "d0");
  if(!sw) a=USBGPIBsend (comport, devnum, "d1");
  if(sw!=1 || sw!=0) a=-1;
  return a;
 };

 /*-------  F11  -------*/
/* Data format         */
int HM8112_DataFormat(int comport, int devnum, int sw)
{
  int a=100;
  if(sw) a=USBGPIBsend (comport, devnum, "l0");
  if(!sw) a=USBGPIBsend (comport, devnum, "l1");
  if(sw!=1 || sw!=0) a=-1;
  return a;
 };

/*-------  F12  -------*/
/*     Set Range       */
/*  auto ON/OFF a1/a0  */
int HM8112_Range(int comport, int devnum, int sw)
{
 int a=100;
 switch (sw) {
	 case 0:
		 a=USBGPIBsend (comport, devnum, "a0");
		 break;
	 case 1:
		 a=USBGPIBsend (comport, devnum, "r1");
		 break;
	 case 2:
		 a=USBGPIBsend (comport, devnum, "r2");
		 break;
	 case 3:
		 a=USBGPIBsend (comport, devnum, "r3");
		 break;
	 case 4:
		 a=USBGPIBsend (comport, devnum, "r4");
		 break;
	 case 5:
		 a=USBGPIBsend (comport, devnum, "r5");
		 break;
	 /* more 5 means AUTO RANGE */
	 default:
		 a=USBGPIBsend (comport, devnum, "a1");
		 break;
		 }
		return a;   
};

/*-------  F13  -------*/
/* Trigger Seting      */
int HM8112_Trigger(int comport, int devnum, int sw)
{
 int a=100;
 switch (sw) {
	 case 0:
		 a=USBGPIBsend (comport, devnum, "t0");
		 break;
	 case 1:
		 a=USBGPIBsend (comport, devnum, "t1");
		 break;
	 case 2:
		 a=USBGPIBsend (comport, devnum, "t2");
		 break;
	 case 3:
		 a=USBGPIBsend (comport, devnum, "t3");
		 break;			
	 default:
		 a=USBGPIBsend (comport, devnum, "t4");
		 break;
	}
		 return a;
};

/*-------  F14  -------*/
/* Offset  ON/OFF      */
int HM8112_Offset(int comport, int devnum, int sw)
{
 int a=100;
 if(sw) a=USBGPIBsend (comport, devnum, "p1"); 
 if(!sw) a=USBGPIBsend (comport, devnum, "p0");
 return a;
 };
 
 /*------- F15  -------*/
int HM8112_MeasureVDC(int comport, int devnum, double *xx)
{
 unsigned char buffert[80],*bufptr;
 unsigned char bt[50],btm[50];
 double x;
 int i,l;
 
 HM8112_SetVDC(comport,devnum);
 Delay(0.5);
 //while(ibsta!=0x100);
 l=USBGPIBenter (comport, devnum, buffert, 15);
 Scan(buffert,"%s>%s[dt#]%f",&x);  /* READ V? */
 *xx=x;
 #ifdef MY_DEBUG
 printf("VDC= %f\n",x);
 #endif
 return l;
};

/*------- F16  -------*/
int HM8112_MeasureVAC(int comport, int devnum, double *xx)
{
 unsigned char buffert[80],*bufptr;
 unsigned char bt[50],btm[50];
 double x;
 int i,l;
 
 HM8112_SetVAC(comport,devnum);
 Delay(0.5);
 //while(ibsta!=0x100);
 l=USBGPIBenter (comport, devnum, buffert, 15);
 Scan(buffert,"%s>%s[dt#]%f",&x);  /* READ V? */
 *xx=x;
 #ifdef MY_DEBUG
 printf("VAC= %f\n",x);
 #endif
 return l;
};

/*------- F17  -------*/
int HM8112_MeasureIDC(int comport, int devnum, double *xx)
{
 unsigned char buffert[80],*bufptr;
 unsigned char bt[50],btm[50];
 double x;
 int i,l;
 
 HM8112_SetIDC(comport,devnum);
 Delay(0.5);
 //while(ibsta!=0x100);
 l=USBGPIBenter (comport, devnum, buffert, 15);
 Scan(buffert,"%s>%s[dt#]%f",&x);  /* READ I? */
 *xx=x;
 #ifdef MY_DEBUG
 printf("IDC= %f\n",x);
 #endif
 return l;
};

/*------- F18  -------*/
int HM8112_MeasureIAC(int comport, int devnum, double *xx)
{
 unsigned char buffert[80],*bufptr;
 unsigned char bt[50],btm[50];
 double x;
 int i,l;
 
 HM8112_SetIAC(comport,devnum);
 Delay(0.5);
 //while(ibsta!=0x100);
 l=USBGPIBenter (comport, devnum, buffert, 15);
 Scan(buffert,"%s>%s[dt#]%f",&x);  /* READ I? */
 *xx=x;
 #ifdef MY_DEBUG
 printf("IAC= %f\n",x);
 #endif
 return l;
};

/*------- F19  -------*/
int HM8112_MeasureR(int comport, int devnum, int mode, double *xx)
{
 unsigned char buffert[80],*bufptr;
 unsigned char bt[50],btm[50];
 double x;
 int i,l;
 if(!mode) HM8112_SetR2(comport,devnum);
 if(mode) HM8112_SetR4(comport,devnum);
 Delay(0.5);
 //while(ibsta!=0x100);
 l=USBGPIBenter (comport, devnum, buffert, 15);
 Scan(buffert,"%s>%s[dt#]%f",&x);  /* READ R? */
 *xx=x;
 #ifdef MY_DEBUG
 printf("Method %i, R= %f\n",mode,x);
 #endif
 return l;
};

 /*------- F21  -------*/
int HM8112_MeasureT(int comport, int devnum, char mode, double *xx)
{
 unsigned char buffert[80],*bufptr;
 unsigned char bt[50],btm[50];
 double x;
 int i,l;
 if(mode=='C') HM8112_SetTC(comport,devnum);
 if(mode=='K') HM8112_SetTK(comport,devnum);
 if(mode=='F') HM8112_SetTF(comport,devnum);
 else HM8112_SetTC(comport,devnum);
 Delay(0.5);
 //while(ibsta!=0x100);
  l=USBGPIBenter (comport, devnum, buffert, 15);
 Scan(buffert,"%s>%s[dt#]%f",&x);  /* READ T? */   
 *xx=x;
 #ifdef MY_DEBUG
 printf("T= %f [%c]\n",x, mode);
 #endif
 return l;
}
/*********************************************************/
/*************** HM8212 functions end ********************/
/*********************************************************/
