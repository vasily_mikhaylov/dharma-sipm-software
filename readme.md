Dharma software v2.2.6 for tests of SiPMs, APDs, other silicon structures and more

Last updated 11.06.2021

Copyright 2021 Vasily Mikhaylov

I developed this software during my PhD research in Nuclear Spectroscopy department of Nuclear Physics Institute of Czech Academy of Sciences, public research institution located in Řež, Czech Republic. Institute webpage: http://www.ujf.cas.cz/ . 

Detailed description of the software including structure, functions and operation is provided in chapter 7, section 7.3 of my PhD thesis MikhaylovPhDthesis_final31032021.pdf. 
In case if you do not have NI LabWindows CVI software, you can use the installer package DharmaSiPM_v2.2.6_installer.zip. 
In the data_examples.zip there are several data samples acquired for the same SiPM before and after irradiation which can be used to check the software operation.

If you have any questions, please contact me at mikhaylov@ujf.cas.cz or vasily.mikhaylov@cern.ch.