/****************************************************/
/****************************************************/
/*******        Dharma software v2.2.6        *******/
/*******       for tests of SiPMs, APDs,      *******/  
/*******   other silicon structures and more  *******/
/*******     last updated 11.06.2021          *******/
/*******   Copyright 2021 Vasily Mikhaylov    *******/
/****************************************************/
/****************************************************/


/****************************************************/
// License notice
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

// Aforementioned information is related to my intellectual propery,
// namely to this source code and the user interface file.
// This program was created in National Instruments LabWindows/CVI environment.
// Therefore, other licensing terms may apply in case if you want to use it
// in form of executable or CVI project file.
// Please check the license directory in the installer package for more information.

	

//#include <windows.h>
#include <analysis.h>
#include <formatio.h>
#include <ansi_c.h>
#include <cvirte.h>		
#include <userint.h>
#include <utility.h>
#include <rs232.h>
#include <visa.h>
#include "DharmaSiPM.h"
//#include "VISA_Extended.h" // just for reference
//#include "oldHWcommands.h" // just for reference
//#include "extraSWparts.h"  // just for reference

// Check if user stopped the measurement
#ifndef CHECKSTOP
#define CHECKSTOP(newFunc) {\
	errorF = (newFunc);\
	if ((errorF) == -1) {StopMeasurement(mode,-1); break;}\
	else if ((errorF) < -1) GPIBErrorQueue(currentDevnum, currentCommand);\
	}
#endif

static int pnlvar;
const int RESP = 1, NORESP = 0, NOREDO = 0;

// Devices adresses and settings
int GPIBMODE = 1; // GPIB connection mode: 0 - USB-GPIB, 1 - NI-GPIB
const int NDEVICES = 3; // XYMOVER excludedd for now, HM8112 is not used
int USBGPIB = 0;
int K6517A = 27; double IVRANGETIME = 10.0; // Device 1
int HIOKICV = 4; // Device 2 
int RS1024 = 20, SCOPEMAINMEAS = 1, SCOPESCALEMEAS = 2, SCOPEMEASCHAN = 2; // Device 3 
int XYMOVER = 14; // Device 4 
int HM8112 = 7; // Device 5 

ViSession rmHandle=0, HM8112Handle=0, K6517AHandle=0, HIOKICVHandle=0, RS1024Handle=0;

// Tab index order
const int NTAB_CV = 0, NTAB_IV = 1, NTAB_CF = 2, NTAB_SV = 3, NTAB_SXY = 4, NTAB_DE = 5;

int errorF=0, currentDevnum=0, debugging=1, interr = 0;
int runflag = 1, outfileopened = 0, tabhandle, ntab=0, mode=-1, elnum=-1, MSEND=0, stophow = 1, RS1024attention = 0, sendMode=1;
int savlog, startcheck, voltageIsOn=0;
int comport=5, comport2D=14, comportX=-1, popen=-1, popen2D=-1, popenX=-1, rate2D=115200, devcheckedAll = 0, devchecked[10] = {0}, hyst = 0;
int lightecho, cvlighttrig = 0, ivlighttrig = 0, cflighttrig = 0, svlighttrig = 0, cvglobalns = 0, ivglobalns = 0, cfglobalns = 0, svglobalns = 0;
int yLeftParCVlast=-1, yRightParCVlast=-1, yLeftParCFlast=-1, yRightParCFlast=-1;

char currentCommand[1024], filename[1024], datname[1024], logname[1024], emailaddr[1024], datime[1024];
double cvdat[10][10000], ivdat[2][10000], cfdat[10][10000], svdat[3][10000];
double readtimeout = 5.0, syncwaittime = 1.5, usertime = 0.2;

FILE *datfile;
FILE *fpp;
FILE *logfile;
FILE *scopefile;


/***************************************/
/******** Auxiliary functions  *********/
/********        BEGIN         *********/
/***************************************/

/***************** Clear buffer ********************/
void ClearBuff(char *buf, int n)
{
	int i;
	for(i=0;i<n;i++) buf[i]=0x00;
};

/******* Get current data and time 10 data + 8 time +  =22  ************/
int GetDataTime(char *dt)
{
	int ev1, ev2;
	FillBytes (dt, 0, 22, 0);
	sprintf(dt,"%s (%s)",DateStr(),TimeStr());
	return 0;
}

/******* Check if file exists  *****************/
int exists(const char *fname)
{
	FILE *file;
	if ((file = fopen(fname, "r")))
	{
		fclose(file);
		return 1;
	}
	return 0;
}

/******* Open dat and log files  *****************/ 
int openFiles(void)
{
	int l=-1;
	
	ClearBuff(filename,1024);
	ClearBuff(datname,1024);
	ClearBuff(logname,1024);
	
	GetCtrlVal (pnlvar, PNL_SAVELOG, &savlog); 
	GetCtrlVal (pnlvar, PNL_DATFILENAME, filename);
	
	if (strlen(filename) > 4) l=strncmp(&filename[strlen(filename)-4],".dat",4);
	
	SetBreakOnLibraryErrors(0);
	
	MakeDir("data");
	
	if(l!=0) sprintf(datname,"data/%s.dat",filename); 
	else sprintf(datname,"%s",filename);
	
	if (exists(datname) == 0) 
	{
		datfile=fopen(datname,"w");
		if (datfile!=NULL) outfileopened=1;
		else return -1;
	}
	else return -1;
	
	if(savlog) 
	{
		if(l!=0) sprintf(logname,"data/%s.log",filename);
		else {strcpy(logname,""); strncpy (logname, filename, strlen(filename)-4); strcat(logname,".log");}
		
		if (exists(logname) == 0) 
		{
			logfile=fopen(logname,"w");
			if (logfile!=NULL) outfileopened=1;
			else return -1;
		}
		else return -1; 
	}
	
	SetBreakOnLibraryErrors(1);
	
	return 1;	
}

/******* Synchronisation with SyncWait insead of delay  *****************/  
// if time is 0, it just checks for events
int SyncWaitLoop(double waittime)
{
	int ret = 0;
	double interval = 0.01;
	
	if (waittime == -1) // default sync wait time
	{
		GetCtrlVal (pnlvar, PNL_SYNCWAITTIME, &syncwaittime); 
		waittime = syncwaittime;
	}
	else if (waittime == -2) // time for user reading
	{
		GetCtrlVal (pnlvar, PNL_USERTIME, &usertime); 
		waittime = usertime;
	}
	
	if (waittime>=0 && waittime<0.1) interval = 0.01;
	else if (waittime>=0.1 && waittime<1.0) interval = 0.1;
	else if (waittime>=1.0) interval = waittime/10.;

	while (waittime > 0) 
	{
		ProcessSystemEvents();
		SyncWait (Timer(), interval);
		
		//Sleep(interval*1000);
		//mark = Timer();
		//SyncWait (mark, interval); 			    
		//sprintf(message,"Timer N[%i] %2.1f sec",numcycles,(float)(READTIMER-0.5-st*0.5));
		//SetCtrlVal(panelSEU, PANEL_STR_STATUS, message);
		
		waittime -= interval;
	}
	if (waittime == 0.0) ProcessSystemEvents();
	
	if (runflag == 0 && stophow == -1) ret = -1;
	
	return ret;
}						  

/******* Dim all callback elements except X *****************/
int DimAllTabsCallbacksExcept(int dim, int exceptTab, int exceptCall1, int exceptCall2)
{
	int i, callback[100]={0}, tab[100]={0};
	int Ncallbacks=11, Ntabs=6;
	
	callback[0]=PNL_SETGPIBMODE;
	callback[1]=PNL_DEVCHECK;
	callback[2]=PNL_QUIT;
	callback[3]=PNL_DATALOAD;
	callback[4]=PNL_GRCLEAR;
	callback[5]=PNL_GRREDRAW;
	callback[6]=PNL_VOLTAGEOFF;
	callback[7]=PNL_SCOPELOAD;
	callback[8]=PNL_SCOPESAVE;
	callback[9]=PNL_STARTMEAS;
	callback[10]=PNL_STOPMEAS;
	tab[0]=NTAB_CV;
	tab[1]=NTAB_IV;
	tab[2]=NTAB_CF;
	tab[3]=NTAB_SV;
	tab[4]=NTAB_SXY;
	tab[5]=NTAB_DE;
	
	for (i=0;i<Ncallbacks;i++)
		if ( (dim==1 && callback[i]!=exceptCall1 && callback[i]!=exceptCall2) || (dim==0 && callback[i]!=PNL_STOPMEAS) ) 
			SetCtrlAttribute (pnlvar, callback[i], ATTR_DIMMED, dim);
		
	for (i=0;i<Ntabs;i++)	
		if ( (dim==1 && tab[i]!=exceptTab) || dim==0 ) SetTabPageAttribute (pnlvar, PNL_TAB, tab[i], ATTR_DIMMED, dim);
		
	return 0;	
}

/******* Set Hioki Scale linear or log *****************/
int SetHiokiScale(void)
{
	int HiokiLogScale=0; 
			
	GetPanelHandleFromTabPage(pnlvar, PNL_TAB, NTAB_CF, &tabhandle);
	GetCtrlVal (tabhandle, TAB_CF_HIOKISCALE, &HiokiLogScale);
	if (HiokiLogScale == 0) 
	{
		SetCtrlAttribute (tabhandle, TAB_CF_STEPFREQ, ATTR_LABEL_TEXT, "Step Freq, kHz");
		SetCtrlAttribute (tabhandle, TAB_CF_HIOKISCALEINFO, ATTR_VISIBLE, 0);
		SetCtrlAttribute (tabhandle, TAB_CF_GRAPH, ATTR_XMAP_MODE, VAL_LINEAR);
		SetCtrlAttribute (tabhandle, TAB_CF_GRAPH, ATTR_YMAP_MODE, VAL_LINEAR);
	}
	else if (HiokiLogScale == 1) 
	{
		SetCtrlAttribute (tabhandle, TAB_CF_STEPFREQ, ATTR_LABEL_TEXT, "Step Freq within");
		SetCtrlAttribute (tabhandle, TAB_CF_HIOKISCALEINFO, ATTR_VISIBLE, 1);
		SetCtrlAttribute (tabhandle, TAB_CF_GRAPH, ATTR_XMAP_MODE, VAL_LOG);
		SetCtrlAttribute (tabhandle, TAB_CF_GRAPH, ATTR_YMAP_MODE, VAL_LOG);
	}
	
	return 0;
}

/******* Set axes for capacitance or resistance *****************/ 
int CRsetAxes (int mode)
{
	int graphLed=0, graphTab=-1,yLeftPar=1, yRightPar=-1;
	
	if (mode < 0) GetActiveTabPage (pnlvar, PNL_TAB, &mode);
	GetPanelHandleFromTabPage(pnlvar, PNL_TAB, mode, &tabhandle);
			
	if (mode==NTAB_CV) 
	{
		graphTab = TAB_CV_GRAPH;
		graphLed = TAB_CV_LED_YRIGHT;
		GetCtrlVal (tabhandle, TAB_CV_CRPAR1, &yLeftPar); 
		GetCtrlVal (tabhandle, TAB_CV_CRPAR2, &yRightPar);
		if (yLeftPar != yLeftParCVlast || yRightPar != yRightParCVlast) 
		{
			cvlighttrig = 0; 
			DeleteGraphPlot (tabhandle, TAB_CV_GRAPH, -1, VAL_IMMEDIATE_DRAW);
		}
		yLeftParCVlast = yLeftPar;
		yRightParCVlast = yRightPar;
	}
	else if (mode==NTAB_CF)
	{
		graphTab = TAB_CF_GRAPH;
		graphLed = TAB_CF_LED_YRIGHT;
		GetCtrlVal (tabhandle, TAB_CF_CRPAR1, &yLeftPar); 
		GetCtrlVal (tabhandle, TAB_CF_CRPAR2, &yRightPar);
		if (yLeftPar != yLeftParCFlast || yRightPar != yRightParCFlast) 
		{
			cflighttrig = 0;
			DeleteGraphPlot (tabhandle, TAB_CF_GRAPH, -1, VAL_IMMEDIATE_DRAW);
		}
		yLeftParCFlast = yLeftPar;
		yRightParCFlast = yRightPar;
	}
		
	SetCtrlAttribute (tabhandle, graphTab, ATTR_ACTIVE_YAXIS, VAL_LEFT_YAXIS);	
	if (yLeftPar == 1 || yLeftPar == 2) 
	{
		SetCtrlAttribute (tabhandle, graphTab, ATTR_YMAP_MODE, VAL_LINEAR);
		SetCtrlAttribute (tabhandle, graphTab, ATTR_YNAME, "Capacitance, F");
	}
	else if (yLeftPar == 3 || yLeftPar == 4) 
	{
			SetCtrlAttribute (tabhandle, graphTab, ATTR_YMAP_MODE, VAL_LOG);
			SetCtrlAttribute (tabhandle, graphTab, ATTR_YNAME, "Resistance, Ohm");	
	}
	
	if (yRightPar >= 0)
	{
		SetCtrlAttribute (tabhandle, graphLed, ATTR_VISIBLE, 1);	
		SetCtrlAttribute (tabhandle, graphTab, ATTR_ACTIVE_YAXIS, VAL_RIGHT_YAXIS);	
		SetCtrlAttribute (tabhandle, graphTab, ATTR_YLABEL_VISIBLE, 1);	
		if (yRightPar == 1 || yRightPar == 2) 
		{
			SetCtrlAttribute (tabhandle, graphTab, ATTR_YMAP_MODE, VAL_LINEAR);
			SetCtrlAttribute (tabhandle, graphTab, ATTR_YNAME, "Capacitance, F");
		}
		else if (yRightPar == 3 || yRightPar == 4) 
		{
			SetCtrlAttribute (tabhandle, graphTab, ATTR_YMAP_MODE, VAL_LOG);
			SetCtrlAttribute (tabhandle, graphTab, ATTR_YNAME, "Resistance, Ohm");	
		}
	}
	else 
	{
		SetCtrlAttribute (tabhandle, graphTab, ATTR_ACTIVE_YAXIS, VAL_RIGHT_YAXIS);	
		SetCtrlAttribute (tabhandle, graphTab, ATTR_YNAME, "");
		SetCtrlAttribute (tabhandle, graphLed, ATTR_VISIBLE, 0);	
		SetCtrlAttribute (tabhandle, graphTab, ATTR_YLABEL_VISIBLE, 0);
	}
	
	SetCtrlAttribute (tabhandle, graphTab, ATTR_ACTIVE_YAXIS, VAL_LEFT_YAXIS);	
		
	return 0;
}

/******* Recalculate voltage to acoomodate for Vdrop in long cables *****************/ 
double VrecalcToDrop(double Vdesired)
{
	double Vrecalc=0;
	
	// 0.006V drop for every volt supplied. 
	// At IV/CRV/CRF measurements there is a 0.1V Vdrop, so real Vbd is 0.1V lower.
	Vrecalc = Vdesired*(1.006) - 0.1; 
		
	return Vrecalc;
}



/***************************************/
/******** Auxiliary functions  *********/
/********         END          *********/
/***************************************/


/***************************************/
/******** COM - GPIB FUNCTIONS *********/
/********        BEGIN         *********/
/***************************************/

/* Open COM-port (for 2D mover or any other device) */
int InitCOM(int port, int rate)
{
	int err = -1;
	char pn[6]="COM1";
	char conName[100]="", status[300]="";
	sprintf(pn,"COM%i",port);
	sprintf (conName, "COM%i-port", port);

	SetBreakOnLibraryErrors(0);
	if (rate <= 0) rate = rate2D;
	err=OpenComConfig (port, pn, rate, 0, 8, 1, 512, 512); 
	if(err==0) 
	{
		GetCtrlVal (pnlvar, PNL_READTIMEOUT, &readtimeout);
		if (readtimeout >= 0) 
		{
			SetComTime (port, readtimeout);
			sprintf (status, "%s is opened, timeout is set to %.3f sec", conName, readtimeout);
		}
		else sprintf (status, "%s is opened", conName);
	}
	else
	{
		sprintf (status, "%s is not opened", conName);
	}
	
	SetCtrlVal (pnlvar, PNL_STATUS, status);
	if (err>=0) SyncWaitLoop(-2); // user reading
	if (err>=0 && port== XYMOVER) SyncWaitLoop(1.0); // special delay for XYMOVER
	SetBreakOnLibraryErrors(1);
	
	return err;
};

/* Close COM-port */
int CloseCOM(int port)
{
	int err=-1;
	char status[300]="";

	err=CloseCom (port);
	sprintf(status,"COM%i-port was closed",comport);
	SetCtrlVal (pnlvar, PNL_STATUS, status);
	return err;
};

/* Open GPIB communication via USB-GPIB or NI-GPIB */
// GPIB mode is chosen in beginning of this code
int InitGPIB(int mode)
{
	int err = -1;
	char pn[5]="COM1";
	char conName[100]="", status[300]="";

	SetBreakOnLibraryErrors(0);
	if (GPIBMODE == 0) 
	{
		GetCtrlVal (pnlvar, PNL_COMPORT, &comport);
		sprintf(pn,"COM%i",comport);
		sprintf (conName, "COM%i-port", comport);
		err = OpenCom (comport, pn);
	}
	else if (rmHandle<=0 || mode==-1) 
	{
		sprintf (conName, "VisaRM");
		err = viOpenDefaultRM (&rmHandle);
	}

	if(err==0) 
	{
		SetCtrlVal (pnlvar, PNL_COM_LED, 1);
		GetCtrlVal (pnlvar, PNL_READTIMEOUT, &readtimeout);
		if (readtimeout >= 0) 
		{
			if (GPIBMODE == 0) 
			{
				SetComTime (comport, readtimeout);
				sprintf (status, "%s is opened, timeout is set to %.3f sec", conName, readtimeout);
			}
			else sprintf (status, "%s is opened", conName);
		}
	}
	else
	{
		SetCtrlVal (pnlvar, PNL_COM_LED, 0);
		sprintf (status, "%s is not opened", conName);
	}
	
	SetCtrlVal (pnlvar, PNL_STATUS, status);
	SetBreakOnLibraryErrors(1);
	SyncWaitLoop(-2); // user reading
	
	return err;
};

/* Close GPIB */
int CloseGPIB(int port)
{
	int err=-1;
	char status[300]="";

	SetBreakOnLibraryErrors(0);
	if (GPIBMODE == 0)
	{
		err=ComWrt (port, "IBO\n", 4);
		SyncWaitLoop(0.1);
		CloseCom (port);
		sprintf(status,"COM%i-port was closed",comport);
	}
	else if (rmHandle>0)
	{
		err = viClose(rmHandle);
		sprintf(status,"VisaRM was closed");
	}
	SetCtrlVal (pnlvar, PNL_STATUS, status);
	SyncWaitLoop(-2); // user reading
	SetBreakOnLibraryErrors(1);
	return err;
};

/* Initialize Visa device connection */   
int InitViDev(int devnum, int mode, int printout)
{
	ViSession devHandle = 0;
	int err=0;
	char devname[100]="", status[100]="", resourceString[100]="";
	
	SetBreakOnLibraryErrors(0);
	GetCtrlVal (pnlvar, PNL_READTIMEOUT, &readtimeout);
	GetCtrlVal (pnlvar, PNL_PORT_6517A, &K6517A); 
	GetCtrlVal (pnlvar, PNL_PORT_3532, &HIOKICV);
	GetCtrlVal (pnlvar, PNL_PORT_1024, &RS1024);
	
	if (GPIBMODE == 1 && popen == 0)
	{
		if (devnum == K6517A)  {devHandle = K6517AHandle; sprintf (devname,"KEITHLEY 6517A");} 
		else if (devnum == HIOKICV) {devHandle = HIOKICVHandle; sprintf (devname,"HIOKI 3532-50");}
		else if (devnum == RS1024) {devHandle = RS1024Handle; sprintf (devname,"Rohde&Schwarz RTO1024");}
		
		if (devHandle > 0 && mode!=-1) 
		{
			//viClear(devHandle); 
			return 0;
		}
	}												  
	else return 0;
	
	sprintf(resourceString, "GPIB::%d::INSTR", devnum);
	err = viOpen(rmHandle, resourceString, VI_NULL, VI_NULL, &devHandle);
	if (devnum != K6517A) err = viClear(devHandle); //clear instrument's io buffers
	err = viSetAttribute(devHandle, VI_ATTR_TMO_VALUE, readtimeout*1000);
	
	if (devnum == K6517A) K6517AHandle = devHandle; 
	else if (devnum == HIOKICV) HIOKICVHandle = devHandle; 
	else if (devnum == RS1024) RS1024Handle = devHandle; 
	
	if (devHandle > 0) sprintf (status,"Connection to %s is opened, timeout is set to %.3f sec", devname, readtimeout);
	else sprintf (status,"Connection to %s is not opened", devname);
	if (printout == 1) SetCtrlVal (pnlvar, PNL_STATUS, status);
	
	SetBreakOnLibraryErrors(1);
	return err;
}

/*  Send a command to device by USB-GPIB */
int USBGPIBsend (int comport, int device, char *cmd)
{
	int bn=0,err=0;
	char comd[40];

	/* Unlistner CMD */
	bn = ComWrt (comport, "IBc?\n", 5);
	err = ComRdByte (comport);
	//Delay(0.5);
	//printf("GPIB Unlistner [0x%X]\n",err); 
	//FlushOutQ (comport); 

	/* New GPIB addr */  
	sprintf(comd,"IBC%c\n",(char)(device+32));
	bn = ComWrt (comport, comd, strlen(comd));
	err = ComRdByte (comport); 
	//Delay(0.5); 
	//printf("GPIB Readdressing [0x%X]\n",err); 
	//FlushOutQ (comport); 

	/* New GPIB cmd */  
	sprintf(comd,"IB%c%c%s%c%c\n",0x10,0x02,cmd,0x10,0x03);
	bn = ComWrt (comport, comd, strlen(comd));
	err = ComRdByte (comport);
	//Delay(0.5); 
	//printf("GPIB [ %s ] command [0x%X]\n",comd,err); 
	//FlushOutQ (comport); 

	//ComWrt (comport, "\n", 1); 
	return err;	
};

/*  Setup a device for data output  by USB-GPIB */
int USBGPIBenter (int comport, int device, char *buf, int nm)
{
	int bn=0,err=0;
	char comd[40];

	strcpy(buf,"");

	/* New GPIB addr */  
	sprintf(comd,"IBC%c\n",(char)(device+64));
	bn = ComWrt (comport, comd, strlen(comd));
	err = ComRdByte (comport);
	//Delay(0.10);
	//Delay(0.5); 
	//printf("GPIB Readdressing [0x%X]\n",err); 
	//FlushOutQ (comport); 

	/* Read GPIB */  
	bn = ComWrt (comport, "IB?\n", 4);
	err = ComRdByte (comport);
	//Delay(0.5); 
	//printf("GPIB READ command [0x%X]\n",err); 
	bn = ComRd (comport, buf, nm);
	//if (bn != nm)  printf("GPIB READ timeout %i\n",err);  

	//FlushOutQ (comport); 

	//ComWrt (comport, "\n", 1); 
	return err;	
};

/*  Get device attention */ 
int USBGPIBgetAttention (int comport, int device)
{
	int bn=0,err=0;
	char comd[40];
	char buf[40];

	strcpy(buf,"");

	/* New GPIB addr */  
	sprintf(comd,"IBC%c\n",(char)(device+64));
	bn = ComWrt (comport, comd, strlen(comd));
	err = ComRdByte (comport);
	//Delay(0.10);
	//Delay(0.5); 
	//printf("GPIB Readdressing [0x%X]\n",err); 
	//FlushOutQ (comport); 

	/* Read GPIB */  
	bn = ComWrt (comport, "IB?\n", 4);
	err = ComRdByte (comport);
	//SyncWaitLoop(5);
	//Delay(0.5); 
	//printf("GPIB READ command [0x%X]\n",err); 
	bn = ComRd (comport, buf, 4);
	//if (bn != nm)  printf("GPIB READ timeout %i\n",err);  

	//FlushOutQ (comport); 

	//ComWrt (comport, "\n", 1); 
	//getchar(); 
	return err;	
};

/* USB-GPIB interface clear */  
int USBGPIBClear(int comport, int device)
{
	USBGPIBsend(comport, device, "IBZ");
	return 0;
};

/**************************************************************/
// Send command to any device by USB-GPIB, NI-GPIB or COM-port
// For COM-ports (non-GPIB) use devnum < 0
/**************************************************************/							 
int SendCommand(int port, int devnum, char *command, int responsewait, int maxNOREDO, int bytestoread, char *response)
{
	int nbytes, redo = 1, error = 1, errW=0, errR=0, devHandle = 0;
	ViUInt32 returnCount = 0;
	char errDescription[1024], conName[100]="", commandLF[1024]="";
	char *errDescriptionN=NULL;
	char status[300], buf[4096]="", *buf2;
	
	sprintf(currentCommand,"%s",command);
	currentDevnum=devnum;
	
	if (GPIBMODE == 1)
	{
		if (devnum == K6517A)  devHandle = K6517AHandle; 
		else if (devnum == HIOKICV) devHandle = HIOKICVHandle; 
		else if (devnum == RS1024)  devHandle = RS1024Handle; 
	}
	else devHandle = 1;
	
	if ((popen == 0 && devHandle > 0 && devnum > 0) || (popenX == 0 && devnum < 0) || (port==comport2D && popen2D == 0 && devnum < 0))  
	{
		while (redo > 0)
		{
			// send command
			//if (SyncWaitLoop(-1) == -1) {error = -1; break;} 
			if (GPIBMODE == 0 && devnum > 0)
			{
				if (devnum != RS1024) USBGPIBClear(port, USBGPIB);   // clear USBGPIB interface
				if (devnum == RS1024 && RS1024attention == 0) USBGPIBgetAttention (port, devnum);  // needed for RS1024 instead of clear USBGPIB interface
				errW = USBGPIBsend (port, devnum, command);  	
				if (errW < 0) 
				{
					errDescriptionN = GetRS232ErrorString (errW);
					printf("\nCommand [%s] was not sent to device [%d] with error:\n%s\n",command, devnum, errDescriptionN);
				}
			}
			else if (GPIBMODE == 1 && devnum > 0)
			{
				sprintf(commandLF, "%s\n", command);
				errW = viWrite(devHandle, (ViBuf)commandLF, (ViUInt32)strlen(commandLF), VI_NULL);  
				if (errW < 0) 
				{
					viStatusDesc(devHandle, errW, errDescription); 
					printf("\nCommand [%s] was not sent to device [%d] with error:\n%s\n",command, devnum, errDescription);
				}										
			}
			else if (devnum < 0)
			{
				sprintf(commandLF, "%s\r\n", command);
				errW = ComWrt (port, commandLF, strlen(commandLF));
				if (errW < 0) 
				{
					errDescriptionN = GetRS232ErrorString (errW);
					printf("\nCommand [%s] was not sent to device [%d] with error:\n%s\n",command, devnum, errDescriptionN);
				}
			}
			
			// get responce if reqested
			if (responsewait == 1)
			{
				//if (SyncWaitLoop(-1) == -1) {error = -1; break;}	
				SyncWaitLoop(-1);
				ClearBuff(buf,4096);				
				ClearBuff(response,bytestoread);
				
				if (GPIBMODE == 0 && devnum > 0)
				{
					// 5 service bytes of USB-GPIB: (02) before message and (0A,10,03,06) after // (HEX format)
					errR = USBGPIBenter (port, devnum, buf, bytestoread+5); 
					buf2 = &buf[1];
					Scan (buf2,"%s>%s[t10]",response); // changed to t10
					nbytes = NumFmtdBytes ();
					if (errR < 0) 
					{
						errDescriptionN = GetRS232ErrorString (errR);
						printf("\nResponse to command [%s] was not received from device [%d] with error:\n%s\n",command, devnum, errDescriptionN);
					}
				}
				else if (GPIBMODE == 1 && devnum > 0) 
				{
					errR = viRead(devHandle, (ViBuf)buf, bytestoread+1, &returnCount);
					Scan (buf,"%s>%s[t10]",response);
					nbytes = NumFmtdBytes ();
					//nbytes = returnCount;
					if (errR < 0) 
					{
						viStatusDesc(devHandle, errR, errDescription); 
						printf("\nResponse to command [%s] was not received from device [%d] with error:\n%s\n",command, devnum, errDescription);
					}
				}
				else if (devnum < 0)
				{
					nbytes = ComRd (port, buf, bytestoread+2);
					if (nbytes < 0) errR = nbytes;
					Scan (buf,"%s>%s[t13]",response);
					nbytes = NumFmtdBytes ();
					if (errR < 0) 
					{
						errDescriptionN = GetRS232ErrorString (errR);
						printf("\nResponse to command [%s] was not received from device [%d] with error:\n%s\n",command, devnum, errDescriptionN);
					}
				}
				response[nbytes] = 0; //terminate the string properly
				if (debugging) printf(buf);
				
				if (nbytes == bytestoread) redo = 0;
				else 
				maxNOREDO--;
				if (maxNOREDO < 0) redo = 0;
			}
			else redo = 0;
			
			if (SyncWaitLoop(-1) == -1) {error = -1; return error;} 
		}
	}
	else 
	{
		if (GPIBMODE == 0 || devnum < 0) sprintf (conName, "COM%i-port", port);
		else sprintf (conName, "VisaRM or remote device");
		sprintf (status, "Cannot send the command: %s is not opened", conName);
		SetCtrlVal (pnlvar, PNL_STATUS, status);
		error = 0;
	}
	
	if (errW < -1) error = errW;
	if (errR < -1) error = errR;

	return error;
}			

/*  Check for instrument errors via GPIB: read and print all */
int GPIBErrorQueue(int devnum, char *command)
{
	char resp[4096]="";
    int stb;
	int err;
    
	err = SendCommand(comport, devnum, "*STB?", RESP, NOREDO, 1024, resp);
	sscanf(resp,"%d",&stb);
	if ((stb & 4) > 0)
    {	
        do
    	{
			err = SendCommand(comport, devnum, "SYST:ERR?", RESP, NOREDO, 1024, resp);
			if (resp[0]=='0' || strlen(resp)==0) break;
			printf("\nAfter command [%s] sent to device [%d], following instrument errors occured:\n", command, devnum);
    		printf("%s\n", resp);

    	} while (1);
    }
    
	return stb;
}


/***************************************/
/******** COM - GPIB FUNCTIONS *********/
/********         END          *********/
/***************************************/



/******************* Devices cheking  ***********************/
int DevCheck(int mode)
{
	int i, dev = 1, address, led, check[10], err = 10;
	char answer[6]="", devname[100]="", status[100]="", resourceString[100]="";
	
	SetCtrlVal (pnlvar, PNL_LEDRUN, 1);
	if (popen!=0 || mode == -1) popen= InitGPIB(mode);
	if ( NDEVICES>=4 && ((popen2D!=0 && mode == NTAB_SXY) || mode == -1) ) 
	{
		GetCtrlVal (pnlvar, PNL_PORT_XYMOVER, &comport2D);
		popen2D = InitCOM(comport2D, rate2D);
	}

	if (popen==0) 
	{
		
		if (mode != -1)
		{
			check[0] = 1;
			if (mode == NTAB_CV || mode == NTAB_CF) check[1] = 1;
			else check[1] = 0;
			if (mode == NTAB_SV || mode == NTAB_SXY) check[2] = 1; 
			else check[2] = 0; 
			if (mode == NTAB_SXY) check[3] = 1;
			else check[3] = 0;
		}
		else if (mode == -1)
		{
			check[0] = 1; 
			check[1] = 1; 
			check[2] = 1;
			check[3] = 1;
		}
		
		GetCtrlVal (pnlvar, PNL_READTIMEOUT, &readtimeout);
		GetCtrlVal (pnlvar, PNL_PORT_6517A, &K6517A); 
		GetCtrlVal (pnlvar, PNL_PORT_3532, &HIOKICV);
		GetCtrlVal (pnlvar, PNL_PORT_1024, &RS1024);
		GetCtrlVal (pnlvar, PNL_PORT_XYMOVER, &XYMOVER);
		
		for (i = 0; i<NDEVICES; i++)
		{
			if (check[i] == 1)
			{
				if (i==0) 
				{
					address = K6517A; 
					led = PNL_KEITH_LED; 
					sprintf (devname,"KEITHLEY 6517A");
				}
				else if (i==1) 
				{
					address = HIOKICV; 
					led = PNL_HIOKI_LED; 
					sprintf (devname,"HIOKI 3532-50");
				}
				else if (i==2) 
				{
					address = RS1024; 
					led = PNL_RS_LED; 
					sprintf (devname,"Rohde&Schwarz RTO1024");
				}
				else if (i==3) 
				{
					address = XYMOVER; 
					led = PNL_XYMOVER; 
					sprintf (devname,"Makeblock XY mover V2.0"); 
				}
				
				if (GPIBMODE == 1 && address != XYMOVER) err = InitViDev(address, -1, 0);
				
				if (i < 3) SendCommand(comport, address, "*ESE?", RESP, NOREDO, 1, answer); // simple way: ask if the executive error == 0	   
				else if (i == 3 && popen2D==0) SendCommand(address, -1, "", RESP, 0, 2, answer); // ask if OK
				if ( (answer[0] == '0' && i < 3) || (answer[0] == 'O' && i == 3) ) 
				{
					SetCtrlVal (pnlvar, led, 1);
					sprintf (status,"%s is ready for work, timeout is set to %.3f sec", devname, readtimeout);
					SetCtrlVal (pnlvar, PNL_STATUS, status);
					GPIBErrorQueue(address, "#device_initialization_and_check");
				}
				else 
				{
					SetCtrlVal (pnlvar, led, 0);
					sprintf (status,"%s is not ready for work", devname);
					SetCtrlVal (pnlvar, PNL_STATUS, status);
					dev = 0;
				} 	
				
				SyncWaitLoop(-2); // user reading
			}
		}
		
	}
	else 
	{
		sprintf (status, "Cannot check the devices: no GPIB connection");
		SetCtrlVal (pnlvar, PNL_STATUS, status);
		dev = 0;
	}

	SetCtrlVal (pnlvar, PNL_LEDRUN, 0);
	
	return dev;
}	

/******************* Stop the measurement right now!  ***********************/  
int StopMeasurement(int mode, int interrupted)
{
	int ifVoff=0, mailEnb=0;
	char status[300]="", buf2[100]="", buf3[100]="", resp[4096]="";
	
	GetCtrlVal (pnlvar, PNL_VOLTAGEOFFATEND, &ifVoff);
	if(ifVoff==1 && mode<10) 
	{
		SendCommand(comport, K6517A, "sour:volt 0.0", NORESP, NOREDO, 0, NULL);
		SendCommand(comport, K6517A, "outp:stat off", NORESP, NOREDO, 0, NULL);
		SendCommand(comport, K6517A, "outp:stat?", RESP, NOREDO, 1, resp);
		if (resp[0]=='0')
		{
			voltageIsOn = 0;
			sprintf(buf3,", Voltage is off");
		}
		else 
		{
			voltageIsOn = 1;
			sprintf(buf3,", Failed to turn off voltage, it may be still on!");
		}
	}
	SendCommand(comport, K6517A, "syst:zch on", NORESP, NOREDO, 0, NULL); /* Zero Check ON*/
	
	if (interrupted == -1) sprintf(buf2, " by interrupt");
	
	if(mode==NTAB_CV) sprintf(status, "C(V) measurement was finished%s%s",buf2,buf3);
	else if(mode==NTAB_IV) sprintf(status, "I(V) measurement was finished%s%s",buf2,buf3);
	else if(mode==NTAB_CF) sprintf(status, "C(F) measurement was finished%s%s",buf2,buf3);
	else if(mode==NTAB_SV) sprintf(status, "Scope(V) measurement was finished%s%s",buf2,buf3);
	else if(mode==NTAB_SXY) sprintf(status, "Scope(XY) measurement was finished%s%s",buf2,buf3);
	else if(mode==9) sprintf(status, "Measurement failed: 10 last data request attempts failed");
	else if(mode==10) sprintf(status, "Cannot start measurement: cannot open files or files already exist");
	else if(mode==11) sprintf(status, "Cannot start measurement: stop voltage is larger than max voltage");
	else if(mode==13) sprintf(status, "Cannot start measurement: some devices are not ready");
	SetCtrlVal (pnlvar, PNL_STATUS, status);
	
	if (mode!=10)
	{
		if (mode!=NTAB_SXY) GPIBErrorQueue(K6517A, "#end_of_measurement");
		if (mode==NTAB_CV || mode==NTAB_CF) GPIBErrorQueue(HIOKICV, "#end_of_measurement");
		if (mode==NTAB_SV || mode==NTAB_SXY) GPIBErrorQueue(RS1024, "#end_of_measurement");
	}
	
	if(datfile!=NULL && outfileopened==1) fclose(datfile);  
	if(savlog==1 && logfile!=NULL && outfileopened==1) fclose(logfile);
	outfileopened=0;
	
	GetDataTime(datime); 
	SetCtrlVal (pnlvar, PNL_STOPTIME, datime);
	SetCtrlVal (pnlvar, PNL_LEDRUN, 0);
	SetCtrlAttribute (pnlvar, PNL_STARTMEAS, ATTR_DIMMED, 0);
	SetCtrlAttribute (pnlvar, PNL_STOPMEAS, ATTR_DIMMED, 1);
	
	GetCtrlVal (pnlvar, PNL_MAILENB,&mailEnb); 
	if(mailEnb==1 && MSEND==1)
	{
		MSEND=0;
		LaunchExecutable (emailaddr);
	}
	
	//runflag=1;
	DimAllTabsCallbacksExcept(0, -1, -1, -1);
	
	return 0;
}

/******************* Save parameters from GUI  ***********************/  
int SaveConfig(void)
{
	int mailEnb,setting,ifVoff,yLeftPar,yRightPar,fyLeftPar,fyRightPar,autorange,HiokiLogScale,relVbd,ecount,autoScale,invert,addr,comrate,rwait,bytes;
	double vstart,vstop,vstep,vmax,freq,h_volt,measWaitTime;
	double vstart2,vstop2,vstep2,vmax2,curmax,currentRange,measWaitTime2;	
	double fstart,fstop,fstep,fvmax,volt,fh_volt,fmeasWaitTime;	
	double vstart3,vstop3,vstep3,vmax3,Vbd,scopeScale,scopeWaitTime;	
	char comd[1024]="";
	
	GetActiveTabPage (pnlvar, PNL_TAB, &ntab);
	GetCtrlVal (pnlvar, PNL_SETGPIBMODE, &GPIBMODE);
	GetCtrlVal (pnlvar, PNL_COMPORT, &comport);
	GetCtrlVal (pnlvar, PNL_PORT_6517A, &K6517A);
	GetCtrlVal (pnlvar, PNL_PORT_3532, &HIOKICV);
	GetCtrlVal (pnlvar, PNL_PORT_1024, &RS1024);
	GetCtrlVal (pnlvar, PNL_PORT_XYMOVER, &XYMOVER);
	GetCtrlVal (pnlvar, PNL_STARTCHECK, &startcheck);
	GetCtrlVal (pnlvar, PNL_SAVELOG, &savlog);
	GetCtrlVal (pnlvar, PNL_MAILENB, &mailEnb);
	
	GetCtrlVal (pnlvar, PNL_HYSTERESIS, &hyst);
	GetCtrlVal (pnlvar, PNL_LIGHTECHO, &lightecho);	
	GetCtrlVal (pnlvar, PNL_VOLTAGEOFFATEND, &ifVoff);		
	GetCtrlVal (pnlvar, PNL_SCOPESETTING, &setting);
	GetCtrlVal (pnlvar, PNL_READTIMEOUT, &readtimeout);
	GetCtrlVal (pnlvar, PNL_SYNCWAITTIME, &syncwaittime);
	GetCtrlVal (pnlvar, PNL_USERTIME, &usertime);
	GetCtrlVal (pnlvar, PNL_DEBUGPRINT, &debugging);
	
	GetPanelHandleFromTabPage(pnlvar, PNL_TAB, NTAB_CV, &tabhandle);
	GetCtrlVal (tabhandle, TAB_CV_STARTVOLTAGE, &vstart);
	GetCtrlVal (tabhandle, TAB_CV_STOPVOLTAGE, &vstop);
	GetCtrlVal (tabhandle, TAB_CV_STEPVOLTAGE, &vstep);
	GetCtrlVal (tabhandle, TAB_CV_MAXVOLTAGE, &vmax);
	GetCtrlVal (tabhandle, TAB_CV_FREQ, &freq);
	GetCtrlVal (tabhandle, TAB_CV_HIOKI_VOLTAGE, &h_volt);
	GetCtrlVal (tabhandle, TAB_CV_MEASWAITTIME, &measWaitTime);
	GetCtrlVal (tabhandle, TAB_CV_CRPAR1, &yLeftPar); 
	GetCtrlVal (tabhandle, TAB_CV_CRPAR2, &yRightPar);
	
	GetPanelHandleFromTabPage(pnlvar, PNL_TAB, NTAB_IV, &tabhandle);
	GetCtrlVal (tabhandle, TAB_IV_STARTVOLTAGE, &vstart2);
	GetCtrlVal (tabhandle, TAB_IV_STOPVOLTAGE, &vstop2);
	GetCtrlVal (tabhandle, TAB_IV_STEPVOLTAGE, &vstep2);
	GetCtrlVal (tabhandle, TAB_IV_MAXVOLTAGE, &vmax2);
	GetCtrlVal (tabhandle, TAB_IV_MAXCURRENT, &curmax);
	GetCtrlVal (tabhandle, TAB_IV_AUTORANGE, &autorange);
	GetCtrlVal (tabhandle, TAB_IV_CURRENTRANGE, &currentRange);
	GetCtrlVal (tabhandle, TAB_IV_MEASWAITTIME, &measWaitTime2);
	
	GetPanelHandleFromTabPage(pnlvar, PNL_TAB, NTAB_CF, &tabhandle);
	GetCtrlVal (tabhandle, TAB_CF_STARTFREQ, &fstart);
	GetCtrlVal (tabhandle, TAB_CF_STOPFREQ, &fstop);
	GetCtrlVal (tabhandle, TAB_CF_STEPFREQ, &fstep);
	GetCtrlVal (tabhandle, TAB_CF_MAXVOLTAGE, &fvmax);
	GetCtrlVal (tabhandle, TAB_CF_VOLTAGE, &volt);
	GetCtrlVal (tabhandle, TAB_CF_HIOKI_VOLTAGE, &fh_volt);
	GetCtrlVal (tabhandle, TAB_CF_MEASWAITTIME, &fmeasWaitTime);
	GetCtrlVal (tabhandle, TAB_CF_HIOKISCALE, &HiokiLogScale);
	GetCtrlVal (tabhandle, TAB_CF_CRPAR1, &fyLeftPar); 
	GetCtrlVal (tabhandle, TAB_CF_CRPAR2, &fyRightPar);
	
	GetPanelHandleFromTabPage(pnlvar, PNL_TAB, NTAB_SV, &tabhandle);
	GetCtrlVal (tabhandle, TAB_SV_STARTVOLTAGE, &vstart3);
	GetCtrlVal (tabhandle, TAB_SV_STOPVOLTAGE, &vstop3);
	GetCtrlVal (tabhandle, TAB_SV_STEPVOLTAGE, &vstep3);
	GetCtrlVal (tabhandle, TAB_SV_MAXVOLTAGE, &vmax3);	
	GetCtrlVal (tabhandle, TAB_SV_RELVBD, &relVbd);
	GetCtrlVal (tabhandle, TAB_SV_VBD, &Vbd);
	GetCtrlVal (tabhandle, TAB_SV_EVENTCOUNT, &ecount);
	GetCtrlVal (tabhandle, TAB_SV_SCOPETIME, &scopeWaitTime);
	GetCtrlVal (tabhandle, TAB_SV_SCOPESCALE, &scopeScale);
	GetCtrlVal (tabhandle, TAB_SV_SCOPEAUTOSCALE, &autoScale);	
	GetCtrlVal (tabhandle, TAB_SV_INVERT, &invert);		
	
	GetPanelHandleFromTabPage(pnlvar, PNL_TAB, NTAB_SXY, &tabhandle);
	
	GetPanelHandleFromTabPage(pnlvar, PNL_TAB, NTAB_DE, &tabhandle);
	GetCtrlVal (tabhandle, TAB_DE_SENDMODE, &sendMode);  
	GetCtrlVal (tabhandle, TAB_DE_ADDR, &addr);  
	GetCtrlVal (tabhandle, TAB_DE_COMRATE, &comrate);
	GetCtrlVal (tabhandle, TAB_DE_RESPONSEWAIT, &rwait);  
	GetCtrlVal (tabhandle, TAB_DE_BYTESTOREAD, &bytes);
	GetCtrlVal (tabhandle, TAB_DE_COMMAND, comd);
		
	GetCtrlVal (pnlvar, PNL_DATFILENAME, filename);
	GetCtrlVal (pnlvar, PNL_STRMAIL, emailaddr);
	
	fpp = fopen("cfg.ini", "w");
	fprintf(fpp,"%d %d %d %d %d %d %d %d %d %d\n",ntab,GPIBMODE,comport,K6517A,HIOKICV,RS1024,XYMOVER,startcheck,savlog,mailEnb); // main left
	fprintf(fpp,"%d %d %d %d %lf %lf %lf %d\n",hyst,lightecho,ifVoff,setting,readtimeout,syncwaittime,usertime,debugging); // main right
	fprintf(fpp,"%lf %lf %lf %lf %lf %lf %lf %d %d\n",vstart,vstop,vstep,vmax,freq,h_volt,measWaitTime,yLeftPar,yRightPar); // CV
	fprintf(fpp,"%lf %lf %lf %lf %lf %d %lf %lf\n",vstart2,vstop2,vstep2,vmax2,curmax,autorange,currentRange,measWaitTime2); // IV
	fprintf(fpp,"%lf %lf %lf %lf %lf %lf %lf %d %d %d\n",fstart,fstop,fstep,fvmax,volt,fh_volt,fmeasWaitTime,fyLeftPar,fyRightPar,HiokiLogScale); // CF
	fprintf(fpp,"%lf %lf %lf %lf %d %lf %d %lf %lf %d %d\n",vstart3,vstop3,vstep3,vmax3,relVbd,Vbd,ecount,scopeWaitTime,scopeScale,autoScale,invert); // SV
	fprintf(fpp,"%d %d %d %d %d %s\n",sendMode,addr,comrate,rwait,bytes,comd); // DE
	fprintf(fpp,"%s\n",filename);
	fprintf(fpp,"%s\n",emailaddr);
	
	fclose(fpp);
	return 0;
}

/******************* Load parameters from GUI  ***********************/  
int LoadConfig(void)
{
	int mailEnb,setting,ifVoff,yLeftPar,yRightPar,fyLeftPar,fyRightPar,autorange,HiokiLogScale,relVbd,ecount,autoScale,invert,addr,comrate,rwait,bytes;
	double vstart,vstop,vstep,vmax,freq,h_volt,measWaitTime;
	double vstart2,vstop2,vstep2,vmax2,curmax,currentRange,measWaitTime2;	
	double fstart,fstop,fstep,fvmax,volt,fh_volt,fmeasWaitTime;	
	double vstart3,vstop3,vstep3,vmax3,Vbd,scopeScale,scopeWaitTime;	
	char comd[1024]="";
	
	strncpy(emailaddr,"",100);
	
	SetBreakOnLibraryErrors(0);
	fpp = fopen("cfg.ini", "r");
	if (fpp !=NULL) 
	{
		fscanf(fpp,"%d %d %d %d %d %d %d %d %d %d\n",&ntab,&GPIBMODE,&comport,&K6517A,&HIOKICV,&RS1024,&XYMOVER,&startcheck,&savlog,&mailEnb); // main left
		fscanf(fpp,"%d %d %d %d %lf %lf %lf %d\n",&hyst,&lightecho,&ifVoff,&setting,&readtimeout,&syncwaittime,&usertime,&debugging); // main right
		fscanf(fpp,"%lf %lf %lf %lf %lf %lf %lf %d %d\n",&vstart,&vstop,&vstep,&vmax,&freq,&h_volt,&measWaitTime,&yLeftPar,&yRightPar); // CV
		fscanf(fpp,"%lf %lf %lf %lf %lf %d %lf %lf\n",&vstart2,&vstop2,&vstep2,&vmax2,&curmax,&autorange,&currentRange,&measWaitTime2); // IV
		fscanf(fpp,"%lf %lf %lf %lf %lf %lf %lf %d %d %d\n",&fstart,&fstop,&fstep,&fvmax,&volt,&fh_volt,&fmeasWaitTime,&fyLeftPar,&fyRightPar, &HiokiLogScale); // CF
		fscanf(fpp,"%lf %lf %lf %lf %d %lf %d %lf %lf %d %d\n",&vstart3,&vstop3,&vstep3,&vmax3,&relVbd,&Vbd,&ecount,&scopeWaitTime,&scopeScale,&autoScale,&invert); // SV
		fscanf(fpp,"%d %d %d %d %d %s\n",&sendMode,&addr,&comrate,&rwait,&bytes,comd);
		fscanf(fpp,"%s\n",filename);
		fscanf(fpp,"%s\n",emailaddr);
		fclose(fpp);
		
		SetActiveTabPage (pnlvar, PNL_TAB, ntab);
		SetCtrlVal (pnlvar, PNL_SETGPIBMODE, GPIBMODE);
		if (GPIBMODE == 1) SetCtrlAttribute (pnlvar, PNL_COMPORT, ATTR_DIMMED, 1);
		SetCtrlVal (pnlvar, PNL_COMPORT, comport);
		SetCtrlVal (pnlvar, PNL_PORT_6517A, K6517A);
		SetCtrlVal (pnlvar, PNL_PORT_3532, HIOKICV);
		SetCtrlVal (pnlvar, PNL_PORT_1024, RS1024);
		SetCtrlVal (pnlvar, PNL_PORT_XYMOVER, XYMOVER);
		SetCtrlVal (pnlvar, PNL_STARTCHECK, startcheck);
		SetCtrlVal (pnlvar, PNL_SAVELOG, savlog);
		SetCtrlVal (pnlvar, PNL_MAILENB, mailEnb);
		if (!mailEnb) SetCtrlAttribute (pnlvar, PNL_STRMAIL, ATTR_DIMMED, 1);
		
		SetCtrlVal (pnlvar, PNL_HYSTERESIS, hyst);
		SetCtrlVal (pnlvar, PNL_LIGHTECHO, lightecho);
		SetCtrlVal (pnlvar, PNL_VOLTAGEOFFATEND, ifVoff);		
		SetCtrlVal (pnlvar, PNL_SCOPESETTING, setting);
		SetCtrlVal (pnlvar, PNL_READTIMEOUT, readtimeout);
		SetCtrlVal (pnlvar, PNL_SYNCWAITTIME, syncwaittime);
		SetCtrlVal (pnlvar, PNL_USERTIME, usertime);
		SetCtrlVal (pnlvar, PNL_DEBUGPRINT, debugging);
		
		GetPanelHandleFromTabPage(pnlvar, PNL_TAB, NTAB_CV, &tabhandle);
		SetCtrlVal (tabhandle, TAB_CV_STARTVOLTAGE, vstart);
		SetCtrlVal (tabhandle, TAB_CV_STOPVOLTAGE, vstop);
		SetCtrlVal (tabhandle, TAB_CV_STEPVOLTAGE, vstep);
		SetCtrlVal (tabhandle, TAB_CV_MAXVOLTAGE, vmax);
		SetCtrlVal (tabhandle, TAB_CV_FREQ, freq);
		SetCtrlVal (tabhandle, TAB_CV_HIOKI_VOLTAGE, h_volt);
		SetCtrlVal (tabhandle, TAB_CV_MEASWAITTIME, measWaitTime);
		SetCtrlVal (tabhandle, TAB_CV_CRPAR1, yLeftPar); 
		SetCtrlVal (tabhandle, TAB_CV_CRPAR2, yRightPar);
		CRsetAxes(NTAB_CV);
		
		GetPanelHandleFromTabPage(pnlvar, PNL_TAB, NTAB_IV, &tabhandle);
		SetCtrlVal (tabhandle, TAB_IV_STARTVOLTAGE, vstart2);
		SetCtrlVal (tabhandle, TAB_IV_STOPVOLTAGE, vstop2);
		SetCtrlVal (tabhandle, TAB_IV_STEPVOLTAGE, vstep2);
		SetCtrlVal (tabhandle, TAB_IV_MAXVOLTAGE, vmax2);
		SetCtrlVal (tabhandle, TAB_IV_MAXCURRENT, curmax); 
		SetCtrlVal (tabhandle, TAB_IV_AUTORANGE, autorange);
		SetCtrlVal (tabhandle, TAB_IV_CURRENTRANGE, currentRange);
		SetCtrlVal (tabhandle, TAB_IV_MEASWAITTIME, measWaitTime2);
		
		GetPanelHandleFromTabPage(pnlvar, PNL_TAB, NTAB_CF, &tabhandle);
		SetCtrlVal (tabhandle, TAB_CF_STARTFREQ, fstart);
		SetCtrlVal (tabhandle, TAB_CF_STOPFREQ, fstop);
		SetCtrlVal (tabhandle, TAB_CF_STEPFREQ, fstep);
		SetCtrlVal (tabhandle, TAB_CF_MAXVOLTAGE, fvmax);
		SetCtrlVal (tabhandle, TAB_CF_VOLTAGE, volt);
		SetCtrlVal (tabhandle, TAB_CF_HIOKI_VOLTAGE, fh_volt);
		SetCtrlVal (tabhandle, TAB_CF_MEASWAITTIME, fmeasWaitTime);
		SetCtrlVal (tabhandle, TAB_CF_CRPAR1, fyLeftPar); 
		SetCtrlVal (tabhandle, TAB_CF_CRPAR2, fyRightPar);
		CRsetAxes(NTAB_CF);
		SetCtrlVal (tabhandle, TAB_CF_HIOKISCALE, HiokiLogScale);
		SetHiokiScale();
		
		GetPanelHandleFromTabPage(pnlvar, PNL_TAB, NTAB_SV, &tabhandle);
		SetCtrlVal (tabhandle, TAB_SV_STARTVOLTAGE, vstart3);
		SetCtrlVal (tabhandle, TAB_SV_STOPVOLTAGE, vstop3);
		SetCtrlVal (tabhandle, TAB_SV_STEPVOLTAGE, vstep3);
		SetCtrlVal (tabhandle, TAB_SV_MAXVOLTAGE, vmax3);
		SetCtrlVal (tabhandle, TAB_SV_RELVBD, relVbd);
		SetCtrlVal (tabhandle, TAB_SV_VBD, Vbd);
		SetCtrlVal (tabhandle, TAB_SV_EVENTCOUNT, ecount);
		SetCtrlVal (tabhandle, TAB_SV_SCOPETIME, scopeWaitTime);
		SetCtrlVal (tabhandle, TAB_SV_SCOPESCALE, scopeScale);
		SetCtrlVal (tabhandle, TAB_SV_SCOPEAUTOSCALE, autoScale);
		SetCtrlVal (tabhandle, TAB_SV_INVERT, invert);
		if (!relVbd) SetCtrlAttribute (tabhandle, TAB_SV_VBD, ATTR_DIMMED, 1);
		
		GetPanelHandleFromTabPage(pnlvar, PNL_TAB, NTAB_SXY, &tabhandle);
		
		GetPanelHandleFromTabPage(pnlvar, PNL_TAB, NTAB_DE, &tabhandle);
		SetCtrlVal (tabhandle, TAB_DE_SENDMODE, sendMode);  
		SetCtrlVal (tabhandle, TAB_DE_ADDR, addr);  
		SetCtrlVal (tabhandle, TAB_DE_COMRATE, comrate);
		SetCtrlAttribute (tabhandle, TAB_DE_COMRATE, ATTR_DIMMED, sendMode);
		SetCtrlVal (tabhandle, TAB_DE_RESPONSEWAIT, rwait);  
		SetCtrlVal (tabhandle, TAB_DE_BYTESTOREAD, bytes);
		SetCtrlVal (tabhandle, TAB_DE_COMMAND, comd);
		
		SetCtrlVal (pnlvar, PNL_DATFILENAME, filename);
		SetCtrlVal (pnlvar, PNL_STRMAIL, emailaddr);
	}
	SetBreakOnLibraryErrors(1);
	
	return 0;
}



/****************************************/
/********       CVI MAIN     ************/
/****************************************/
/****************************************/

int main (int argc, char *argv[])
{
	if (InitCVIRTE (0, argv, 0) == 0)
	return -1;	/* out of memory */
	if ((pnlvar = LoadPanel (0, "DharmaSiPM.uir", PNL)) < 0)
	return -1;
	
	SetTabPageAttribute (pnlvar, PNL_TAB, NTAB_SXY, ATTR_VISIBLE, 0);
	DisplayPanel (pnlvar);
	LoadConfig();
	GetCtrlVal (pnlvar, PNL_STARTCHECK, &startcheck);
	if (startcheck==1) devcheckedAll = DevCheck(-1);
	
	RunUserInterface ();
	DiscardPanel (pnlvar); 
	return 0;
}



/****************************************/
/********       CALLBACKS     ***********/
/****************************************/


/****************************************/ 
/***** START MEASUREMENT FUNCTION *******/ 
/************     BEGIN     *************/
/****************************************/

int CVICALLBACK StartMeasurement (int panel, int control, int event,
void *callbackData, int eventData1, int eventData2)
{
	int i=0, j=0, ns=0, k=0, devch = 0,a=0, ncycle=0, cycles=0, enditter=0, range=0, capOK=0, autorange=0, rangeOK=0, tmpn1=0, yLeftPar=0, yRightPar=0;
	int relVbd=0, eventCount=0, wfmCount=0, eventCountRequested=0, err=0, HiokiLogScale=0, checkScale=0, autoScale=0, ifVoff=0, invert=0;
	
	double vstart=0, vstop=0, vstep=0, fstart=0, fstop=0, fstep=0, frestep=0, vmax=0, volt=0, h_volt=0, measWaitTime=0, currentMax=0.05, currentRange=0.00000002;
	double fvoltage=0, fcurrent=0, ftempr=0, capSerial=0, capParallel=0, resSerial=0, resParallel=0, fc=0, vc=0, vt=0, freq=0, incr=0;
	double vcdraw=0, vstartdraw=0, vstopdraw=0, Vbd=0,vcRecalc=0, currentResult=0,peakPlus=0,peakMinus=0,average=0,RMS=0,stdev=0,signalPtP=0,scopeScale=0.02,scopeWaitTime=0.1;
	
	char buf[1024]="", cmd[1024]="", resp[4096]="", tmp[1024]="", status[1024]="";

	switch (event)
	{
	case EVENT_COMMIT:		 
		
		GetActiveTabPage (pnlvar, PNL_TAB, &mode);
		if (mode == NTAB_DE)
		{
			sprintf(status,"Measurement cannot be started: wrong tab");
			SetCtrlVal (pnlvar, PNL_STATUS, status);
			return 0;
		}
		else runflag = 1;
		
		if (runflag==1)
		{
			// Prepare GUI
			SetCtrlVal (pnlvar, PNL_LEDRUN, 1);
			DimAllTabsCallbacksExcept(1, mode, -1, -1);
			SetCtrlAttribute (pnlvar, PNL_STOPMEAS, ATTR_DIMMED, 0);
			
			// Read input parameters from Main Panel
			GetPanelHandleFromTabPage(pnlvar, PNL_TAB, mode, &tabhandle);
			GetCtrlVal (pnlvar, PNL_HYSTERESIS, &hyst);
			GetCtrlVal (pnlvar, PNL_LIGHTECHO, &lightecho);
			GetCtrlVal (pnlvar, PNL_COMPORT, &comport);
			GetCtrlVal (pnlvar, PNL_VOLTAGEOFFATEND, &ifVoff);
			
			if (openFiles() == -1) {StopMeasurement(10,0); break;} 
			
			ClearBuff(buf,1024);
			ClearBuff(cmd,1024);
			popenX = -1;

			// Check if devices are ready
			if (devcheckedAll == 0 && devchecked[mode] == 0) devchecked[mode] = DevCheck(mode);
			SetCtrlVal (pnlvar, PNL_LEDRUN, 1);
			if (runflag == 0) {StopMeasurement(mode,0); break;}
			
			if (devcheckedAll == 1 || devchecked[mode] == 1)
			{
				if (GPIBMODE == 0) USBGPIBClear(comport, USBGPIB);
				//else err = viClear(rmHandle); //clear instrument's io buffers
				
				// Turn Zero Check ON and wait a bit
				CHECKSTOP( SendCommand(comport, K6517A, "syst:zch on", NORESP, NOREDO, 0, NULL) );
				SyncWaitLoop(2.0);
				// Set temperature measurement
				CHECKSTOP( SendCommand(comport, K6517A, "syst:tsc on", NORESP, NOREDO, 0, NULL) );	  
				if (mode==NTAB_CV || mode==NTAB_IV || mode==NTAB_CF) {CHECKSTOP( SendCommand(comport, K6517A, "form:elem read,etem", NORESP, NOREDO, 0, NULL) );}
				else if (mode==NTAB_SV || mode==NTAB_SXY) {CHECKSTOP( SendCommand(comport, K6517A, "form:elem etem", NORESP, NOREDO, 0, NULL) );} 
				// Set voltage range to 200 V
				CHECKSTOP( SendCommand(comport, K6517A, "volt:dc:rang 200", NORESP, NOREDO, 0, NULL) );
				// Set voltage/current measurement
				if (mode==NTAB_IV) {CHECKSTOP( SendCommand(comport, K6517A, "conf:curr:dc", NORESP, NOREDO, 0, NULL) );}
				else {CHECKSTOP( SendCommand(comport, K6517A, "conf:volt:dc", NORESP, NOREDO, 0, NULL) );}
				// Turn Zero Check OFF
				CHECKSTOP( SendCommand(comport, K6517A, "syst:zch off", NORESP, NOREDO, 0, NULL) ); 
				// Set voltage output
				if (ifVoff == 1 && voltageIsOn == 0) 
				{
					CHECKSTOP( SendCommand(comport, K6517A, "sour:volt 0.0", NORESP, NOREDO, 0, NULL) );
					CHECKSTOP( SendCommand(comport, K6517A, "outp:stat on", NORESP, NOREDO, 0, NULL) );
					voltageIsOn = 1;
				}
				// Check the error queues
				GPIBErrorQueue(K6517A, "#general_settings");
				// Get time
				GetDataTime(datime); 
				SetCtrlVal (pnlvar, PNL_STARTTIME, datime);
				if (runflag == 0) {StopMeasurement(mode,0); break;}
				
				
				/*******************************************/
				/***********   CV measurement   ************/
				/****************   BEGIN   ****************/
				/*******************************************/
				if(mode==NTAB_CV && runflag==1)
				{
					// Get input parameters from current tab 
					GetCtrlVal (tabhandle, TAB_CV_STARTVOLTAGE, &vstart);
					GetCtrlVal (tabhandle, TAB_CV_STOPVOLTAGE, &vstop);
					GetCtrlVal (tabhandle, TAB_CV_STEPVOLTAGE, &vstep);
					GetCtrlVal (tabhandle, TAB_CV_MAXVOLTAGE, &vmax);
					GetCtrlVal (tabhandle, TAB_CV_FREQ, &freq);
					GetCtrlVal (tabhandle, TAB_CV_HIOKI_VOLTAGE, &h_volt);
					GetCtrlVal (tabhandle, TAB_CV_MEASWAITTIME, &measWaitTime);
					GetCtrlVal (tabhandle, TAB_CV_CRPAR1, &yLeftPar); 
					GetCtrlVal (tabhandle, TAB_CV_CRPAR2, &yRightPar); 
					
					// Check for maximal voltage 
					if (vstop > vmax) {StopMeasurement(11,0); break;}
					
					// Prepare for measurement
					CRsetAxes(NTAB_CV);
					ns=(int)((vstop-vstart)/vstep);
					vt=0;
					k=0;
					if (hyst==1) {sprintf(tmp, "double"); tmpn1=ns*2+1;} // ns=ns*2; 
					else {sprintf(tmp, "single"); tmpn1=ns+1;}
					sprintf(status,"C(V) from %.2fV to %.2fV step %.2fV [%i points], %s ramping",vstart,vstop,vstep,tmpn1,tmp);
					SetCtrlVal (pnlvar, PNL_STATUS, status);
					SyncWaitLoop(-2); // user reading
					if (runflag == 0) {StopMeasurement(mode,0); break;}
					
					// Redraw old data
					if ((lightecho == 1) && (cvlighttrig == 1)) 
						for (i = 0; i<= cvglobalns; i++) 
						{
							PlotPoint (tabhandle, TAB_CV_GRAPH, cvdat[0][i], cvdat[yLeftPar][i], VAL_SOLID_CIRCLE,VAL_GRAY);
							if (yRightPar >= 0) 
							{
								SetCtrlAttribute (tabhandle, TAB_CV_GRAPH, ATTR_ACTIVE_YAXIS, VAL_RIGHT_YAXIS);	
								PlotPoint (tabhandle, TAB_CV_GRAPH, cvdat[0][i], cvdat[yRightPar][i], VAL_SOLID_SQUARE,VAL_LT_GRAY);
								SetCtrlAttribute (tabhandle, TAB_CV_GRAPH, ATTR_ACTIVE_YAXIS, VAL_LEFT_YAXIS);	
							}
						}
					cvlighttrig=0;
					
					// Set bias voltage
					sprintf(cmd,"sour:volt %lf",volt);
					CHECKSTOP( SendCommand(comport, K6517A, cmd, NORESP, NOREDO, 0, NULL) );
					// Hioki readout parameters, each parameter is 2^ParNumber from Hioki list (SumParMax=255): SumPar1=8(Cs)+16(Cp)=24; SumPar2=2(Rs)+8(Rp)=10
					CHECKSTOP( SendCommand(comport, HIOKICV, "MEAS:ITEM 24,10", NORESP, NOREDO, 0, NULL) );
					// Set Hioki parameters: header off, open-circuit voltage, voltage level, frequency
					CHECKSTOP( SendCommand(comport, HIOKICV, "HEAD OFF", NORESP, NOREDO, 0, NULL) );
					CHECKSTOP( SendCommand(comport, HIOKICV, "LEV V", NORESP, NOREDO, 0, NULL) ); 
					sprintf(cmd,"LEV:VOLT %lf",h_volt/1000.0);
					CHECKSTOP( SendCommand(comport, HIOKICV, cmd, NORESP, NOREDO, 0, NULL) );
					sprintf(cmd, "FREQ %lf", freq*1000);
					CHECKSTOP( SendCommand(comport, HIOKICV, cmd, NORESP, NOREDO, 0, NULL) ); 
					//  Wait for device to get ready
					SyncWaitLoop(10.0);
					CHECKSTOP( SendCommand(comport, HIOKICV, "*OPC?", RESP, NOREDO, 1, resp) );
					
					// Check the error queues
					GPIBErrorQueue(K6517A, "#general_settings");
					GPIBErrorQueue(HIOKICV, "#general_settings");
					
					//----------------  Start measurement cycle  ---------------------------//
					for(i=0;i<ns+1;i++)
					{
						// Set voltage
						if (hyst == 1)
						{
							if(ncycle==0)  vc=vt+(i*vstep);
							if(ncycle==1)  vc=vt-(i*vstep); 
							if(ncycle==2)  vc=vt+(i*vstep); 
						}
						else vc=vstart+(i*vstep);
						//if ( hyst == 1 && i >= (ns/2) ) vc=vstop-(i*vstep);
						//else vc=vstart+(i*vstep);
						sprintf(cmd,"sour:volt %lf",vc);
						CHECKSTOP( SendCommand(comport, K6517A, cmd, NORESP, NOREDO, 0, NULL) );
						
						// Get voltage, temperature and time
						CHECKSTOP( SendCommand(comport, K6517A, "READ?", RESP, NOREDO, 1024, resp) );
						sscanf(resp,"%lf,%lf",&fvoltage,&ftempr);
						SetCtrlVal (pnlvar, PNL_TEMPERATURE, ftempr);
						GetDataTime(datime); 
						SetCtrlVal (pnlvar, PNL_STOPTIME, datime);

						// Get capacitances and resistances
						cycles=0;
						capOK=0;
						while (capOK == 0)
						{
							//  Wait for device to get ready
							SyncWaitLoop(measWaitTime);	
							CHECKSTOP( SendCommand(comport, HIOKICV, "*OPC?", RESP, NOREDO, 1, resp) );
						
							CHECKSTOP( SendCommand(comport, HIOKICV, "MEAS?", RESP, NOREDO, 1024, resp) );
							sscanf(resp,"%lf,%lf,%lf,%lf",&capSerial,&capParallel,&resSerial,&resParallel);
							if( (capSerial > 10E6) || (capSerial < 10E-20) )
							{
								sprintf(status,"CF-meter over limit: attempt N%i\n",cycles);
								printf(status);
								//if(savlog) fprintf(logfile,buf);
								SetCtrlVal (pnlvar, PNL_STATUS, status);
								cycles++;
								if (cycles>=10) {StopMeasurement(mode,9); break;}
							}
							else capOK=1;
						}
						if ((runflag == 0 && stophow == -1) || cycles>=10) break;
						
						// Data printout and drawing
						sprintf(status,"%d: V=%.2f, T=%.2f, Cs=%.2eF, Cp=%.2eF, Rs=%.2eOhm, Rp=%.2eOhm",i,vc,ftempr,capSerial,capParallel,resSerial,resParallel);
						SetCtrlVal (pnlvar, PNL_STATUS, status);
						fprintf(datfile,"%lf, %lf, %lf, %le,%le,%le,%le,\n",fvoltage,ftempr,vc,capSerial,capParallel,resSerial,resParallel); // for consistency with old data
						if(savlog) fprintf(logfile,"V = %lf at %s\n",vc,TimeStr());
						
						cvdat[0][k] = vc;
						cvdat[1][k] = capSerial;
						cvdat[2][k] = capParallel;
						cvdat[3][k] = resSerial;
						cvdat[4][k] = resParallel;
						
						if(ncycle==0 || hyst == 0) PlotPoint (tabhandle, TAB_CV_GRAPH, cvdat[0][k], cvdat[yLeftPar][k], VAL_SOLID_CIRCLE,VAL_GREEN);
						if(ncycle==1) PlotPoint (tabhandle, TAB_CV_GRAPH, cvdat[0][k], cvdat[yLeftPar][k], VAL_SOLID_CIRCLE,VAL_CYAN);
						if(ncycle==2) PlotPoint (tabhandle, TAB_CV_GRAPH, cvdat[0][k], cvdat[yLeftPar][k], VAL_SOLID_CIRCLE,VAL_RED);
						if (yRightPar >=0) 
						{
							SetCtrlAttribute (tabhandle, TAB_CV_GRAPH, ATTR_ACTIVE_YAXIS, VAL_RIGHT_YAXIS);	
							PlotPoint (tabhandle, TAB_CV_GRAPH, cvdat[0][k], cvdat[yRightPar][k], VAL_SOLID_SQUARE,VAL_YELLOW);
							SetCtrlAttribute (tabhandle, TAB_CV_GRAPH, ATTR_ACTIVE_YAXIS, VAL_LEFT_YAXIS);	
						}
						
						k++;
						
						// For measurement with hysteresys
						if (vstart<=0 && hyst==1)
						{
							if((vc >= (vstop-2*vstep)) && (ncycle == 0) && (i>=((ns)-1)))
							{
								sprintf(status, "Go back 1... step is %lf",vstep);
								SetCtrlVal (pnlvar, PNL_STATUS, status);
								ncycle=1;
								i=0;
								vt=vstop;
							}
							if((vc <= vstart) && (ncycle ==1))
							{
								sprintf(status, "Go back 2... step is %lf",vstep);
								SetCtrlVal (pnlvar, PNL_STATUS, status);
								ncycle=2;
								i=0;
								vt=vstart;
							}
							if((vc >= 0) && (ncycle == 2))
							{
								sprintf(status, "End cycle V=%lf",vc);
								SetCtrlVal (pnlvar, PNL_STATUS, status);
								StopMeasurement(mode,0);
								ncycle=3;
								i= 0;
								break;
							}
						}
						
						if (runflag == 0) {StopMeasurement(mode,0); break;}
					}
					//---------------------------------------------//
					
					cvglobalns = k-1;
					cvlighttrig = 1;
					MSEND=1;
					if (i == ns+1) StopMeasurement(mode,0);
				}
				/*******************************************/
				/***********   CV measurement   ************/
				/****************    END    ****************/
				/*******************************************/
				
				
				
				/*******************************************/
				/***********   IV measurement   ************/
				/****************   BEGIN   ****************/
				/*******************************************/
				else if(mode==NTAB_IV && runflag==1)
				{
					// Get input parameters from current tab  
					GetCtrlVal (tabhandle, TAB_IV_STARTVOLTAGE, &vstart);
					GetCtrlVal (tabhandle, TAB_IV_STOPVOLTAGE, &vstop);
					GetCtrlVal (tabhandle, TAB_IV_STEPVOLTAGE, &vstep);
					GetCtrlVal (tabhandle, TAB_IV_MAXVOLTAGE, &vmax);
					GetCtrlVal (tabhandle, TAB_IV_MAXCURRENT, &currentMax);
					currentMax=currentMax/1000.0;
					GetCtrlVal (tabhandle, TAB_IV_AUTORANGE, &autorange);
					GetCtrlVal (tabhandle, TAB_IV_CURRENTRANGE, &currentRange);
					currentRange=currentRange/1000.0;
					GetCtrlVal (tabhandle, TAB_IV_MEASWAITTIME, &measWaitTime);
					
					// Check for maximal voltage 
					if (vstop > vmax) {StopMeasurement(11,0); break;}
					
					// Prepare for measurement
					ns=(int)((vstop-vstart)/vstep);
					sprintf(status,"I(V) from %.2fV to %.2fV step %.2fV [%i points]",vstart,vstop,vstep,ns+1);
					SetCtrlVal (pnlvar, PNL_STATUS, status);
					SyncWaitLoop(-2); // user reading
					if (runflag == 0) {StopMeasurement(mode,0); break;}
					
					// Redraw old data
					if ((lightecho == 1) && (ivlighttrig == 1)) 
						for (i = 0; i<= ivglobalns; i++) 
							PlotPoint (tabhandle, TAB_IV_GRAPH, ivdat[0][i], ivdat[1][i],  VAL_SOLID_CIRCLE, VAL_GRAY);
					ivlighttrig=0;
					
					// Set start current range value and auto-range mode
					sprintf(cmd,"curr:dc:rang %e",currentRange);
					CHECKSTOP( SendCommand(comport, K6517A, cmd, NORESP, NOREDO, 0, NULL) );		
					if (autorange == 0 || autorange == 1) {CHECKSTOP( SendCommand(comport, K6517A, "curr:rang:auto 0", NORESP, NOREDO, 0, NULL) );}
					else if (autorange == 2) {CHECKSTOP( SendCommand(comport, K6517A, "curr:rang:auto 1", NORESP, NOREDO, 0, NULL) );}
					
					// Check the error queues
					GPIBErrorQueue(K6517A, "#general_settings");
					
					//----------------  Start measurement cycle  ---------------------------//
					for(i=0;i<ns+1;i++)
					{
						// Set voltage
						//if (vc<60) vc=vstart+(i*vstep*10);
						//else 
						vc=vstart+(i*vstep);
						sprintf(cmd,"sour:volt %lf",vc);
						CHECKSTOP( SendCommand(comport, K6517A, cmd, NORESP, NOREDO, 0, NULL) );
						
						//  Wait for device to get ready
						SyncWaitLoop(measWaitTime);
						CHECKSTOP( SendCommand(comport, K6517A, "*OPC?", RESP, NOREDO, 1, resp) );
						
						// Get current, temperature and time
						rangeOK=0;
						cycles=0;
						while (rangeOK==0)
						{
							CHECKSTOP( SendCommand(comport, K6517A, "READ?", RESP, NOREDO, 1024, resp) );
							sscanf(resp,"%lf,%lf",&fcurrent,&ftempr);
							if(fcurrent<0) fcurrent=-1*fcurrent;
							
							// Increase range if requested
							if(autorange != 1 || (autorange == 1 && fcurrent<1) ) rangeOK=1;
							else
							{
								currentRange=currentRange*10.0;
								sprintf(cmd,"curr:dc:rang %e",currentRange);
								CHECKSTOP( SendCommand(comport, K6517A, cmd, NORESP, NOREDO, 0, NULL) );
								sprintf(status,"Current range increased to %eA",currentRange);
								SetCtrlVal (pnlvar, PNL_STATUS, status);
								SyncWaitLoop(IVRANGETIME);
								CHECKSTOP( SendCommand(comport, K6517A, "*OPC?", RESP, NOREDO, 1, resp) );
								cycles++;
								if (cycles>=10) {StopMeasurement(mode,9); break;}
							}
						}
						if ((runflag == 0 && stophow == -1) || cycles>=10) break;
						
						// Set temperature and get time
						SetCtrlVal (pnlvar, PNL_TEMPERATURE, ftempr);
						GetDataTime(datime); 
						SetCtrlVal (pnlvar, PNL_STOPTIME, datime);

						// Stop measurement on current overload
						if(fcurrent>=currentMax)
						{
							sprintf(status,"Current overload [%eA]",fcurrent);
							SetCtrlVal (pnlvar, PNL_STATUS, status);
							runflag=0;
							StopMeasurement(mode,0);
							break;
						}		  
						
						// Data printout and drawing
						sprintf(status,"%d: V=%.2f, T=%.2f, I=%.2eA",i,vc,ftempr,fcurrent);
						SetCtrlVal (pnlvar, PNL_STATUS, status);
						fprintf(datfile,"%lf\t%e\t%lf\n", vc, fcurrent, ftempr); // for consistency with old data
						if(savlog) fprintf(logfile,"V = %lf at %s\n",vc,TimeStr());
						PlotPoint (tabhandle, TAB_IV_GRAPH, vc, fcurrent, VAL_SOLID_CIRCLE,VAL_GREEN);
						
						ivdat[0][k] = vc;
						ivdat[1][k] = fcurrent;
						k++;    			
						
						if (runflag == 0) {StopMeasurement(mode,0); break;}
					}
					//---------------------------------------------//
					ivglobalns = k-1;
					ivlighttrig = 1;
					MSEND=1;  
					if (i == ns+1) StopMeasurement(mode,0);
				}			
				/*******************************************/
				/***********   IV measurement   ************/
				/****************    END    ****************/
				/*******************************************/
				
				
				/*******************************************/
				/***********   CF measurement   ************/
				/****************   BEGIN   ****************/
				/*******************************************/
				else if(mode==NTAB_CF && runflag==1)
				{
					// Get input parameters from current tab 
					GetCtrlVal (tabhandle, TAB_CF_STARTFREQ, &fstart);
					GetCtrlVal (tabhandle, TAB_CF_STOPFREQ, &fstop);
					GetCtrlVal (tabhandle, TAB_CF_STEPFREQ, &fstep);
					GetCtrlVal (tabhandle, TAB_CF_MAXVOLTAGE, &vmax);
					GetCtrlVal (tabhandle, TAB_CF_VOLTAGE, &volt);
					GetCtrlVal (tabhandle, TAB_CF_HIOKI_VOLTAGE, &h_volt);
					GetCtrlVal (tabhandle, TAB_CF_MEASWAITTIME, &measWaitTime);
					GetCtrlVal (tabhandle, TAB_CF_CRPAR1, &yLeftPar); 
					GetCtrlVal (tabhandle, TAB_CF_CRPAR2, &yRightPar);
					GetCtrlVal (tabhandle, TAB_CF_HIOKISCALE, &HiokiLogScale);
					
					// Check for maximal voltage 
					if (volt > vmax) {StopMeasurement(11,0); break;}
					
					// Prepare for measurement
					CRsetAxes(NTAB_CF);
					fstart=fstart*1000;
					fstop=fstop*1000;
					if (HiokiLogScale == 0)
					{										  
						fstep=fstep*1000;
						ns=(int)( (fstop-fstart)/fstep );
						sprintf(status,"C(F) from %.2fkHz to %.2fkHz step %.2fkHz [%i points]",fstart/1000,fstop/1000,fstep/1000,ns+1);
					}
					else if (HiokiLogScale == 1)
					{
						ns=(int)( log10(fstop/fstart)*(ceil(9/fstep)) );
						sprintf(status,"C(F) from %.2eHz to %.2eHz, %.0f steps per order of magnitude [%i points]",fstart,fstop,ceil(9/fstep),ns+1);
						fstep = fstep*pow(10,floor(log10(fstart)));
						frestep = 10*pow(10,floor(log10(fstart)));
					}
					SetCtrlVal (pnlvar, PNL_STATUS, status);
					SyncWaitLoop(-2); // user reading
					if (runflag == 0) {StopMeasurement(mode,0); break;}
					
					// Redraw old data
					if ((lightecho == 1) && (cflighttrig == 1)) 
						for (i = 0; i<= cfglobalns; i++) 
						{
							PlotPoint (tabhandle, TAB_CF_GRAPH, cfdat[0][i], cfdat[yLeftPar][i], VAL_SOLID_CIRCLE,VAL_GRAY);
							if (yRightPar >= 0) 
							{
								SetCtrlAttribute (tabhandle, TAB_CF_GRAPH, ATTR_ACTIVE_YAXIS, VAL_RIGHT_YAXIS);	
								PlotPoint (tabhandle, TAB_CF_GRAPH, cfdat[0][i], cfdat[yRightPar][i], VAL_SOLID_SQUARE,VAL_LT_GRAY);
								SetCtrlAttribute (tabhandle, TAB_CF_GRAPH, ATTR_ACTIVE_YAXIS, VAL_LEFT_YAXIS);
							}
						}
					cflighttrig=0;
					
					// Set bias voltage and measuring voltage
					sprintf(cmd,"sour:volt %lf",volt);
					CHECKSTOP( SendCommand(comport, K6517A, cmd, NORESP, NOREDO, 0, NULL) );
					// Hioki readout parameters, each parameter is 2^ParNumber from Hioki list (SumParMax=255): SumPar1=8(Cs)+16(Cp)=24; SumPar2=2(Rs)+8(Rp)=10
					CHECKSTOP( SendCommand(comport, HIOKICV, "MEAS:ITEM 24,10", NORESP, NOREDO, 0, NULL) );
					// Set Hioki parameters: header off, open-circuit voltage, voltage level
					CHECKSTOP( SendCommand(comport, HIOKICV, "HEAD OFF", NORESP, NOREDO, 0, NULL) );
					CHECKSTOP( SendCommand(comport, HIOKICV, "LEV V", NORESP, NOREDO, 0, NULL) ); 
					sprintf(cmd,"LEV:VOLT %lf",h_volt/1000.0);
					CHECKSTOP( SendCommand(comport, HIOKICV, cmd, NORESP, NOREDO, 0, NULL) );
					
					// Check the error queues
					GPIBErrorQueue(K6517A, "#general_settings");
					GPIBErrorQueue(HIOKICV, "#general_settings");
					
					//----------------  Start measurement cycle  ---------------------------//
					i=0;
					j=0;
					while(1)
					{
						fc=fstart+(j*fstep);
						if (HiokiLogScale == 1 && fc>=frestep) 
						{
							j=0; 
							fstart = frestep; 
							frestep=frestep*10; 
							fstep=fstep*10; 
							fc = fstart+(j*fstep);
						}
						if (fc>fstop) {StopMeasurement(mode,0); break;}
						
						// Set frequency
						sprintf(cmd, "FREQ %lf", fc);
						CHECKSTOP( SendCommand(comport, HIOKICV, cmd, NORESP, NOREDO, 0, NULL) ); 
						if (i==0) SyncWaitLoop(10.0); //  Wait for CF-meter to get ready
						
						// Get voltage, temperature and time
						CHECKSTOP( SendCommand(comport, K6517A, "READ?", RESP, NOREDO, 1024, resp) );
						sscanf(resp,"%lf,%lf",&fvoltage,&ftempr);
						SetCtrlVal (pnlvar, PNL_TEMPERATURE, ftempr);
						GetDataTime(datime); 
						SetCtrlVal (pnlvar, PNL_STOPTIME, datime);

						// Get capacitance
						cycles=0;
						capOK=0;
						while (capOK == 0)
						{
							//  Wait for device to get ready
							SyncWaitLoop(measWaitTime); 
							CHECKSTOP( SendCommand(comport, HIOKICV, "*OPC?", RESP, NOREDO, 1, resp) );
							
							CHECKSTOP( SendCommand(comport, HIOKICV, "MEAS?", RESP, NOREDO, 1024, resp) );
							sscanf(resp,"%lf,%lf,%lf,%lf",&capSerial,&capParallel,&resSerial,&resParallel);
							if( (capSerial > 10E6) || (capSerial < 10E-20) )
							{
								sprintf(status,"CF-meter over limit: attempt N%i",cycles);
								SetCtrlVal (pnlvar, PNL_STATUS, status);
								cycles++;
								if (cycles>=10) {StopMeasurement(mode,9); break;}
							}
							else capOK=1;
						}
						if ((runflag == 0 && stophow == -1) || cycles>=10) break;
						
						// Data printout and drawing
						sprintf(status,"%d: f=%.2eHz, T=%.2f, Cs=%.2eF, Cp=%.2eF, Rs=%.2eOhm, Rp=%.2eOhm",i,fc,ftempr,capSerial,capParallel,resSerial,resParallel);
						SetCtrlVal (pnlvar, PNL_STATUS, status);
						fprintf(datfile,"%le,%le,%le,%le,%le,%lf,%lf\n",fc,capSerial,capParallel,resSerial,resParallel,fvoltage,ftempr); // for semi-consistency with old data
						if(savlog) fprintf(logfile,"f = %lf at %s\n",fc,TimeStr());
						
						cfdat[0][i] = fc;
						cfdat[1][i] = capSerial;
						cfdat[2][i] = capParallel;
						cfdat[3][i] = resSerial;
						cfdat[4][i] = resParallel;
						
						PlotPoint (tabhandle, TAB_CF_GRAPH, cfdat[0][i], cfdat[yLeftPar][i], VAL_SOLID_CIRCLE,VAL_GREEN);
						if (yRightPar >=0) 
						{
							SetCtrlAttribute (tabhandle, TAB_CF_GRAPH, ATTR_ACTIVE_YAXIS, VAL_RIGHT_YAXIS);	
							PlotPoint (tabhandle, TAB_CF_GRAPH, cfdat[0][i], cfdat[yRightPar][i], VAL_SOLID_SQUARE,VAL_YELLOW);
							SetCtrlAttribute (tabhandle, TAB_CF_GRAPH, ATTR_ACTIVE_YAXIS, VAL_LEFT_YAXIS);
						}
						
						i++;
						j++;
						
						if (runflag == 0) {StopMeasurement(mode,0); break;}
					}	 
					//---------------------------------------------//
					
					cfglobalns = i-1;
					cflighttrig = 1;  
					MSEND=1;  
					if (i == ns+1) StopMeasurement(mode,0);
				}
				/*******************************************/
				/***********   CF measurement   ************/
				/****************    END    ****************/
				/*******************************************/
				
				
				/*******************************************/
				/********   Scope(V) measurement   *********/
				/****************   BEGIN   ****************/										
				/*******************************************/			   
				
				else if(mode==NTAB_SV && runflag==1)
				{
					// Get input parameters from current tab 
					GetCtrlVal (tabhandle, TAB_SV_STARTVOLTAGE, &vstart);
					GetCtrlVal (tabhandle, TAB_SV_STOPVOLTAGE, &vstop);
					GetCtrlVal (tabhandle, TAB_SV_STEPVOLTAGE, &vstep);
					GetCtrlVal (tabhandle, TAB_SV_MAXVOLTAGE, &vmax);
					GetCtrlVal (tabhandle, TAB_SV_SCOPETIME, &scopeWaitTime);
					GetCtrlVal (tabhandle, TAB_SV_EVENTCOUNT, &eventCountRequested);
					GetCtrlVal (tabhandle, TAB_SV_SCOPESCALE, &scopeScale);
					GetCtrlVal (tabhandle, TAB_SV_SCOPEAUTOSCALE, &autoScale);
					GetCtrlVal (tabhandle, TAB_SV_INVERT, &invert);
					GetCtrlVal (tabhandle, TAB_SV_RELVBD, &relVbd);
					GetCtrlVal (tabhandle, TAB_SV_VBD, &Vbd);
					
					vstartdraw=vstart;
					vstopdraw=vstop;
					if (relVbd) 
					{
						vstart=vstart+Vbd;
						vstop=vstop+Vbd;
					}
					
					// Check for maximal voltage 
					if (vstop > vmax) {StopMeasurement(11,0); break;}
					
					// Prepare for measurement
					ns=(int)((vstop-vstart)/vstep);
					sprintf(status,"Scope(V) from %.2fV to %.2fV step %.2fV [%i points]",vstart,vstop,vstep,ns+1);
					SetCtrlVal (pnlvar, PNL_STATUS, status);
					SyncWaitLoop(-2); // user reading
					if (runflag == 0) {StopMeasurement(mode,0); break;}
					
					// Redraw old data
					if ((lightecho == 1) && (svlighttrig == 1)) 
						for (i = 0; i<= svglobalns; i++) 
						{
							PlotPoint (tabhandle, TAB_SV_GRAPH, svdat[0][i], svdat[1][i], VAL_SOLID_CIRCLE, VAL_GRAY);
							PlotPoint (tabhandle, TAB_SV_GRAPH, svdat[0][i], svdat[1][i]+svdat[2][i], VAL_CROSS,VAL_GRAY);
							PlotPoint (tabhandle, TAB_SV_GRAPH, svdat[0][i], svdat[1][i]-svdat[2][i], VAL_CROSS,VAL_GRAY);
						}
					svlighttrig=0;
					
					// Set scope display and scale
					CHECKSTOP( SendCommand(comport, RS1024, "SYST:DISP:UPD 1", NORESP, NOREDO, 0, NULL) );
					if (scopeScale < 0.001) scopeScale = 0.001; // V/div
					else if (scopeScale > 1.0) scopeScale = 1.0; // V/div
					sprintf(cmd,"CHAN%d:SCAL %lf", SCOPEMEASCHAN, scopeScale);
					CHECKSTOP( SendCommand(comport, RS1024, cmd, NORESP, NOREDO, 0, NULL) );
					
					// Check the error queues
					GPIBErrorQueue(K6517A, "#general_settings");
					GPIBErrorQueue(RS1024, "#general_settings");
					
					//----------------  Start measurement cycle  ---------------------------//
					for(i=0;i<ns+1;i++)
					{
						// Set voltage
						vc=vstart+(i*vstep);
						if (relVbd) vcdraw=vstartdraw+(i*vstep);
						else vcdraw = vc;
						vcRecalc=VrecalcToDrop(vc);
						sprintf(cmd,"sour:volt %lf",vcRecalc);
						CHECKSTOP( SendCommand(comport, K6517A, cmd, NORESP, NOREDO, 0, NULL) );
						
						//  Wait for device to get ready
						SyncWaitLoop(0.1);
						
						// Scope scale adjustment (increasing)
						signalPtP = 0;
						checkScale = 1;
						while (autoScale && checkScale && scopeScale<1.0)   
						{
							sprintf(cmd,"MEAS%d:ARES?", SCOPESCALEMEAS);
							CHECKSTOP( SendCommand(comport, RS1024, cmd, RESP, NOREDO, 1024, resp) );
							sscanf(resp,"%lf",&signalPtP);
							if (signalPtP > 0.7*scopeScale*10) 
							{
								scopeScale = scopeScale*2; // V/div
								if (scopeScale > 1.0) scopeScale = 1.0;
								sprintf(cmd,"CHAN%d:SCAL %lf", SCOPEMEASCHAN, scopeScale);
								CHECKSTOP( SendCommand(comport, RS1024, cmd, NORESP, NOREDO, 0, NULL) );
								SyncWaitLoop(1);
							}
							else checkScale = 0;
						}
						if (runflag == 0 && stophow == -1) break;
						
						// Scope reset results
						CHECKSTOP( SendCommand(comport, RS1024, "MEAS:STAT:RES", NORESP, NOREDO, 0, NULL) );
						
						// Get temperature and time
						CHECKSTOP( SendCommand(comport, K6517A, "READ?", RESP, NOREDO, 1024, resp) );
						sscanf(resp,"%lf",&ftempr);
						SetCtrlVal (pnlvar, PNL_TEMPERATURE, ftempr);
						if(savlog) fprintf(logfile,"V = %lf at %s\n", vc, TimeStr());  
						GetDataTime(datime); 
						SetCtrlVal (pnlvar, PNL_STOPTIME, datime);
						
						// Scope data taking
						eventCount = 0;
						while (eventCount<eventCountRequested)
						{
							//  Wait for device to get ready
							CHECKSTOP( SendCommand(comport, RS1024, "*OPC?", RESP, NOREDO, 1, resp) );
							
							sprintf(cmd,"MEAS%d:ARES?", SCOPEMAINMEAS);
							CHECKSTOP( SendCommand(comport, RS1024, cmd, RESP, NOREDO, 1024, resp) );
							sscanf(resp,"%le,%le,%le,%le,%le,%le,%d,%d",&currentResult,&peakPlus,&peakMinus,&average,&RMS,&stdev,&eventCount,&wfmCount);
							if (eventCount<eventCountRequested) SyncWaitLoop(scopeWaitTime);
						}
						if (runflag == 0 && stophow == -1) break;
						
						//Data printout and drawing
						if (invert) average = -average;
						sprintf(status,"%d: V=%.2f, T=%.2f, Average=%.2enV*s, StDev=%.2enV*s from %d events",i,vc,ftempr,average,stdev,eventCount);
						SetCtrlVal (pnlvar, PNL_STATUS, status);
						fprintf(datfile,"%lf, %lf, %s\n", vc, ftempr, resp);
						PlotPoint (tabhandle, TAB_SV_GRAPH, vcdraw, average, VAL_SOLID_CIRCLE,VAL_GREEN);
						PlotPoint (tabhandle, TAB_SV_GRAPH, vcdraw, average+stdev, VAL_CROSS,VAL_GREEN);
						PlotPoint (tabhandle, TAB_SV_GRAPH, vcdraw, average-stdev, VAL_CROSS,VAL_GREEN);
						
						svdat[0][i] = vcdraw;
						svdat[1][i] = average;
						svdat[2][i] = stdev;
						
						if (runflag == 0) {StopMeasurement(mode,0); break;}
						
					}
					//---------------------------------------------//
					svglobalns = i-1;
					svlighttrig = 1;
					MSEND=1;  
					if (i == ns+1) StopMeasurement(mode,0); //  || runflag == 0
				}
				
				/*******************************************/
				/********   Scope(V) measurement   *********/
				/****************    END    ****************/
				/*******************************************/
			}
			else
			{
				SetCtrlVal (pnlvar, PNL_STATUS, "Some devices are not ready, check again please");
				StopMeasurement(13, 0);
			}
				
		}
		
		runflag = 1;			
		break;
	}
	
	return 0;
}

/****************************************/ 
/***** START MEASUREMENT FUNCTION *******/ 
/************      END      *************/
/****************************************/


/******************* Load measured data from disk  ***********************/
int CVICALLBACK Dataload (int panel, int control, int event,
void *callbackData, int eventData1, int eventData2)
{
	int i, filesel, eventCount, wfmCount, invert,relVbd, yLeftPar=0, yRightPar=0;
	double fvoltage, freq, cap, volt, temp, curr, capSerial=0, capParallel=0, resSerial=0, resParallel=0; 
	double currentResult=0,peakPlus=0,peakMinus=0,average=0,RMS=0,stdev=0, vrelstart=-2, vrelstep=0.1;
	char status[300]="";
	
	switch (event)
	{
		
	case EVENT_COMMIT:
		
		filesel = FileSelectPopup ("", "*.dat", "", "Load data file", VAL_LOAD_BUTTON, 0, 0, 1, 0, datname);
		if (filesel == 1)
		{
			GetCtrlVal (pnlvar, PNL_LIGHTECHO, &lightecho);
			GetActiveTabPage (pnlvar, PNL_TAB, &mode);
			GetPanelHandleFromTabPage(pnlvar, PNL_TAB, mode, &tabhandle);
			SetCtrlVal (pnlvar, PNL_LEDRUN, 1);
			DimAllTabsCallbacksExcept(1, mode, -1, -1);
			
			datfile=fopen(datname,"r"); 
			
			if (mode==NTAB_CV)
			{
				GetCtrlVal (tabhandle, TAB_CV_CRPAR1, &yLeftPar); 
				GetCtrlVal (tabhandle, TAB_CV_CRPAR2, &yRightPar);
				CRsetAxes(NTAB_CV);
				
				if ((lightecho == 1) && (cvlighttrig == 1)) 
					for (i = 0; i<= cvglobalns; i++) 
					{
						PlotPoint (tabhandle, TAB_CV_GRAPH, cvdat[0][i], cvdat[yLeftPar][i], VAL_SOLID_CIRCLE,VAL_GRAY);
						if (yRightPar >= 0) 
						{
							SetCtrlAttribute (tabhandle, TAB_CV_GRAPH, ATTR_ACTIVE_YAXIS, VAL_RIGHT_YAXIS);	
							PlotPoint (tabhandle, TAB_CV_GRAPH, cvdat[0][i], cvdat[yRightPar][i], VAL_SOLID_SQUARE,VAL_LT_GRAY);
							SetCtrlAttribute (tabhandle, TAB_CV_GRAPH, ATTR_ACTIVE_YAXIS, VAL_LEFT_YAXIS);
						}
					}
				
				i = 0;
				while (!feof(datfile)) 
				{
					fscanf (datfile,"%lf,%lf,%lf,%le,%le,%le,%le,\n",&fvoltage,&temp,&volt,&capSerial,&capParallel,&resSerial,&resParallel);
					if (debugging) printf("%lf, %lf, %lf, %le, %le, %le, %le\n",fvoltage,temp,volt,capSerial,capParallel,resSerial,resParallel); //printout
					
					cvdat[0][i] = volt;
					cvdat[1][i] = capSerial;
					cvdat[2][i] = capParallel;
					cvdat[3][i] = resSerial;
					cvdat[4][i] = resParallel;
					
					PlotPoint (tabhandle, TAB_CV_GRAPH, cvdat[0][i], cvdat[yLeftPar][i], VAL_SOLID_CIRCLE,VAL_GREEN);
					if (yRightPar >=0) 
					{
						SetCtrlAttribute (tabhandle, TAB_CV_GRAPH, ATTR_ACTIVE_YAXIS, VAL_RIGHT_YAXIS);	
						PlotPoint (tabhandle, TAB_CV_GRAPH, cvdat[0][i], cvdat[yRightPar][i], VAL_SOLID_SQUARE,VAL_YELLOW);
						SetCtrlAttribute (tabhandle, TAB_CV_GRAPH, ATTR_ACTIVE_YAXIS, VAL_LEFT_YAXIS);
					}
					
					i++;
				}
				cvglobalns = i-1;
				cvlighttrig = 1;
			}
			
			else if (mode==NTAB_IV)
			{
				if ((lightecho == 1) && (ivlighttrig == 1)) 
					for (i = 0; i<= ivglobalns; i++) 
						PlotPoint (tabhandle, TAB_IV_GRAPH, ivdat[0][i], ivdat[1][i], VAL_SOLID_CIRCLE, VAL_GRAY);
				
				i = 0;
				while (!feof(datfile)) 
				{
					fscanf (datfile,"%lf\t%le\t%lf\n",&volt,&curr,&temp);
					if (curr < 0) curr = -curr;
					if (debugging) printf("%lf, %le\n",volt,curr); //printout
					PlotPoint (tabhandle, TAB_IV_GRAPH, volt, curr, VAL_SOLID_CIRCLE,VAL_GREEN);
					
					ivdat[0][i] = volt;
					ivdat[1][i] = curr;
					
					i++;
				}
				ivglobalns = i-1;
				ivlighttrig = 1;
			}
			
			else if (mode==NTAB_CF) 
			{
				GetCtrlVal (tabhandle, TAB_CF_CRPAR1, &yLeftPar); 
				GetCtrlVal (tabhandle, TAB_CF_CRPAR2, &yRightPar);
				CRsetAxes(NTAB_CF);
				
				if ((lightecho == 1) && (cflighttrig == 1)) 
					for (i = 0; i<= cfglobalns; i++) 
					{
						PlotPoint (tabhandle, TAB_CF_GRAPH, cfdat[0][i], cfdat[yLeftPar][i], VAL_SOLID_CIRCLE,VAL_GRAY);
						if (yRightPar >= 0) 
						{
							SetCtrlAttribute (tabhandle, TAB_CF_GRAPH, ATTR_ACTIVE_YAXIS, VAL_RIGHT_YAXIS);	
							PlotPoint (tabhandle, TAB_CF_GRAPH, cfdat[0][i], cfdat[yRightPar][i], VAL_SOLID_SQUARE,VAL_LT_GRAY);
							SetCtrlAttribute (tabhandle, TAB_CF_GRAPH, ATTR_ACTIVE_YAXIS, VAL_LEFT_YAXIS);
						}
					}
				
				i = 0;
				while (!feof(datfile)) 
				{ 
					fscanf (datfile,"%le,%le,%le,%le,%le,%lf,%lf\n",&freq,&capSerial,&capParallel,&resSerial,&resParallel,&volt,&temp);
					if (debugging) printf("%le, %le, %le, %le, %le, %lf, %lf\n",freq,capSerial,capParallel,resSerial,resParallel,volt,temp); //printout
					
					cfdat[0][i] = freq;
					cfdat[1][i] = capSerial;
					cfdat[2][i] = capParallel;
					cfdat[3][i] = resSerial;
					cfdat[4][i] = resParallel;
					
					PlotPoint (tabhandle, TAB_CF_GRAPH, cfdat[0][i], cfdat[yLeftPar][i], VAL_SOLID_CIRCLE,VAL_GREEN);
					if (yRightPar >=0) 
					{
						SetCtrlAttribute (tabhandle, TAB_CF_GRAPH, ATTR_ACTIVE_YAXIS, VAL_RIGHT_YAXIS);	
						PlotPoint (tabhandle, TAB_CF_GRAPH, cfdat[0][i], cfdat[yRightPar][i], VAL_SOLID_SQUARE,VAL_YELLOW);
						SetCtrlAttribute (tabhandle, TAB_CF_GRAPH, ATTR_ACTIVE_YAXIS, VAL_LEFT_YAXIS);
					}
					
					i++;
				}
				cfglobalns = i-1;
				cflighttrig = 1;
			}
			
			else if (mode==NTAB_SV)
			{
				if ((lightecho == 1) && (svlighttrig == 1)) 
					for (i = 0; i<= svglobalns; i++) 
					{
						PlotPoint (tabhandle, TAB_SV_GRAPH, svdat[0][i], svdat[1][i], VAL_SOLID_CIRCLE, VAL_GRAY);
						PlotPoint (tabhandle, TAB_SV_GRAPH, svdat[0][i], svdat[1][i]+svdat[2][i], VAL_CROSS,VAL_GRAY);
						PlotPoint (tabhandle, TAB_SV_GRAPH, svdat[0][i], svdat[1][i]-svdat[2][i], VAL_CROSS,VAL_GRAY);
					}
				
				GetCtrlVal (tabhandle, TAB_SV_RELVBD, &relVbd);
				GetCtrlVal (tabhandle, TAB_SV_INVERT, &invert);
				i = 0;
				while (!feof(datfile)) 
				{
					fscanf (datfile,"%lf, %lf, %le,%le,%le,%le,%le,%le,%d,%d\n",&volt,&temp,&currentResult,&peakPlus,&peakMinus,&average,&RMS,&stdev,&eventCount,&wfmCount);
					if (debugging) printf("%lf, %le, %le\n",volt,average,stdev); //printout
					if (relVbd) volt=vrelstart+vrelstep*i;
					if (invert) average = -average;
					PlotPoint (tabhandle, TAB_SV_GRAPH, volt, average, VAL_SOLID_CIRCLE,VAL_GREEN);
					PlotPoint (tabhandle, TAB_SV_GRAPH, volt, average+stdev, VAL_CROSS,VAL_GREEN);
					PlotPoint (tabhandle, TAB_SV_GRAPH, volt, average-stdev, VAL_CROSS,VAL_GREEN);
					
					svdat[0][i] = volt;
					svdat[1][i] = average;
					svdat[2][i] = stdev;
					
					i++;
				}
				svglobalns = i-1;
				svlighttrig = 1;
			}
			
			fclose(datfile);
			sprintf(status,"File %s loaded",datname);
			SetCtrlVal (pnlvar, PNL_STATUS, status);
		}
		
		SetCtrlVal (pnlvar, PNL_LEDRUN, 0);
		DimAllTabsCallbacksExcept(0, -1, -1, -1);
		
		break;
	}
	return 0;
}

/******************* Callback for device checking  ***********************/
int CVICALLBACK Devcheckcall (int panel, int control, int event,
void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
	case EVENT_COMMIT:
		
		DimAllTabsCallbacksExcept(1, -1, -1, -1);
		
		devcheckedAll = DevCheck(-1);
		
		DimAllTabsCallbacksExcept(0, -1, -1, -1);
		
		break;
	}
	return 0;
}

/******************* Clear graph  ***********************/
int CVICALLBACK ClearGraph (int panel, int control, int event,
void *callbackData, int eventData1, int eventData2)
{
	int tabid;
	switch (event)
	{
	case EVENT_COMMIT:
		GetActiveTabPage (pnlvar, PNL_TAB, &mode);
		GetPanelHandleFromTabPage(pnlvar, PNL_TAB, mode, &tabhandle);
		if (mode == NTAB_CV) 
		{
			tabid = TAB_CV_GRAPH;
			cvlighttrig = 0;
		}
		if (mode == NTAB_IV) 
		{
			tabid = TAB_IV_GRAPH;
			ivlighttrig = 0;
		}
		if (mode == NTAB_CF) 
		{
			tabid = TAB_CF_GRAPH;
			cflighttrig = 0;
		}
		if (mode == NTAB_SV) 
		{
			tabid = TAB_SV_GRAPH;
			svlighttrig = 0;
		}
		
		if (mode != NTAB_DE) DeleteGraphPlot (tabhandle, tabid, -1, VAL_IMMEDIATE_DRAW);

		break;
	}						  
	return 0;
}

/******************* Enable email notification at end of measurement  ***********************/
int CVICALLBACK MailEnable (int panel, int control, int event,
void *callbackData, int eventData1, int eventData2)
{
	int mailEnb=0;
	char buf[100]="";
	
	switch (event)
	{
	case EVENT_COMMIT:
		
		GetCtrlVal (pnlvar, PNL_MAILENB,&mailEnb);
		ClearBuff(buf,100);
		ClearBuff(emailaddr,100);  
		if(mailEnb==1)
		{
			SetCtrlAttribute (pnlvar, PNL_STRMAIL, ATTR_DIMMED, 0); 
			GetCtrlVal (pnlvar, PNL_STRMAIL,&emailaddr[0]); 
			sprintf(buf,"smail.exe %s end.txt 1",emailaddr);
			ClearBuff(emailaddr,100); 
			strncpy(&emailaddr[0],&buf[0], strlen(buf));
		}
		else
		{
			SetCtrlAttribute (pnlvar, PNL_STRMAIL, ATTR_DIMMED, 1);	
		} 
		
		break;
	}
	return 0;
}

/******************* Quit software  ***********************/
int CVICALLBACK Quit (int panel, int control, int event,
void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
	case EVENT_COMMIT:
		if(popen == 0) 
		{
			CloseGPIB(comport);
			popen = -1;
		}
		if(popen2D == 0)
		{
			CloseCOM(comport2D);
			popen2D = -1;
		}
		SyncWaitLoop(0.5); // user reading long
		
		SaveConfig();
		
		QuitUserInterface (0);
		break;
	}
	return 0;
}


/******************* Turn off Keithley bias voltage  ***********************/ 
int CVICALLBACK VoltageOFF (int panel, int control, int event,
void *callbackData, int eventData1, int eventData2)
{
	int err=-1;
	char status[300]="", resp[4096]="";
	
	switch (event)
	{
	case EVENT_COMMIT:
		
		DimAllTabsCallbacksExcept(1, -1, -1, -1);
		if (popen!=0) popen = InitGPIB(0);
		GetCtrlVal (pnlvar, PNL_PORT_6517A, &K6517A);
		if (GPIBMODE == 1) {err = InitViDev(K6517A, 0, 1); SyncWaitLoop(-2);} // user reading
		
		SendCommand(comport, K6517A, "sour:volt 0.0", NORESP, NOREDO, 0, NULL);
		SendCommand(comport, K6517A, "outp:stat off", NORESP, NOREDO, 0, NULL);
		SendCommand(comport, K6517A, "outp:stat?", RESP, NOREDO, 1, resp);
		if (resp[0]=='0')
		{
			voltageIsOn = 0;
			sprintf(status,"Voltage is off");
		}
		else 
		{
			voltageIsOn = 1;
			sprintf(status,", Failed to turn off voltage, it may be still on!");
		}
		GPIBErrorQueue(K6517A, "#voltage_off");
		SetCtrlVal (pnlvar, PNL_STATUS, status);
		SetCtrlVal (pnlvar, PNL_LEDRUN,0);
		DimAllTabsCallbacksExcept(0, -1, -1, -1);
		
		break;
	}
	return 0;
}

/******************* Send command in debug mode  ***********************/ 
int CVICALLBACK SendGenericCommand (int panel, int control, int event,
void *callbackData, int eventData1, int eventData2)
{
	int i=0, err = 10, port = -1, godevnum = 0, goresponsewait = 0, interrupted = 0, gobytestoread = 1000, gorate=0;
	char gocommand[1024]="", comd[1024]="", goresponse[4096]="", buf[4096]="", smode[100]="", status[1024]="";
	
	switch (event)
	{
	case EVENT_COMMIT:
		
		SetCtrlVal (pnlvar, PNL_LEDRUN, 1);
		DimAllTabsCallbacksExcept(1, NTAB_DE, -1, -1);
		SetCtrlAttribute (pnlvar, PNL_STOPMEAS, ATTR_DIMMED, 0);
		GetPanelHandleFromTabPage(pnlvar, PNL_TAB, NTAB_DE, &tabhandle);
		GetCtrlVal (pnlvar, PNL_COMPORT, &port);
		GetCtrlVal (tabhandle, TAB_DE_ADDR, &godevnum);
		GetCtrlVal (tabhandle, TAB_DE_SENDMODE, &sendMode);
		ClearBuff(gocommand,1024);
		GetCtrlVal (tabhandle, TAB_DE_COMMAND, gocommand);
		GetCtrlVal (tabhandle, TAB_DE_RESPONSEWAIT, &goresponsewait);
		GetCtrlVal (tabhandle, TAB_DE_BYTESTOREAD, &gobytestoread); 
		GetCtrlVal (tabhandle, TAB_DE_COMRATE, &gorate); 												   
		
		if (sendMode == 1) 
		{
			sprintf(smode, "GPIB::%d", godevnum);
			if (popen != 0) popen = InitGPIB(0);
		}
		else if (sendMode == 0) 
		{
			sprintf(smode, "COM::%d",godevnum);
			port = godevnum; 
			godevnum=-1; 
			popen2D = 0;
			popenX = InitCOM(port, gorate); 
		}
		if (GPIBMODE == 1 && sendMode == 1) {err = InitViDev(godevnum, 0, 1); SyncWaitLoop(-2);} // user reading
		
		sprintf(comd, "[%s] > %s\n", smode, gocommand);
		SetCtrlVal (tabhandle, TAB_DE_RESPONSE, comd);	
		if (SendCommand(port, godevnum, gocommand, goresponsewait, NOREDO, gobytestoread, buf) == -1) interrupted = 1;
		if (strlen(buf)>0) sprintf(goresponse, "%s\n", buf);
		
		if (goresponsewait == 1 && interrupted == 0) SetCtrlVal (tabhandle, TAB_DE_RESPONSE, goresponse);
		else if (interrupted == 1)
		{
			sprintf(status, "USB-GPIB debug command was terminated");
			SetCtrlVal (pnlvar, PNL_STATUS, status);
		}
		
		if (sendMode == 1) GPIBErrorQueue(godevnum, gocommand);
		
		DimAllTabsCallbacksExcept(0, -1, -1, -1);
		SetCtrlAttribute (pnlvar, PNL_STOPMEAS, ATTR_DIMMED, 1);
		SetCtrlVal (pnlvar, PNL_LEDRUN, 0);

		break;
	}
	return 0;
}

/******************* Clear terminal in debug mode  ***********************/ 
int CVICALLBACK ClearTerminal (int panel, int control, int event,
void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
	case EVENT_COMMIT:
		
		GetPanelHandleFromTabPage(pnlvar, PNL_TAB, NTAB_DE, &tabhandle);
		DeleteTextBoxLines (tabhandle, TAB_DE_RESPONSE, 0, -1);

		break;
	}
	return 0;
}

/******************* Set delay for synchorinsation of communication with devices  ***********************/ 
int CVICALLBACK SetSyncWaitTime (int panel, int control, int event,
void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
	case EVENT_COMMIT:
		
		GetCtrlVal (pnlvar, PNL_SYNCWAITTIME, &syncwaittime);

		break;
	}
	return 0;
}

/******************* Callback to stop the measurement   ***********************/  
int CVICALLBACK StopMeasurementCall (int panel, int control, int event,
void *callbackData, int eventData1, int eventData2)
{
	int answer = 0;
	char textTerminate[10000]="If yes, current measurement step will be terminated and the measurement will be stopped.\n"
		"Keithley6517A voltage output will be turned off.\n"
		"All the data acquired in the current measurement step will be lost.\n"
		"Are you sure?";
	char textStop[10000]="If yes, current measurement step will be stopped after complition of the current step.\n"
		"Keithley6517A voltage output will be turned off.\n"
		"Are you sure?";
	
	switch (event)
	{
	case EVENT_COMMIT:
		
		GetCtrlVal (pnlvar, PNL_STOPHOW, &stophow);
		if (stophow == -1) answer = ConfirmPopup ("Are you sure to terminate the measurement?", textTerminate);
		if (stophow == 1) answer = ConfirmPopup ("Are you sure to stop the measurement?", textStop);

		if (answer == 1) runflag = 0;

		break;
	}
	return 0;
}

/******************* Set mode for device comunication in debug mode   ***********************/  
int CVICALLBACK SetSendMode (int panel, int control, int event,
void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
	case EVENT_COMMIT:
		
		GetPanelHandleFromTabPage(pnlvar, PNL_TAB, NTAB_DE, &tabhandle);
		GetCtrlVal (tabhandle, TAB_DE_SENDMODE, &sendMode);
		SetCtrlAttribute (tabhandle, TAB_DE_COMRATE, ATTR_DIMMED, sendMode);

		break;
	}
	return 0;
}

/******************* Load RS1024 oscilloscope settings    ***********************/  
int CVICALLBACK ScopeLoad (int panel, int control, int event,
void *callbackData, int eventData1, int eventData2)
{
	int err = 0, setting = 0, stb=0;
	char cmd[1024] = "", resp[4096]="", status[300]="";
	
	switch (event)
	{
	case EVENT_COMMIT:
		
		DimAllTabsCallbacksExcept(1, -1, -1, -1);
		
		if (popen!=0) popen = InitGPIB(0);
		GetCtrlVal (pnlvar, PNL_PORT_1024, &RS1024);
		if (GPIBMODE == 1) {err = InitViDev(RS1024, 0, 1); SyncWaitLoop(-2);} // user reading
		GPIBErrorQueue(RS1024, "#before_scope_load_setting");
		
		GetCtrlVal (pnlvar, PNL_SCOPESETTING, &setting);
		sprintf(cmd,"*RCL %d;*OPC?",setting);
		err = SendCommand(comport, RS1024, cmd, RESP, NOREDO, 1, resp);
		stb = GPIBErrorQueue(RS1024, cmd);
		
		if (err && !stb && resp[0]=='1') sprintf(status, "Scope setting %d loaded", setting);
		else sprintf(status, "Scope setting %d not loaded", setting);
		SetCtrlVal (pnlvar, PNL_STATUS, status);
		
		DimAllTabsCallbacksExcept(0, -1, -1, -1);

		break;
	}
	return 0;
}

/******************* Save RS1024 oscilloscope settings    ***********************/  
int CVICALLBACK ScopeSave (int panel, int control, int event,
void *callbackData, int eventData1, int eventData2)
{
	int err = 0, setting = 0, stb = 0;
	char cmd[1024] = "", resp[4096]="", status[300]="";
	
	switch (event)
	{
	case EVENT_COMMIT:
		
		DimAllTabsCallbacksExcept(1, -1, -1, -1);
		
		if (popen!=0) popen = InitGPIB(0);
		GetCtrlVal (pnlvar, PNL_PORT_1024, &RS1024);
		if (GPIBMODE == 1) {err = InitViDev(RS1024, 0, 1); SyncWaitLoop(-2);} // user reading
		GPIBErrorQueue(RS1024, "#before_scope_save_setting");
		
		GetCtrlVal (pnlvar, PNL_SCOPESETTING, &setting);
		sprintf(cmd,"*SAV %d;*OPC?",setting);
		err = SendCommand(comport, RS1024, cmd, RESP, NOREDO, 1, resp);
		stb = GPIBErrorQueue(RS1024, cmd);
		
		if (err && !stb && resp[0]=='1') sprintf(status, "Scope setting %d saved", setting);
		else sprintf(status, "Scope setting %d not saved", setting);
		SetCtrlVal (pnlvar, PNL_STATUS, status);
		
		
		DimAllTabsCallbacksExcept(0, -1, -1, -1);

		break;
	}
	return 0;
}

/******************* Show/hide raw messages from devices in terminal   ***********************/  
int CVICALLBACK DebugInit (int panel, int control, int event,
void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
	case EVENT_COMMIT:
		
		GetCtrlVal (pnlvar, PNL_DEBUGPRINT, &debugging);

		break;
	}
	return 0;
}

int CVICALLBACK SetGpibMode (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int i;
	char status[300]="";
	
	switch (event)
	{
		case EVENT_COMMIT:
			
			GetCtrlVal (pnlvar, PNL_SETGPIBMODE, &GPIBMODE);
			GetCtrlVal (pnlvar, PNL_COMPORT, &comport);
			
			popen = -1;
			popen2D = -1;
			popenX = -1;
			rmHandle=0;
			HM8112Handle=0;
			K6517AHandle=0;
			HIOKICVHandle=0;
			RS1024Handle=0;
			devcheckedAll = 0;
			for (i=0;i<10;i++) devchecked[i] = 0;

			if (GPIBMODE == 1) SetCtrlAttribute (pnlvar, PNL_COMPORT, ATTR_DIMMED, 1);
			else if (GPIBMODE == 0) SetCtrlAttribute (pnlvar, PNL_COMPORT, ATTR_DIMMED, 0);
			SetCtrlVal (pnlvar, PNL_COM_LED, 0);
			SetCtrlVal (pnlvar, PNL_KEITH_LED, 0);
			SetCtrlVal (pnlvar, PNL_HIOKI_LED, 0);
			SetCtrlVal (pnlvar, PNL_RS_LED, 0);
			SetCtrlVal (pnlvar, PNL_XYMOVER, 0);

			if (GPIBMODE == 1) sprintf (status, "Mode NI-GPIB via Visa is chosen, please init the devices again");
			if (GPIBMODE == 0) sprintf (status, "Mode USB-GPIB via COM is chosen, please init the devices again");
			SetCtrlVal (pnlvar, PNL_STATUS, status);

			break;
	}
	return 0;
}

int CVICALLBACK HiokiChangeScale (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int HiokiLogScale=0;
	
	switch (event)
	{
		case EVENT_COMMIT:
			
			SetHiokiScale();

			break;
	}
	return 0;
}

int CVICALLBACK RedrawGraph (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int i,yLeftPar=0, yRightPar=0;
	switch (event)
	{
		
	case EVENT_COMMIT:
		
			GetActiveTabPage (pnlvar, PNL_TAB, &mode);
			GetPanelHandleFromTabPage(pnlvar, PNL_TAB, mode, &tabhandle);
			SetCtrlVal (pnlvar, PNL_LEDRUN, 1);
			DimAllTabsCallbacksExcept(1, mode, -1, -1);
			
			if (mode==NTAB_CV)
			{
				GetCtrlVal (tabhandle, TAB_CV_CRPAR1, &yLeftPar); 
				GetCtrlVal (tabhandle, TAB_CV_CRPAR2, &yRightPar);
				CRsetAxes(NTAB_CV);
				for (i = 0; i<= cvglobalns; i++) 
				{
					PlotPoint (tabhandle, TAB_CV_GRAPH, cvdat[0][i], cvdat[yLeftPar][i], VAL_SOLID_CIRCLE,VAL_GREEN);
					if (yRightPar >= 0) 
					{
						SetCtrlAttribute (tabhandle, TAB_CV_GRAPH, ATTR_ACTIVE_YAXIS, VAL_RIGHT_YAXIS);	
						PlotPoint (tabhandle, TAB_CV_GRAPH, cvdat[0][i], cvdat[yRightPar][i], VAL_SOLID_SQUARE,VAL_YELLOW);
						SetCtrlAttribute (tabhandle, TAB_CV_GRAPH, ATTR_ACTIVE_YAXIS, VAL_LEFT_YAXIS);
					}
				}
			}
			
			else if (mode==NTAB_IV)
			{
				for (i = 0; i<= ivglobalns; i++) 
					PlotPoint (tabhandle, TAB_IV_GRAPH, ivdat[0][i], ivdat[1][i], VAL_SOLID_CIRCLE, VAL_GREEN);
			}
			
			else if (mode==NTAB_CF) 
			{
				GetCtrlVal (tabhandle, TAB_CF_CRPAR1, &yLeftPar); 
				GetCtrlVal (tabhandle, TAB_CF_CRPAR2, &yRightPar);
				CRsetAxes(NTAB_CF);
				for (i = 0; i<= cfglobalns; i++) 
				{
					PlotPoint (tabhandle, TAB_CF_GRAPH, cfdat[0][i], cfdat[yLeftPar][i], VAL_SOLID_CIRCLE,VAL_GREEN);
					if (yRightPar >= 0) 
					{
						SetCtrlAttribute (tabhandle, TAB_CF_GRAPH, ATTR_ACTIVE_YAXIS, VAL_RIGHT_YAXIS);	
						PlotPoint (tabhandle, TAB_CF_GRAPH, cfdat[0][i], cfdat[yRightPar][i], VAL_SOLID_SQUARE,VAL_YELLOW);
						SetCtrlAttribute (tabhandle, TAB_CF_GRAPH, ATTR_ACTIVE_YAXIS, VAL_LEFT_YAXIS);
					}
				}
			}
			
			else if (mode==NTAB_SV)
			{
				for (i = 0; i<= svglobalns; i++) 
				{
					PlotPoint (tabhandle, TAB_SV_GRAPH, svdat[0][i], svdat[1][i], VAL_SOLID_CIRCLE, VAL_GREEN);
					PlotPoint (tabhandle, TAB_SV_GRAPH, svdat[0][i], svdat[1][i]+svdat[2][i], VAL_CROSS,VAL_GREEN);
					PlotPoint (tabhandle, TAB_SV_GRAPH, svdat[0][i], svdat[1][i]-svdat[2][i], VAL_CROSS,VAL_GREEN);
				}
			}
		
		SetCtrlVal (pnlvar, PNL_LEDRUN, 0);
		DimAllTabsCallbacksExcept(0, -1, -1, -1);
		
		break;
	}
	return 0;
}

/******************* Enable email notification at end of measurement  ***********************/
int CVICALLBACK RelVbEnable (int panel, int control, int event,
void *callbackData, int eventData1, int eventData2)
{
	int relVbd=0;
	
	switch (event)
	{
	case EVENT_COMMIT:
		
		GetPanelHandleFromTabPage(pnlvar, PNL_TAB, NTAB_SV, &tabhandle);
		GetCtrlVal (tabhandle, TAB_SV_RELVBD, &relVbd);
		SetCtrlAttribute (tabhandle, TAB_SV_VBD, ATTR_DIMMED, !relVbd);
		
		break;
	}
	return 0;
}
