/**************************************************************************/
/* LabWindows/CVI User Interface Resource (UIR) Include File              */
/* Copyright (c) National Instruments 2021. All Rights Reserved.          */
/*                                                                        */
/* WARNING: Do not add to, delete from, or otherwise modify the contents  */
/*          of this include file.                                         */
/**************************************************************************/

#include <userint.h>

#ifdef __cplusplus
    extern "C" {
#endif

     /* Panels and Controls: */

#define  PNL                              1
#define  PNL_TAB                          2       /* control type: tab, callback function: (none) */
#define  PNL_LIGHTECHO                    3       /* control type: radioButton, callback function: (none) */
#define  PNL_HYSTERESIS                   4       /* control type: radioButton, callback function: (none) */
#define  PNL_USERTIME                     5       /* control type: numeric, callback function: (none) */
#define  PNL_SYNCWAITTIME                 6       /* control type: numeric, callback function: SetSyncWaitTime */
#define  PNL_READTIMEOUT                  7       /* control type: numeric, callback function: (none) */
#define  PNL_SCOPESETTING                 8       /* control type: numeric, callback function: (none) */
#define  PNL_DATFILENAME                  9       /* control type: string, callback function: (none) */
#define  PNL_TEXTMSG_4                    10      /* control type: textMsg, callback function: (none) */
#define  PNL_SETGPIBMODE                  11      /* control type: ring, callback function: SetGpibMode */
#define  PNL_TEXTMSG_5                    12      /* control type: textMsg, callback function: (none) */
#define  PNL_VOLTAGEOFF                   13      /* control type: command, callback function: VoltageOFF */
#define  PNL_TEXTMSG_3                    14      /* control type: textMsg, callback function: (none) */
#define  PNL_VOLTAGEOFFATEND              15      /* control type: radioButton, callback function: (none) */
#define  PNL_DEBUGPRINT                   16      /* control type: radioButton, callback function: DebugInit */
#define  PNL_SAVELOG                      17      /* control type: radioButton, callback function: (none) */
#define  PNL_TEXTMSG_2                    18      /* control type: textMsg, callback function: (none) */
#define  PNL_STARTMEAS                    19      /* control type: command, callback function: StartMeasurement */
#define  PNL_STOPMEAS                     20      /* control type: command, callback function: StopMeasurementCall */
#define  PNL_QUIT                         21      /* control type: command, callback function: Quit */
#define  PNL_PORT_6517A                   22      /* control type: numeric, callback function: (none) */
#define  PNL_COMPORT                      23      /* control type: numeric, callback function: (none) */
#define  PNL_TEMPERATURE                  24      /* control type: numeric, callback function: (none) */
#define  PNL_PORT_1024                    25      /* control type: numeric, callback function: (none) */
#define  PNL_PORT_XYMOVER                 26      /* control type: numeric, callback function: (none) */
#define  PNL_PORT_3532                    27      /* control type: numeric, callback function: (none) */
#define  PNL_RS_LED                       28      /* control type: LED, callback function: (none) */
#define  PNL_COM_LED                      29      /* control type: LED, callback function: (none) */
#define  PNL_XYMOVER                      30      /* control type: LED, callback function: (none) */
#define  PNL_KEITH_LED                    31      /* control type: LED, callback function: (none) */
#define  PNL_HIOKI_LED                    32      /* control type: LED, callback function: (none) */
#define  PNL_STATUS                       33      /* control type: string, callback function: (none) */
#define  PNL_STARTCHECK                   34      /* control type: radioButton, callback function: (none) */
#define  PNL_DEVCHECK                     35      /* control type: command, callback function: Devcheckcall */
#define  PNL_GRCLEAR                      36      /* control type: command, callback function: ClearGraph */
#define  PNL_GRREDRAW                     37      /* control type: command, callback function: RedrawGraph */
#define  PNL_SCOPESAVE                    38      /* control type: command, callback function: ScopeSave */
#define  PNL_SCOPELOAD                    39      /* control type: command, callback function: ScopeLoad */
#define  PNL_DATALOAD                     40      /* control type: command, callback function: Dataload */
#define  PNL_STRMAIL                      41      /* control type: string, callback function: MailEnable */
#define  PNL_MAILENB                      42      /* control type: radioButton, callback function: MailEnable */
#define  PNL_LEDRUN                       43      /* control type: LED, callback function: (none) */
#define  PNL_STOPTIME                     44      /* control type: string, callback function: (none) */
#define  PNL_DECORATION_4                 45      /* control type: deco, callback function: (none) */
#define  PNL_DECORATION_3                 46      /* control type: deco, callback function: (none) */
#define  PNL_DECORATION_2                 47      /* control type: deco, callback function: (none) */
#define  PNL_STARTTIME                    48      /* control type: string, callback function: (none) */
#define  PNL_STOPHOW                      49      /* control type: ring, callback function: (none) */

     /* tab page panel controls */
#define  TAB_CF_STARTFREQ                 2       /* control type: numeric, callback function: (none) */
#define  TAB_CF_STOPFREQ                  3       /* control type: numeric, callback function: (none) */
#define  TAB_CF_STEPFREQ                  4       /* control type: numeric, callback function: (none) */
#define  TAB_CF_HIOKI_VOLTAGE             5       /* control type: numeric, callback function: (none) */
#define  TAB_CF_MEASWAITTIME              6       /* control type: numeric, callback function: (none) */
#define  TAB_CF_VOLTAGE                   7       /* control type: numeric, callback function: (none) */
#define  TAB_CF_MAXVOLTAGE                8       /* control type: numeric, callback function: (none) */
#define  TAB_CF_GRAPH                     9       /* control type: graph, callback function: (none) */
#define  TAB_CF_HIOKIAXISINFO             10      /* control type: textMsg, callback function: (none) */
#define  TAB_CF_HIOKISCALEINFO            11      /* control type: textMsg, callback function: (none) */
#define  TAB_CF_HIOKISCALE                12      /* control type: radioButton, callback function: HiokiChangeScale */
#define  TAB_CF_CRPAR2                    13      /* control type: ring, callback function: (none) */
#define  TAB_CF_LED_YRIGHT                14      /* control type: LED, callback function: (none) */
#define  TAB_CF_LED_YLEFT                 15      /* control type: LED, callback function: (none) */
#define  TAB_CF_CRPAR1                    16      /* control type: ring, callback function: (none) */

     /* tab page panel controls */
#define  TAB_CV_FREQ                      2       /* control type: numeric, callback function: (none) */
#define  TAB_CV_STARTVOLTAGE              3       /* control type: numeric, callback function: (none) */
#define  TAB_CV_STOPVOLTAGE               4       /* control type: numeric, callback function: (none) */
#define  TAB_CV_HIOKI_VOLTAGE             5       /* control type: numeric, callback function: (none) */
#define  TAB_CV_MEASWAITTIME              6       /* control type: numeric, callback function: (none) */
#define  TAB_CV_STEPVOLTAGE               7       /* control type: numeric, callback function: (none) */
#define  TAB_CV_MAXVOLTAGE                8       /* control type: numeric, callback function: (none) */
#define  TAB_CV_GRAPH                     9       /* control type: graph, callback function: (none) */
#define  TAB_CV_HIOKIAXISINFO             10      /* control type: textMsg, callback function: (none) */
#define  TAB_CV_CRPAR1                    11      /* control type: ring, callback function: (none) */
#define  TAB_CV_CRPAR2                    12      /* control type: ring, callback function: (none) */
#define  TAB_CV_LED_YRIGHT                13      /* control type: LED, callback function: (none) */
#define  TAB_CV_LED_YLEFT                 14      /* control type: LED, callback function: (none) */

     /* tab page panel controls */
#define  TAB_DE_CLEARTERMINAL             2       /* control type: command, callback function: ClearTerminal */
#define  TAB_DE_SENDCOMMAND               3       /* control type: command, callback function: SendGenericCommand */
#define  TAB_DE_RESPONSE                  4       /* control type: textBox, callback function: (none) */
#define  TAB_DE_COMMAND                   5       /* control type: string, callback function: (none) */
#define  TAB_DE_BYTESTOREAD               6       /* control type: numeric, callback function: (none) */
#define  TAB_DE_COMRATE                   7       /* control type: numeric, callback function: (none) */
#define  TAB_DE_ADDR                      8       /* control type: numeric, callback function: (none) */
#define  TAB_DE_RESPONSEWAIT              9       /* control type: radioButton, callback function: (none) */
#define  TAB_DE_TEXTMSG                   10      /* control type: textMsg, callback function: (none) */
#define  TAB_DE_SENDMODE                  11      /* control type: ring, callback function: SetSendMode */

     /* tab page panel controls */
#define  TAB_IV_STARTVOLTAGE              2       /* control type: numeric, callback function: (none) */
#define  TAB_IV_STOPVOLTAGE               3       /* control type: numeric, callback function: (none) */
#define  TAB_IV_STEPVOLTAGE               4       /* control type: numeric, callback function: (none) */
#define  TAB_IV_MAXCURRENT                5       /* control type: numeric, callback function: (none) */
#define  TAB_IV_MEASWAITTIME              6       /* control type: numeric, callback function: (none) */
#define  TAB_IV_MAXVOLTAGE                7       /* control type: numeric, callback function: (none) */
#define  TAB_IV_GRAPH                     8       /* control type: graph, callback function: (none) */
#define  TAB_IV_CURRENTRANGE              9       /* control type: ring, callback function: (none) */
#define  TAB_IV_AUTORANGE                 10      /* control type: ring, callback function: (none) */
#define  TAB_IV_LED_YLEFT                 11      /* control type: LED, callback function: (none) */

     /* tab page panel controls */
#define  TAB_SV_STARTVOLTAGE              2       /* control type: numeric, callback function: (none) */
#define  TAB_SV_VBD                       3       /* control type: numeric, callback function: (none) */
#define  TAB_SV_STOPVOLTAGE               4       /* control type: numeric, callback function: (none) */
#define  TAB_SV_STEPVOLTAGE               5       /* control type: numeric, callback function: (none) */
#define  TAB_SV_EVENTCOUNT                6       /* control type: numeric, callback function: (none) */
#define  TAB_SV_SCOPETIME                 7       /* control type: numeric, callback function: (none) */
#define  TAB_SV_SCOPESCALE                8       /* control type: numeric, callback function: (none) */
#define  TAB_SV_MAXVOLTAGE                9       /* control type: numeric, callback function: (none) */
#define  TAB_SV_GRAPH                     10      /* control type: graph, callback function: (none) */
#define  TAB_SV_RELVBD                    11      /* control type: radioButton, callback function: RelVbEnable */
#define  TAB_SV_SCOPEAUTOSCALE            12      /* control type: radioButton, callback function: (none) */
#define  TAB_SV_LED_YLEFT                 13      /* control type: LED, callback function: (none) */
#define  TAB_SV_INVERT                    14      /* control type: radioButton, callback function: (none) */

     /* tab page panel controls */
#define  TAB_SXY_GRAPH                    2       /* control type: graph, callback function: (none) */
#define  TAB_SXY_STOPXY                   3       /* control type: numeric, callback function: (none) */
#define  TAB_SXY_NOWY                     4       /* control type: numeric, callback function: (none) */
#define  TAB_SXY_EVENTCOUNT               5       /* control type: numeric, callback function: (none) */
#define  TAB_SXY_SCOPETIME                6       /* control type: numeric, callback function: (none) */
#define  TAB_SXY_SCOPESCALE               7       /* control type: numeric, callback function: (none) */
#define  TAB_SXY_NOWX                     8       /* control type: numeric, callback function: (none) */
#define  TAB_SXY_GOTOY                    9       /* control type: numeric, callback function: (none) */
#define  TAB_SXY_SCOPEAUTOSCALE           10      /* control type: radioButton, callback function: (none) */
#define  TAB_SXY_INVERT                   11      /* control type: radioButton, callback function: (none) */
#define  TAB_SXY_GOTOX                    12      /* control type: numeric, callback function: (none) */
#define  TAB_SXY_STEPXY                   13      /* control type: numeric, callback function: (none) */
#define  TAB_SXY_STARTXY_2                14      /* control type: ring, callback function: (none) */
#define  TAB_SXY_STARTXY                  15      /* control type: ring, callback function: (none) */
#define  TAB_SXY_GOBACKXY                 16      /* control type: radioButton, callback function: (none) */
#define  TAB_SXY_GOHOME                   17      /* control type: command, callback function: (none) */
#define  TAB_SXY_NOWXY                    18      /* control type: graph, callback function: (none) */
#define  TAB_SXY_GOTOXY                   19      /* control type: command, callback function: (none) */
#define  TAB_SXY_DECORATION               20      /* control type: deco, callback function: (none) */


     /* Control Arrays: */

          /* (no control arrays in the resource file) */


     /* Menu Bars, Menus, and Menu Items: */

          /* (no menu bars in the resource file) */


     /* Callback Prototypes: */

int  CVICALLBACK ClearGraph(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ClearTerminal(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK Dataload(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK DebugInit(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK Devcheckcall(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK HiokiChangeScale(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK MailEnable(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK Quit(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK RedrawGraph(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK RelVbEnable(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ScopeLoad(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ScopeSave(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK SendGenericCommand(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK SetGpibMode(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK SetSendMode(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK SetSyncWaitTime(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK StartMeasurement(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK StopMeasurementCall(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK VoltageOFF(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);


#ifdef __cplusplus
    }
#endif
